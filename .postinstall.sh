#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/blipnblup/
sudo chown $USER ~/.local/share/icons/blipnblup.svg
update-icon-caches ~/.local/share/icons/
