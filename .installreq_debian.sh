#!/bin/sh

sudo apt-get install \
    libx11-dev libxrandr-dev libasound2-dev libegl1-mesa-dev \
    git make cmake build-essential doxygen libroar-dev libjack-dev \
    libsamplerate0-dev liboss4-salsa-asound2 libgbm-dev
