#!/bin/sh

./.installreq_debian.sh
./setup_dry.sh

git pull
cd ..
mkdir BlipNBlup-build
cd BlipNBlup-build
qmake ../BlipNBlup/BlipNBlup.pro
sudo make install
sudo chown -R $USER ~/.local/share/luckey/blipnblup/
sudo chown $USER ~/.local/share/icons/blipnblup.svg
update-icon-caches ~/.local/share/icons/
cd ..
rm -rf BlipNBlup-build
