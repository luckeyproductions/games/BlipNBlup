/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "controllable.h"

Controllable::Controllable(Context* context): SceneObject(context),
    move_{},
    aim_{},
    maxPitch_{ 90.f },
    minPitch_{ 0.f },

    actions_{},
    actionSince_{},

    graphicsNode_{ nullptr },
    model_{ nullptr },
    rigidBody_{ nullptr },
    collider_{ nullptr },
    animCtrl_{ nullptr },

    harmful_{ false }
{
    for (int a{ 0 }; a < NUM_CONTROLLABLE_ACTIONS; ++a)
        actionSince_[a] = 0.f;
}

void Controllable::OnNodeSet(Node *node)
{
    if (!node)
        return;

    graphicsNode_ = node_->CreateChild("Graphics");
    model_ = graphicsNode_->CreateComponent<AnimatedModel>();
    animCtrl_ = model_->GetNode()->CreateComponent<AnimationController>();

    rigidBody_ = node_->CreateComponent<RigidBody>();
    collider_ = node_->CreateComponent<CollisionShape>();

    model_->SetCastShadows(true);
}

void Controllable::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    ResetInput();

    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->ResetForces();

    animCtrl_->StopLayer(1u);
}

void Controllable::Update(float timeStep)
{
    for (int a{ 0 }; a < static_cast<int>(actions_.size()); ++a)
    {
        if (actions_[a])
            actionSince_[a] += timeStep;
    }
}

void Controllable::Disable()
{
    SceneObject::Disable();

    ResetInput();
}

void Controllable::SetMove(const Vector3& move)
{
    if (move.Length() > 1.f)
        move_ = move.Normalized();
    else
        move_ = move;
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (int i{ 0 }; i < static_cast<int>(actions.size()); ++i)
        {
            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (actions[i])
                    HandleAction(i);
                else
                    actionSince_[i] = 0.f;
            }
        }
    }
}

void Controllable::AlignWithMovement(float timeStep)
{
    Quaternion rot{ node_->GetRotation() };
    Quaternion targetRot{};
    targetRot.FromLookRotation(move_);
    rot = rot.Slerp(targetRot, Clamp(timeStep * 23.f, 0.f, 1.f));
    node_->SetRotation(rot);
}

void Controllable::AlignWithVelocity(float timeStep)
{
    Quaternion targetRot{};
    Quaternion rot{node_->GetRotation()};
    targetRot.FromLookRotation(rigidBody_->GetLinearVelocity());
    ClampPitch(targetRot);
    const float horizontalVelocity{(rigidBody_->GetLinearVelocity() * Vector3{ 1.f, 0.f, 1.f }).Length()};
    node_->SetRotation(rot.Slerp(targetRot, Clamp(timeStep * horizontalVelocity, 0.f, 1.f)));
}

void Controllable::ClampPitch(Quaternion& rot)
{
    const float maxCorrection{ rot.EulerAngles().x_ - maxPitch_ };
    if (maxCorrection > 0.f)
        rot = Quaternion{ -maxCorrection, node_->GetRight() } * rot;

    float minCorrection{rot.EulerAngles().x_ - minPitch_};
    if (minCorrection < 0.f)
        rot = Quaternion{ -minCorrection, node_->GetRight() } * rot;
}
