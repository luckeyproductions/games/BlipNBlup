/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WASP_H
#define WASP_H

#include "flyer.h"

class Rage;

class Wasp: public Flyer
{
    DRY_OBJECT(Wasp, Flyer);
public:
    static void RegisterObject(Context* context);

    Wasp(Context* context);
    void Set(const Vector3& position, const Quaternion& rotation) override;

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void OnReleased(StringHash, VariantMap&);
    void OnCatched(StringHash, VariantMap&);

    Rage* rage_;
};

#endif // WASP_H
