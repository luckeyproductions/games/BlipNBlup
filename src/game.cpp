/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ui/gui.h"
#include "ui/dash.h"
#include "block.h"
#include "blockmap.h"
#include "bnbcam.h"
#include "bubble.h"
#include "catchable.h"
#include "fish.h"
#include "overworld/island.h"
#include "satong.h"
#include "fly.h"
#include "wasp.h"
#include "wind.h"
#include "bnbcam.h"

#include "couch.h"
#include "jukebox.h"
#include "spawnmaster.h"

#include "game.h"

void Game::RegisterObject(Context* context)
{
    BlockMap::RegisterObject(context);
    Arcade::RegisterObject(context);

    BnBCam::RegisterObject(context);
    Block::RegisterObject(context);
    Fish::RegisterObject(context);
    Satong::RegisterObject(context);
    Bubble::RegisterObject(context);
    Wind::RegisterObject(context);

    Catchable::RegisterObject(context);
    Fly::RegisterObject(context);
    Wasp::RegisterObject(context);
}

Game::Game(Context* context): Object(context),
    world_{},
    arcade_{ nullptr },
    status_{ GS_NONE },
    mode_{ GM_NONE },
    difficulty_{ D_NORMAL }
{
    context_->RegisterSubsystem<JukeBox>();
    context_->RegisterSubsystem<Island>();
    arcade_ = context->RegisterSubsystem<Arcade>();

//    AudioSettings* audioSettings{ GetSubsystem<Settings>()->GetAudioSettings() };
//    const float musicGain{ audioSettings->GetAttribute(AS_MUSIC_ENABLED).GetBool() * audioSettings->GetAttribute(AS_MUSIC_GAIN).GetFloat() };
//    const float effectGain{ audioSettings->GetAttribute(AS_SAMPLES_ENABLED).GetBool() * audioSettings->GetAttribute(AS_SAMPLES_GAIN).GetFloat() };

    Audio* audio{ GetSubsystem<Audio>() };
//    audio->SetMasterGain(SOUND_MUSIC, musicGain);
//    audio->SetMasterGain(SOUND_EFFECT, effectGain);

    audio->SetMasterGain(SOUND_MUSIC, .23f);
    audio->SetMasterGain(SOUND_EFFECT, 1.f);

    SubscribeToEvent(E_FISHFAINTED, DRY_HANDLER(Game, HandleFishFainted));
    SubscribeToEvent(E_BUGRELEASED, DRY_HANDLER(Game, HandleBugReleased));

    // Draw Debug
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Game, HandlePostRenderUpdate));
}

bool Game::IsSinglePlayerStory() const
{
    return GetSubsystem<Couch>()->GetNumControllingPlayers() == 1u && mode_ == GM_STORY;
}

void Game::SwitchFish()
{
    for (Player* p: GetSubsystem<Couch>()->GetPlayers())
    {
        Controllable* c{ p->GetControllable() };
        if (!c)
        {
            continue;
        }
        else
        {
            if (c == world_.blip_ && world_.blup_->IsConscious())
                p->SetControl(world_.blup_);
            else if (c == world_.blup_ && world_.blip_->IsConscious())
                p->SetControl(world_.blip_);

            GetSubsystem<InputMaster>()->Unpress(p->GetPlayerID());

            break;
        }
    }
}

Scene* Game::CreatePlayScene()
{
    Scene* scene{ new Scene(context_) };
    scene->CreateComponent<Octree>();
    scene->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);
    Zone* zone{ scene->CreateComponent<Zone>() };
    zone->SetBoundingBox({ -Vector3::ONE * 1000.f, Vector3::ONE * 1000.f });

    PhysicsWorld* physicsWorld{ scene->CreateComponent<PhysicsWorld>() };
    physicsWorld->SetGravity(Vector3::DOWN * 55.f);
    physicsWorld->SetInternalEdge(true);
    physicsWorld->SetSplitImpulse(true);
    physicsWorld->SetFps(70);

    world_.walkNav_ = scene->CreateComponent<NavigationMesh>();
    world_.flyNav_  = scene->CreateComponent<NavigationMesh>();

    for (NavigationMesh* navMesh: { world_.walkNav_, world_.flyNav_ })
    {
        navMesh->SetAgentRadius(2/3.f);
        navMesh->SetPadding(Vector3::UP);
        navMesh->SetTileSize(128);
    }

    world_.walkNav_->SetAgentHeight(1.f);
    world_.walkNav_->SetAgentMaxClimb(.17f);
    world_.walkNav_->SetAgentMaxSlope(45.f);

    world_.flyNav_->SetAgentHeight(4.f);
    world_.flyNav_->SetAgentMaxClimb(1.5f);
    world_.flyNav_->SetAgentMaxSlope(60.f);

    // Add sky
    Skybox* skybox{ scene->CreateComponent<Skybox>() };
    skybox->SetModel(RES(Model, "Models/Box.mdl"));
    skybox->SetMaterial(RES(Material, "Materials/Skybox.xml"));
    skybox->GetMaterial()->SetShaderParameter("MatDiffColor", Color(0.5f, 0.6f, 1.0f));

    // Create sun.
    Node* sunNode{ scene->CreateChild("Sun") };
    sunNode->SetPosition({ -2.3f, 10.f, -4.2f });
    sunNode->LookAt(Vector3::ZERO);
    Light* sun{ sunNode->CreateComponent<Light>() };
    sun->SetLightType(LIGHT_DIRECTIONAL);
    sun->SetBrightness(.42f);
    sun->SetColor({ .95f, .9f, .85f });
    sun->SetCastShadows(true);
//    light->SetShadowIntensity(0.13f);

    // Create upwards direcitonal light for suggested radiocity.
    Node* floorLightNode{ scene->CreateChild("FloorLight") };
    floorLightNode->SetPosition({ 0.f, -10.f, 0.f });
    floorLightNode->LookAt({ 0.f, 0.f, 0.f });
    Light* floorLight{ floorLightNode->CreateComponent<Light>() };
    floorLight->SetLightType(LIGHT_DIRECTIONAL);
    floorLight->SetBrightness(.25f);
    floorLight->SetColor({ .05f, .23f, .13f });

    // Create block map
    world_.level_ = scene->CreateChild("Level")->CreateComponent<BlockMap>();

    return scene;
}

void Game::PrepareStoryScene()
{
    if (!world_.playScene_)
        world_.playScene_ = CreatePlayScene();

    if (!world_.blip_)
    {
        world_.blip_ = SPAWN->Create<Fish>();
        world_.blup_ = SPAWN->Create<Fish>();
    }

    world_.blip_->BecomeBlip();
    world_.blup_->BecomeBlup();

    world_.playScene_->SetElapsedTime(0.f);
}

void Game::PrepareArcadeScene()
{
    if (!arcade_)
        return;

    if (!world_.playScene_)
        world_.playScene_ = CreatePlayScene();

    for (Fish* fish: { world_.blip_, world_.blup_ })
    {
        if (fish)
            fish->Disable();
    }

    world_.playScene_->SetElapsedTime(0.f);
}

void Game::SetStatus(GameStatus newStatus)
{
    if (status_ == newStatus)
        return;

    GUI* gui{ GetSubsystem<GUI>() };
    GameStatus oldStatus{ status_ };
    status_ = newStatus;

    if (newStatus == GS_PLAY && (oldStatus != GS_PAUSED || oldStatus == GS_OVERWORLD))
        UpdatePlayerViewports();
    else if (newStatus < GS_PLAY)
        RENDERER->SetNumViewports(1u);

    if (newStatus == GS_GAMEOVER)
    {
        gui->FadeOut();
    }
    else if (newStatus == GS_OVERWORLD)
    {
        gui->SetBlackness(1.f);
        MC->UpdateViewportRects();
    }
    else
    {
        const int oldYSectors{ MC->GetYSectors() };
        MC->UpdateViewportRects();

        if (MC->GetYSectors() != oldYSectors)
            DASH->UpdateSizes();
    }

    if (oldStatus == GS_GAMEOVER)
        gui->GetDash()->ResetFade();

    VariantMap eventData{};
    eventData[GameStatusChanged::P_OLDSTATUS] = oldStatus;
    eventData[GameStatusChanged::P_STATUS]    = newStatus;
    SendEvent(E_GAMESTATUSCHANGED, eventData);
}

void Game::SetGameMode(GameMode mode)
{
    mode_ = mode;

    VariantMap eventData{};
    eventData[GameModeChanged::P_MODE] = mode_;
    SendEvent(E_GAMEMODECHANGED, eventData);
}

void Game::StartNewStory(Difficulty difficulty)
{
    difficulty_ = difficulty;
    SetGameMode(GM_STORY);

    PrepareStoryScene();
    PODVector<Player*> players{ GetSubsystem<Couch>()->GetPlayers() };
    if (players.Size() > 0u)
        players.Front()->SetControl(world_.blip_, false);
    if (players.Size() > 1u)
        players.At(1u)->SetControl(world_.blup_, false);
    if (players.Size() > 2u)
    {
        for (unsigned p{ 2u }; p < players.Size(); ++p)
            players.At(p)->SetControl(nullptr);
    }

    SetStatus(GS_OVERWORLD);
}

void Game::StartStoryLevel(const String& mapName)
{
    LoadMap(mapName);
    DASH->ResetFishConsciousness();

    for (Player* p: COUCH->GetControllingPlayers())
        p->GetCamera()->JumpToTarget();

    Unpause();
}

void Game::RestartStoryLevel()
{
    StartStoryLevel(world_.level_->GetFilename());
}

void Game::StartArcade(GameMode mode, const String& mapName)
{
    difficulty_ = D_NORMAL;
    SetGameMode(mode);
    PrepareArcadeScene();
    LoadMap(mapName);
    unsigned numPlayers{ COUCH->GetNumPlayers() };
    arcade_->SetTeamSize(Min(CeilToInt(numPlayers * .5f), arcade_->MaxTeamSize(RES(XMLFile, CompleteMapName(mapName))))); /// Should be taken from arcade menu option
    arcade_->StartMatch();

    Unpause();
}

void Game::UpdatePlayerViewports()
{
    PODVector<SoundListener*> listeners{};
    PODVector<Player*> players{ COUCH->GetControllingPlayers() };
    RENDERER->SetNumViewports(players.Size());
    for (unsigned p{ 0u }; p < players.Size(); ++p)
    {
        Player* player{ players.At(p) };
        player->GetCamera()->ClaimViewport(p);

        BnBCam* jib{ player->GetCamera() };
        jib->SetReflectionsEnabled(world_.level_->HasWater());
        listeners.Push(jib->GetComponent<SoundListener>());
    }

    AUDIO->SetListeners(listeners);
}

String Game::CompleteMapName(const String& mapName)
{
    String fullMapName{ mapName };

    if (!fullMapName.Contains("Maps/", true))
        fullMapName.Insert(0, "Maps/");

    if (!fullMapName.EndsWith(".emp", false))
        fullMapName = fullMapName.Append(".emp");

    return fullMapName;
}

void Game::LoadMap(const String& mapName)
{
    const String fullMapName{ CompleteMapName(mapName) };

    world_.level_->LoadXMLFile(RES(XMLFile, fullMapName));

    for (NavigationMesh* navMesh: { world_.walkNav_, world_.flyNav_ })
        navMesh->Build();
}

void Game::TogglePause()
{
    if (status_ == GS_PLAY && !GetSubsystem<Island>()->IsActive())
        Pause();
    else if (status_ == GS_PAUSED)
        Unpause();
}

void Game::Pause()
{
    if (status_ == GS_OVERWORLD)
        return;

    world_.playScene_->SetUpdateEnabled(false);

    if (status_ != GS_GAMEOVER)
        SetStatus(GS_PAUSED);
}

void Game::Unpause()
{
    if (status_ != GS_GAMEOVER)
    {
        world_.playScene_->SetUpdateEnabled(true);
        SetStatus(GS_PLAY);
    }
}

void Game::HandleFishFainted(StringHash /*eventType*/, VariantMap& eventData)
{
    Fish* fish{ static_cast<Fish*>(eventData[FishFainted::P_FISH].GetPtr()) };
    if (!fish)
        return;

    if (GAME->GetGameMode() == GM_STORY)
    {
        if (world_.blip_->IsConscious() || world_.blup_->IsConscious())
        {
            if (IsSinglePlayerStory())
                SwitchFish();
        }
        else
        {
            SetStatus(GS_GAMEOVER);
        }
    }
    else if (GAME->GetGameMode() == GM_SURVIVAL)
    {
        if (!arcade_->AnyConscious(fish->GetTeam()))
        {
            SetStatus(GS_GAMEOVER);
        }
    }
}

void Game::HandleBugReleased(StringHash /*eventType*/, VariantMap& eventData)
{
    const bool devoured{ eventData[BugReleased::P_DEVOURED].GetPtr() != nullptr };

    if (mode_ == GM_STORY && devoured && SPAWN->CountActiveDerived<Catchable>() == 0)
        SetStatus(GS_OVERWORLD);
}

void Game::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!world_.playScene_)
        return;

    DebugRenderer* debug{ world_.playScene_->GetComponent<DebugRenderer>() };
    for (NavigationMesh* navMesh: { /*world_.walkNav_,*/ world_.flyNav_ })
        navMesh->DrawDebugGeometry(debug, false);

//    PODVector<RigidBody*> blockMapBodies{};
//    world_.level_->GetNode()->GetComponents<RigidBody>(blockMapBodies, true);
//    for (RigidBody* blockBody: blockMapBodies)
//        blockBody->DrawDebugGeometry(debug, false);
}
