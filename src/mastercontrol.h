/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);

    void Setup() override;
    void Start() override;
    void Stop() override;
    void Exit();

    void UpdateViewportRects();
    int GetYSectors() const { return ySectors_; }

    float Sine(const float freq, const float min, const float max, const float shift = 0.f);
    float Cosine(const float freq, const float min, const float max, const float shift = 0.f);

private:
    void SetupDefaultRenderPath();
    void CreateScene();
    void SubscribeToEvents();

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);

    float SinePhase(float freq, float shift);

    int ySectors_;
    bool drawDebug_;
    String storagePath_;
};

#endif // MASTERCONTROL_H
