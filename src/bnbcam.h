/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BNBCAM_H
#define BNBCAM_H

#include "luckey.h"

struct Reflector
{
    void SetUpdateMode(RenderSurfaceUpdateMode mode)
    {
        RenderSurface* surface{ static_cast<Texture2D*>(material_->GetTexture(TU_DIFFUSE))->GetRenderSurface() };
        surface->SetUpdateMode(mode);
    }

    void Clear()
    {
        camera_->Remove();
        camera_ = nullptr;
        water_->Remove();
        water_ = nullptr;
        material_.Reset();
    }

    Camera* camera_{ nullptr };
    StaticModel* water_{ nullptr };
    SharedPtr<Material> material_{ nullptr };
};

class BnBCam: public LogicComponent
{
    DRY_OBJECT(BnBCam, LogicComponent);

public:
    static void RegisterObject(Context* context);
    BnBCam(Context *context);

    void Update(float timeStep) override;

    void SetTarget(Node* target, bool forceJump = false);
    void JumpToTarget();
    void ClaimViewport(unsigned v);

    Quaternion GetRotation() const { return node_->GetRotation(); }
    Camera* GetCamera() const { return camera_; }

    void SetReflectionsEnabled(bool enabled);
    void UpdateReflectionAspectRatio();

    
protected:
    void OnNodeSet(Node* node) override;

private:
    void JumpToNode(Node* node);
    void HandleRestart(StringHash eventType, VariantMap& eventData);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);

    Node* targetNode_;
    Camera* camera_;
    unsigned viewportIndex_;
    Vector3 lastTargetPos_;
    float sinceSwitch_;
    Reflector reflector_;
};

#endif // BNBCAM_H
