/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"

#include "walker.h"

Walker::Walker(Context* context): Controllable(context),
    onGround_    { false },
    maxSlope_    { 23.f },
    doubleJumper_{ false },
    doubleJumped_{ false },
    jumpInterval_{ .05f },
    sinceJump_   { jumpInterval_ }
{
    maxPitch_ = 60.f;
    minPitch_ = -80.f;
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Walker::OnNodeSet(Node *node)
{
    Controllable::OnNodeSet(node);

    rigidBody_->SetLinearRestThreshold(0.f);
    rigidBody_->SetLinearDamping(.23f);
    rigidBody_->SetAngularDamping(1.f);
    rigidBody_->SetAngularFactor(Vector3::UP);
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Walker, HandleNodeCollision));
}

void Walker::Set(const Vector3& position, const Quaternion& rotation)
{
    Controllable::Set(position, rotation);

    sinceJump_ = jumpInterval_;
    doubleJumped_ = false;
}

void Walker::Update(float timeStep)
{
    Controllable::Update(timeStep);

    sinceJump_ += timeStep;
}

void Walker::FixedUpdate(float timeStep)
{
    float run{ Clamp(1.f - 5.5f * actionSince_[PIA_RUN], 0.f, 1.f) * onGround_ };
    float maxWalkSpeed{ maxRunSpeed_ * (.6f + .4f * run * onGround_) };
    float walkThrust{ runThrust_ * (.8f + .2f * run * onGround_) };
    float relativeSoleSize{ node_->GetScale().x_ * node_->GetScale().z_ };

    maxWalkSpeed *= node_->GetScale().z_;
    walkThrust *= node_->GetScale().z_;

    PODVector<RigidBody*> bodies{};
    rigidBody_->GetCollidingBodies(bodies);
    if (!bodies.Size())
        onGround_ = false;

    //Apply movement
    if (move_.Length() > 0.f)
    {
        AlignWithMovement(timeStep);

        rigidBody_->SetFriction(.42f * onGround_);
        Vector3 force{ walkThrust * move_ };
        const Vector3 planarVelocity{ rigidBody_->GetLinearVelocity() * Vector3::ONE - Vector3::UP };

        if (planarVelocity.Length() <= maxWalkSpeed
         || planarVelocity.DotProduct(force) < 0.f)
        {
            const float remainingSpeed{ Min(
                            maxWalkSpeed - planarVelocity.Length() * (1.f - (planarVelocity.Angle(force) / 90.f)),
                            maxWalkSpeed)};

            const float maxThrust{ Max(walkThrust * remainingSpeed * rigidBody_->GetMass(), 0.f) };
            if (force.Length() > maxThrust)
                force = force.Normalized() * maxThrust;

            rigidBody_->ApplyForce(force * timeStep);
        }
    }
    else
    {
        rigidBody_->SetFriction(2.f * onGround_ * relativeSoleSize);
    }

    //Update animation and orientation
    float velocity{ rigidBody_->GetLinearVelocity().Length() / node_->GetScale().z_ };

    if (onGround_)
    {
//        AlignWithFloor(timeStep);

        if (animCtrl_)
        {
            if (velocity < .01f)
            {
                animCtrl_->PlayExclusive(idleAnim_, 0, true, .1f);
            }
            else
            {
                animCtrl_->PlayExclusive(walkAnim_, 0, true, .1f);
                animCtrl_->SetSpeed(walkAnim_, velocity * .38f);
            }
        }
    }
    else
    {
//        AlignWithVelocity(timeStep);
        if (animCtrl_)
        {
            animCtrl_->PlayExclusive(midairAnim_, 0, true, .1f);
            animCtrl_->SetSpeed(midairAnim_, velocity * .23f);
        }
    }
}

void Walker::HandleNodeCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    CheckOnGround(contacts);
}

void Walker::HandleNodeCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    MemoryBuffer contacts{ eventData[NodeCollision::P_CONTACTS].GetBuffer() };
    CheckOnGround(contacts);
}

void Walker::CheckOnGround(MemoryBuffer& contacts)
{
    const float flatSpeed{ rigidBody_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() };
    const float maxIncline{ flatSpeed * Sin(maxSlope_) };

    while (!contacts.IsEof())
    {
        /*Vector3 contactPosition = */ contacts.ReadVector3();
        const Vector3 contactNormal{ contacts.ReadVector3() };
        /*float contactDistance = */contacts.ReadFloat();
        /*float contactImpulse = */contacts.ReadFloat();

        if (contactNormal.Angle(Vector3::UP) < maxSlope_ && rigidBody_->GetLinearVelocity().y_ <= maxIncline )
        {
            onGround_ = true;
            doubleJumped_ = false;
            return;
        }
    }
}

void Walker::AlignWithFloor(float timeStep)
{
    PODVector<PhysicsRaycastResult> hitResults{};
    Ray footRay{};
    footRay.origin_ = node_->GetPosition() - node_->GetUp() * (collider_->GetSize().y_ - collider_->GetSize().x_) * .5f;
    footRay.direction_ = Vector3::DOWN;

    if (GetScene()->GetComponent<PhysicsWorld>()->Raycast(hitResults, footRay, .8f, 1)
     && (hitResults[0].normal_.Angle(Vector3::UP)) < maxSlope_)
    {
        const float angleDelta{ node_->GetUp().Angle(hitResults[0].normal_) };

        if (angleDelta > 1.f)
            node_->RotateAround(footRay.origin_, Quaternion::IDENTITY.Slerp(Quaternion(node_->GetUp(), hitResults[0].normal_), Clamp(timeStep * 23.0f, 0.0f, 1.0f)), TS_WORLD);
    }
}

void Walker::Jump()
{
    if (sinceJump_ < jumpInterval_)
        return;

    else if (onGround_)
    {
        sinceJump_ = 0.f;
        onGround_ = false;
        Vector3 jumpImpulse{ Vector3::UP * jumpForce_ +
                    rigidBody_->GetLinearVelocity() * jumpForce_ * .023f };

        rigidBody_->ApplyImpulse(jumpImpulse);

        PlaySample("Jump.wav");
    }

    else if (doubleJumper_ && !doubleJumped_)
    {
        doubleJumped_ = true;
        const Vector3 jumpImpulse{ Vector3::UP * jumpForce_ * .5f +
                             rigidBody_->GetLinearVelocity().Normalized() * jumpForce_ * .0666f
                           };
        rigidBody_->ApplyImpulse(jumpImpulse);
    }
}


