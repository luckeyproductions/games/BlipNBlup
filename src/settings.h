/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "luckey.h"

#define SETTINGSPATH GetSubsystem<MasterControl>()->GetStoragePath() + "/Settings.xml"

static const char* GS_ANTIALIASING { "Anti-Aliasing"     };
static const char* GS_BLOOM        { "Bloom"             };
static const char* GS_SSAO         { "Ambient Occlusion" };
static const char* GS_VSYNC        { "V-Sync"            };
static const char* GS_FULLSCREEN   { "Fullscreen"        };
static const char* GS_RESOLUTION   { "Resolution"        };

static const char* AS_MUSIC_ENABLED   { "Music Enabled"   };
static const char* AS_MUSIC_GAIN      { "Music Gain"      };
static const char* AS_SAMPLES_ENABLED { "Samples Enabled" };
static const char* AS_SAMPLES_GAIN    { "Samples Gain"    };

enum SettingsCategory{ SC_GRAPHICS, SC_AUDIO, SC_INPUT };
class GraphicsSettings: public Serializable
{
    DRY_OBJECT(GraphicsSettings, Serializable);
    friend class Settings;

public:
    static void RegisterObject(Context* context);
    GraphicsSettings(Context* context);

private:
    bool fullScreen_;
    bool vSync_;
    bool antiAliasing_;
    bool bloom_;
    bool ambientOcclusion_;

    IntVector2 resolution_;
};

class AudioSettings: public Serializable
{
    DRY_OBJECT(AudioSettings, Serializable);
    friend class Settings;

public:
    static void RegisterObject(Context* context);
    AudioSettings(Context* context);

private:
    bool musicEnabled_;
    float musicGain_;
    bool effectsEnabled_;
    float effectsGain_;
};

class Settings: public Object
{
    DRY_OBJECT(Settings, Object);

public:
    static void RegisterObject(Context* context);
    Settings(Context* context);

    bool LoadXML(const XMLElement& source);
    bool SaveXML(XMLElement& dest);

    GraphicsSettings* GetGraphicsSettings() const { return graphics_; }
    AudioSettings* GetAudioSettings() const { return audio_; }

private:
    GraphicsSettings* graphics_;
    AudioSettings* audio_;
};

#endif // SETTINGS_H
