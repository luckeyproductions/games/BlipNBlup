/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "controllable.h"
#include "catchable.h"
#include "fish.h"
#include "bubble.h"
#include "ui/dash.h"

#include "satong.h"

void Satong::RegisterObject(Context* context)
{
    context->RegisterFactory<Satong>();
}

Satong::Satong(Context* context): SceneObject(context),
    model_{ nullptr },
    animCtrl_{ nullptr },
    satiation_{ .5f },
    team_{ TP_RANDOM },
    ikSolver_{ nullptr },
    ikEffector_{ nullptr },
    chillTime_{ 0.f },
    accessories_{}
{
    SubscribeToEvent(E_SCENEDRAWABLEUPDATEFINISHED, DRY_HANDLER(Satong, HandleSceneDrawableUpdateFinished));
}

void Satong::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    satiation_ = .5f;
    SetTeam(TP_RANDOM);
    ikEffector_->RemoveAttributeAnimation("Weight");
    ikEffector_->SetWeight(0.f);

    for (StaticModel* model: accessories_)
        model->Remove();

    /// if beach
    {
        StaticModel* chair{ node_->CreateComponent<AnimatedModel>() };
        chair->SetModel(RES(Model, "Models/Chair.mdl"));
        chair->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
        accessories_.Push(chair);

        Node* shellNode{ node_->GetChild("Shell", true) };
        StaticModel* shell{ shellNode->CreateComponent<StaticModel>() };
        shell->SetModel(RES(Model, "Models/Shell.mdl"));
        shell->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
        accessories_.Push(shell);

        Node* strawNode{ node_->GetChild("Straw", true) };
        StaticModel* straw{ strawNode->CreateComponent<StaticModel>() };
        straw->SetModel(RES(Model, "Models/Straw.mdl"));
        straw->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
        accessories_.Push(straw);

        Node* headNode{ node_->GetChild("Head", true) };
        StaticModel* shades{ headNode->CreateComponent<StaticModel>() };
        shades->SetModel(RES(Model, "Models/Shades.mdl"));
        shades->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
        accessories_.Push(shades);
    }

    ikEffector_->SetRotationDecay(.25f);
    animCtrl_->SetTime("Animations/Satong/Chill.ani", Random(animCtrl_->GetLength("Animations/Satong/Chill.ani")));
}

void Satong::Devour(Catchable* catchable)
{
    Node* catNode{ catchable->GetNode() };

    ikEffector_->SetTargetPosition(GetBitePos(catNode));
    ValueAnimation* ikEffectAnim{ new ValueAnimation{ context_ } };
    ikEffectAnim->SetKeyFrame(0.f, ikEffector_->GetWeight());
    ikEffectAnim->SetKeyFrame(1/3.f, 1.f);
    ikEffectAnim->SetKeyFrame(1.f, 0.f);
    ikEffectAnim->SetInterpolationMethod(IM_SINUSOIDAL);
    ikEffector_->SetAttributeAnimation("Weight", ikEffectAnim, WM_ONCE);
    chillTime_ = animCtrl_->GetTime("Animations/Satong/Chill.ani");

    catNode->GetDerivedComponent<Controllable>()->Disable();
    satiation_ = Min(1.f, satiation_ + catchable->GetNourishment());

    VariantMap eventData{};
    eventData.Insert({ BugReleased::P_DEVOURED, catchable });
    eventData.Insert({ BugReleased::P_DEVOURER, this });
    catchable->SendEvent(E_BUGRELEASED, eventData);
}

void Satong::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
void Satong::Start() {}
void Satong::DelayedStart() {}
void Satong::Stop() {}

void Satong::Update(float timeStep)
{
    const GameMode mode{ GAME->GetGameMode() };
    const GameStatus status{ GAME->GetStatus() };
    if (status != GS_PLAY || (mode != GM_STORY && mode != GM_SURVIVAL))
        return;

    UpdateSatiation(timeStep);
    LeanIn(timeStep);
}

void Satong::UpdateSatiation(float timeStep)
{
    Difficulty difficulty{ GetSubsystem<Game>()->GetDifficulty() };
    float multiplier;
    switch (difficulty)
    {
    case D_NORMAL: default: multiplier = 1.f; break;
    case D_EASY: multiplier =  .5f; break;
    case D_HARD: multiplier = 1.5f; break;
    }

    satiation_ = Max(0.f, satiation_ - timeStep * multiplier * .00666f);
    DASH->SetSatiation(satiation_, team_);
}

void Satong::LeanIn(float timeStep)
{
    if (ikEffector_->GetAttributeAnimation("Weight") == nullptr)
    {
        const float radius{ 4.2f };
        if (Node* nearest{ DetectFood(radius) })
        {
            if (ikEffector_->GetWeight() == 0.f)
                chillTime_ = animCtrl_->GetTime("Animations/Satong/Chill.ani");

            const Vector3 worldPos{ node_->GetWorldPosition() };
            const Vector3 bitePos{ GetBitePos(nearest) };
            const Quaternion biteRot{ GetBiteRot(nearest) };
            ikEffector_->SetTargetPosition(Lerp(ikEffector_->GetTargetPosition(), bitePos, .5f));
            ikEffector_->SetTargetRotation(biteRot);
            ikEffector_->SetWeight(Lerp(ikEffector_->GetWeight(), PowN(Min(.5f, (radius - (bitePos - worldPos).Length()) / radius), 2u), timeStep * 17.f));
            ikEffector_->SetRotationWeight(1.f);
        }
        else
        {
            float easeWeight{ Lerp(ikEffector_->GetWeight(), 0.f, timeStep * 5.f) };
            if (easeWeight < .001f)
                easeWeight = 0.f;
            ikEffector_->SetWeight(easeWeight);
        }
    }

    if (!Equals(ikEffector_->GetWeight(), 0.f))
        animCtrl_->SetTime("Animations/Satong/Chill.ani", chillTime_);
}

Node* Satong::DetectFood(float radius)
{
    const Vector3 worldPos{ node_->GetWorldPosition() };
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    PODVector<RigidBody*> result{};
    physics->GetRigidBodies(result, Sphere{ worldPos, radius }, L_BUBBLES);
    Node* nearest{ nullptr };
    float nearestDistance{ M_INFINITY };
    for (RigidBody* rb: result)
    {
        Node* bubbleNode{ rb->GetNode() };
        const float distance{ rb->GetNode()->GetWorldPosition().DistanceToPoint(worldPos) };
        Bubble* bubble{ bubbleNode->GetComponent<Bubble>() };
        bool friendlyContents{ false };
        if (GetTeam() != TP_RANDOM && bubble->GetContents() && bubble->GetContents()->GetTeam() == GetTeam())
            friendlyContents = true;

        if (bubble && bubbleNode->IsEnabled() && !bubble->IsEmpty() && !friendlyContents)
        {
            if (distance < nearestDistance)
            {
                nearest = bubbleNode;
                nearestDistance = distance;
            }
        }
    }

    return nearest;
}

void Satong::PostUpdate(float /*timeStep*/)
{
    const GameMode mode{ GAME->GetGameMode() };
    if (team_ != TP_RANDOM && mode != GM_SURVIVAL)
        return;

    if (satiation_ == 0.f)
    {
        GAME->SetStatus(GS_GAMEOVER);
        for (bool blup: { false, true })
            DASH->SetFishConscious(blup, false);
    }
}

void Satong::OnNodeSet(Node* node)
{
    if (!node)
        return;

    model_ = node_->CreateComponent<AnimatedModel>();
    model_->SetModel(RES(Model, "Models/Satong.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutline.xml")->Clone());
    model_->SetCastShadows(true);

    animCtrl_ = node_->CreateComponent<AnimationController>();
    animCtrl_->PlayExclusive("Animations/Satong/Chill.ani", 0u, true);

    ikSolver_ = node_->GetChild("Spine02", true)->CreateComponent<IKSolver>();
    ikSolver_->SetFeature(IKSolver::UPDATE_ORIGINAL_POSE, true);
    ikSolver_->SetFeature(IKSolver::TARGET_ROTATIONS, true);

    ikEffector_ = node_->GetChild("Head", true)->CreateComponent<IKEffector>();
    ikEffector_->SetChainLength(3);
    ikEffector_->SetWeight(0.f);
    ikEffector_->SetRotationWeight(0.f);

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    rb->SetCollisionLayer(L_CHARACTERS);
    rb->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);

    node_->CreateComponent<CollisionShape>()->SetConvexHull(RES(Model, "Models/Satong_COLLISION.mdl"));
}

void Satong::SetTeam(TeamPreference team)
{
    Color diffCol;
    switch (team)
    {
    default:
    case TP_RANDOM: diffCol = Color::WHITE; break;
    case TP_BLIP:   diffCol.FromHSV(.05f, 1.f, 1.f); break;
    case TP_BLUP:   diffCol.FromHSV(.55f, 1.f, 1.f); break;
    }

    for (unsigned m{ 0u }; m < model_->GetNumGeometries(); ++m)
    {
        Material* mat{ model_->GetMaterial(m) };
        mat->SetShaderParameter("MatDiffColor", diffCol);
    }

    team_ = team;
}

void Satong::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void Satong::OnMarkedDirty(Node* node) {}
void Satong::OnNodeSetEnabled(Node* node) {}

Vector3 Satong::GetBitePos(Node* node)
{
    const Vector3 eatPos{ node->GetWorldPosition() };
    const Vector3 eatDelta{ eatPos - (node_->GetWorldPosition() + Vector3::UP) };
    return eatPos - eatDelta.Normalized() * .25f;
}

Quaternion Satong::GetBiteRot(Node* node)
{
    const Vector3 biteDirection{ (GetBitePos(node) - node_->GetWorldPosition()).Normalized() };
    Quaternion eatRot{};
    eatRot.FromLookRotation(biteDirection);
    return eatRot * Quaternion{ -60.f, biteDirection.CrossProduct(Vector3::UP).Normalized() };
}

void Satong::HandleSceneDrawableUpdateFinished(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const float effectorWeight{ ikEffector_->GetWeight() };
    if (!Equals(effectorWeight, 0.f))
    {
        Node* jawNode{ node_->GetChild("Jaw", true) };
        jawNode->SetRotation({ effectorWeight * 60.f, 180.f, 0.f  });
    }
}

bool Satong::Save(Serializer& dest) const { return LogicComponent::Save(dest); }
bool Satong::SaveXML(XMLElement& dest) const { return LogicComponent::SaveXML(dest); }
bool Satong::SaveJSON(JSONValue& dest) const { return LogicComponent::SaveJSON(dest); }
void Satong::MarkNetworkUpdate() { LogicComponent::MarkNetworkUpdate(); }
void Satong::GetDependencyNodes(PODVector<Node*>& dest) {}
void Satong::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
