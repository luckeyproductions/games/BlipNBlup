/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "fly.h"
#include "catchable.h"
#include "rage.h"

Fly::Fly(Context* context): Flyer(context)
{
    maxFlySpeed_ = 11.f;
    flyThrust_ = 235.f;

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Fly::RegisterObject(Context* context)
{
    context->RegisterFactory<Fly>();
}

void Fly::OnNodeSet(Node *node)
{
    if (!node)
        return;

    Flyer::OnNodeSet(node);

    Catchable*  catchable{ node_->CreateComponent<Catchable>() };
    catchable->SetReleaseTime(23.f);
    catchable->SetNourishment(1/16.f);
    SubscribeToEvent(node_, E_CATCH, DRY_HANDLER(Fly, OnCatched));
    SubscribeToEvent(node_, E_BUGRELEASED, DRY_HANDLER(Fly, OnReleased));

    rage_ = node_->CreateComponent<Rage>();
    rage_->SetCooldown(5.f);

    model_->SetModel(RES(Model, "Models/Fly.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/Carapace.xml")->Clone());
    model_->SetMaterial(1, RES(Material, "Materials/VColOutline.xml"));

    rigidBody_->SetCollisionLayer(L_CHARACTERS);
    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);
    rigidBody_->SetMass(1.f);
    collider_->SetSphere(2/3.f);

    animCtrl_->Play("Animations/Fly_Fly.ani", 0, true, 0.f);
    animCtrl_->SetTime("Animations/Fly_Fly.ani", randomizer_);
}

void Fly::Set(const Vector3& position, const Quaternion& rotation)
{
    Flyer::Set(position, rotation);

    flyHeight_ = 3.f;
    rage_->ResetAnger();

    if (GAME->GetGameMode() != GM_CTF)
        GetComponent<Catchable>()->SetTeam(TP_RANDOM);
}

void Fly::Update(float timeStep)
{
//    if (GAME->GetGameMode() != GM_CTF)
//        move_ = Quaternion{ 5.f - 2.f * rage_->GetAnger(), Vector3::UP } * node_->GetDirection(); ///Should be handled by AI component

    Flyer::Update(timeStep);

    animCtrl_->SetSpeed("Animations/Fly_Fly.ani", 2.f + .3f * rigidBody_->GetLinearVelocity().Length());
    model_->GetMaterial()->SetShaderParameter("MatEmissiveColor", Color::RED * MC->Sine(2.3f, 0.f, .5f * rage_->GetAnger(), randomizer_));

    maxFlySpeed_ = 13.f + 7.f * rage_->GetAnger();
    flyThrust_ = 500.f + 200.f * rage_->GetAnger();
}

void Fly::OnCatched(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    animCtrl_->PlayExclusive("Animations/Fly_Caught.ani", 1u, true, .1f);
}

void Fly::OnReleased(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    rage_->FillAnger();

    animCtrl_->StopLayer(1u);
    animCtrl_->PlayExclusive("Animations/Fly_Fly.ani", 0, true, .1f);
    animCtrl_->SetSpeed("Animations/Fly_Fly.ani", 5.f);
}
