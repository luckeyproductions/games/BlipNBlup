/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COUCH_H
#define COUCH_H

#include "gamedefs.h"

#include "luckey.h"

#define COUCH GetSubsystem<Couch>()

class Controllable;
class BnBCam;

class Player: public RefCounted
{
public:
    Player(unsigned id = 0);
    ~Player();

    unsigned GetPlayerID() const { return id_; }
    unsigned GetJoystickID() const { return joystickId_; }
    void SetJoystickID(unsigned id) { joystickId_ = id; }

    void SetControl(Controllable* controllable, bool easeCamera = true);
    Controllable* GetControllable() const { return controllable_; }
    BnBCam* GetCamera() const { return camera_; }

    bool IsNonePlayer() const { return id_ == 0u; }
    TeamPreference GetTeamPreference() const { return preferredTeam_; }

private:
    unsigned id_;
    unsigned joystickId_;
    TeamPreference preferredTeam_;
    Controllable* controllable_;
    BnBCam* camera_;
};

class Couch: public Object
{
    DRY_OBJECT(Couch, Object);

public:
    Couch(Context* context);

    Player* AddPlayer();
    bool RemovePlayer(unsigned id);

    Player* GetPlayer(unsigned id) const;
    PODVector<Player*> GetPlayers() const;
    PODVector<Player*> GetControllingPlayers() const;
    unsigned GetNumPlayers() const { return players_.Size(); }
    unsigned GetNumControllingPlayers() const;

private:
    HashMap<unsigned, SharedPtr<Player>> players_;
};

#endif // COUCH_H
