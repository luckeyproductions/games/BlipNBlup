/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "base.h"
#include "catchable.h"
#include "flyer.h"

#include "bugbrain.h"

BugBrain::BugBrain(Context* context): LogicComponent(context),
    controllable_{ nullptr },
    catchable_{ nullptr },
    spawnPos_{},
    path_{}
{
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(BugBrain, HandlePostRenderUpdate));
}

void BugBrain::Update(float timeStep)
{
    catchable_ = node_->GetComponent<Catchable>();

    switch (GAME->GetGameMode())
    {
    default: return;
    case GM_STORY: case GM_SURVIVAL: ThinkStory(timeStep * 5.f); break;
    case GM_CTF:   ThinkCTF  (timeStep * 5.f);   break;
    }
}

void BugBrain::OnNodeSet(Node* node)
{
    if (!node)
        return;

    controllable_ = node_->GetDerivedComponent<Controllable>();
}

void BugBrain::FollowPath(float timeStep)
{
    const Vector3 worldPos{ GetNode()->GetWorldPosition() };
    const Vector3 flatDir{ node_->GetWorldDirection().ProjectOntoPlane(Vector3::UP).Normalized() };
    const Vector3 firstPoint{ path_.Front() };
    const Vector3 toPath{ firstPoint - worldPos };
    const Vector3 flatToPath{ toPath.ProjectOntoPlane(Vector3::UP) };
    const Vector3 v{ GetComponent<RigidBody>()->GetLinearVelocity().ProjectOntoPlane(Vector3::UP) };

    controllable_->SetMove(controllable_->GetMove().Lerp((flatToPath.Normalized() - v * .125f).Normalized() + AvoidWall(), timeStep));

    if (flatDir.DotProduct(flatToPath) > -GetNavMesh()->GetAgentRadius())
    {
        if (flatDir.DistanceToPoint(flatToPath) < 2.3f)
            path_.Erase(0u);
    }
}

Vector3 BugBrain::AvoidWall()
{
    Vector3 avoid{};
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    if (!physics)
        return avoid;

    const Vector3 v{ GetComponent<RigidBody>()->GetLinearVelocity().ProjectOntoPlane(Vector3::UP) };
    const float distance{ 5.f };
    for (bool right: { true, false })
    {
        Ray wallRay{ node_->GetWorldPosition(), (right ? 1.f : -1.f) * (node_->GetWorldRight() + v.Normalized()) };
        PhysicsRaycastResult result{};
        if (physics->SphereCast(result, wallRay, 1.f, distance, L_WORLD))
        {
            if (result.distance_ < 2.f && result.normal_.DotProduct(node_->GetWorldDirection()) < -.5f)
                path_.Clear();
            else
                avoid -= wallRay.direction_ * Sqrt(Max(0.f, distance - result.distance_)) * .17f * v.Length();
        }
    }

    return avoid;
}

void BugBrain::ThinkStory(float timeStep)
{
    if (!controllable_ || (catchable_ && catchable_->IsCaught()))
    {
        path_.Clear();
        return;
    }

    const Vector3 flatDir{ node_->GetWorldDirection().ProjectOntoPlane(Vector3::UP).Normalized() };
    NavigationMesh* navMesh{ GetNavMesh() };

    if (!navMesh)
        return;

    const Vector3 worldPos{ node_->GetWorldPosition() };
    const Vector3 flatToMesh{ (navMesh->FindNearestPoint(worldPos) - worldPos).ProjectOntoPlane(Vector3::UP) };
    if ((path_.IsEmpty() ? flatToMesh.Length() > .05f
         : flatToMesh.Normalized().DotProduct((path_.Front() - worldPos).Normalized()) < -M_1_SQRT2))
        path_.Clear();

    if (path_.Size())
    {
        FollowPath(timeStep);
    }
    else
    {
        PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
        if (!physics)
            return;

        PhysicsRaycastResult result{};
        const Ray groundRay{ worldPos, Vector3::DOWN };
        if (physics->RaycastSingle(result, groundRay, 50.f, L_WORLD))
        {
            Vector3 groundPos{ result.position_ };

            const Vector3 xDir{ flatDir.ProjectOntoPlane(Vector3::FORWARD) };
            const Vector3 zDir{ flatDir.ProjectOntoPlane(Vector3::RIGHT) };
            const Vector3 orthoDir{ (Abs(xDir.x_) > Abs(zDir.z_) ? xDir.Normalized() : zDir.Normalized()) };
            physics->RaycastSingle(result, { node_->GetWorldPosition() + orthoDir * 2.f, orthoDir }, 23.f, L_WORLD);
            Vector3 flatDelta{ (result.position_ - groundPos).ProjectOntoPlane(Vector3::UP) };

            if (result.body_ && flatDelta.Length() > 7.f)
                navMesh->FindPath(path_, groundPos, result.position_.ProjectOntoPlane(Vector3::UP, groundPos));

            if (!path_.IsEmpty())
                return;

            physics->RaycastSingle(result, { node_->GetWorldPosition() - orthoDir * 2.f, -orthoDir }, 23.f, L_WORLD);
            flatDelta = (result.position_ - node_->GetWorldPosition()).ProjectOntoPlane(Vector3::UP);

            if (result.body_ && flatDelta.Length() > 7.f)
                navMesh->FindPath(path_, groundPos, result.position_.ProjectOntoPlane(Vector3::UP, groundPos));

            if (!path_.IsEmpty())
                return;

            navMesh->FindPath(path_, groundPos, groundPos + Quaternion{ Random(360.f), Vector3::UP } * Vector3::FORWARD * 7.f);
        }
        else
        {
            path_ = { navMesh->FindNearestPoint(node_->GetWorldPosition()) };
        }
    }
}

void BugBrain::ThinkCTF(float timeStep)
{
    Controllable* con{ GetControllable() };
    Catchable* cat{ GetComponent<Catchable>() };
    if (!con)
        return;

    Arcade* a{ GetSubsystem<Arcade>() };
    if (!a)
        return;

    const Team& team{ (cat == a->GetTeamBlip().bug_ ? a->GetTeamBlip() : a->GetTeamBlup() ) };
    if (!team.base_)
        return;

    const Vector3 worldPos{ GetNode()->GetWorldPosition() };
    const Vector3 basePos{ team.base_->GetNode()->GetWorldPosition() };
    const Vector3 flatDelta{ (basePos - worldPos).ProjectOntoPlane(Vector3::UP) };
    const Vector3 v{ GetComponent<RigidBody>()->GetLinearVelocity() };

    if ((!cat || !cat->IsCaught()) && flatDelta.Length() > 5.f)
    {
        NavigationMesh* navMesh{ GetNavMesh() };
        const float agentRadius{ navMesh->GetAgentRadius() };
        const float agentHeight{ navMesh->GetAgentHeight() };

        if (path_.IsEmpty() || navMesh->GetDistanceToWall(worldPos, agentRadius * 3.f) < agentRadius * 2.f)
        {
            if (navMesh)
                navMesh->FindPath(path_, worldPos, basePos, { .5f * agentRadius, 1.5f * agentHeight, .5f * agentRadius });
        }
        else
        {
           FollowPath(timeStep);
        }
    }
    else
    {
        path_.Clear();
        const Vector3 sway{ MC->Sine(.23f, -.23f, .23f), MC->Sine(.17f, -.025f, .025f), MC->Sine(.34f, -.23f, .23f) };
        con->SetMove(flatDelta + sway - v.Normalized() * v.LengthSquared() * .01f);
    }
}

NavigationMesh* BugBrain::GetNavMesh() const
{
    const World& world{ GAME->GetWorld() };
    return (IsFlyer() ? world.flyNav_ : world.walkNav_);
}

void BugBrain::OnNodeSetEnabled(Node* /*node*/)
{
    if (node_->IsEnabled())
    {
        spawnPos_ = node_->GetWorldPosition();
        path_.Clear();
    }
}

bool BugBrain::IsFlyer() const
{
    return node_->GetDerivedComponent<Flyer>() != nullptr;
}

void BugBrain::HandlePostRenderUpdate(StringHash eventType, VariantMap& evenData)
{
    if (path_.IsEmpty())
        return;

    Vector3 from{ GetNode()->GetWorldPosition() };
    for (unsigned p{ 0u }; p < path_.Size(); ++p)
    {
        const Vector3 to{ path_.At(p) };
        GetScene()->GetComponent<DebugRenderer>()->AddLine(from, to, Color::ORANGE, false);
        from = to;
    }
}
