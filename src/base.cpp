/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gamedefs.h"
#include "block.h"

#include "base.h"

Base::Base(Context* context): SceneObject(context),
    model_{ nullptr },
    collider_{ nullptr }
{
}


void Base::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

    model_ = node_->CreateComponent<StaticModel>();

    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
    rigidBody->SetCollisionLayer(L_WORLD);
    rigidBody->SetFriction(BLOCK_DEFAULTFRICTION);
    rigidBody->SetRestitution(BLOCK_DEFAULTRESTITUTION);
    collider_ = node_->CreateComponent<CollisionShape>();
}

void Base::Initialize(Model* model, const PODVector<Material*>& materials)
{

    SharedPtr<Model> collisionModel{ nullptr };
    if (model)
    {
        String colliderName{ model->GetName() };
        colliderName.Insert(colliderName.Length() - 4, "_COLLISION");
        collisionModel = RES_NO(Model, colliderName);

        if (!collisionModel)
            collisionModel = model;
    }

    collider_->SetTriangleMesh(collisionModel/*, 0, Vector3::ONE,
                               node_->GetPosition(),
                               node_->GetRotation()*/);

    model_->SetModel(model);
    for (unsigned m{ 0u }; m < materials.Size(); ++m)
        model_->SetMaterial(m, materials.At(m)->Clone());


}

void Base::SetTeam(TeamPreference team)
{
    Color diffCol;
    switch (team)
    {
    default:
    case TP_RANDOM: diffCol.FromHSV(.34f, .8f, .7f); break;
    case TP_BLIP:   diffCol.FromHSV(.05f, .9f, .8f); break;
    case TP_BLUP:   diffCol.FromHSV(.55f, .9f, .7f); break;
    }
    Color emitCol;
    switch (team)
    {
    default:
    case TP_RANDOM: emitCol.FromHSV(.27f, .8f, .4f); break;
    case TP_BLIP:   emitCol.FromHSV(.17f, .9f, .5f); break;
    case TP_BLUP:   emitCol.FromHSV(.48f, .7f, .6f); break;
    }

    for (unsigned m{ 0u }; m < model_->GetNumGeometries(); ++m)
    {
        Material* mat{ model_->GetMaterial(m) };
        mat->SetShaderParameter("MatDiffColor", diffCol.Lerp(Color::BLACK, .5f * m));
        mat->SetShaderParameter("MatEmissiveColor", emitCol * m);
    }
}
