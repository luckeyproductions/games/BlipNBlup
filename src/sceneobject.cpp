/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "sceneobject.h"

SceneObject::SceneObject(Context* context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::OnNodeSet(Node* node)
{
    if (!node)
        return;

    LogicComponent::OnNodeSet(node);
}

void SceneObject::Set(const Vector3& position, const Quaternion& rotation)
{
    node_->SetWorldPosition(position);
    node_->SetWorldRotation(rotation);
    node_->SetEnabledRecursive(true);

    randomizer_ = Random();
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::PlaySample(String sampleName, float frequency, float gain, bool onNode)
{
    Node* node{ (onNode ? node_ : GetScene()->CreateChild("Sample")) };
    if (!onNode)
        node->SetWorldPosition(node_->GetWorldPosition());

    SoundSource3D* source{ node->CreateComponent<SoundSource3D>() };
    SharedPtr<Sound> sample{ RES(Sound, String{ "Samples/" } + sampleName) };
    source->SetAngleAttenuation(666.f, 666.f);
    source->SetAutoRemoveMode((onNode ? REMOVE_COMPONENT : REMOVE_NODE));
    source->Play(sample, sample->GetFrequency() * frequency, gain);
}
