/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"

#include "dash.h"

void Dash::RegisterObject(Context* context)
{
    context->RegisterFactory<Dash>();

    DRY_COPY_BASE_ATTRIBUTES(UIElement);
}

Dash::Dash(Context* context): UIElement(context),
    fishStatus_{ nullptr, nullptr },
    starveBar_{ nullptr },
    scoreBoard_{ nullptr, nullptr },
    starveBoard_{ nullptr, nullptr }
{
    SetHorizontalAlignment(HA_CENTER);
    CreateFishStatusIcons();
    starveBar_ = CreateStarveBar();
    CreateScoreBoard();
    UpdateSizes();

    SetVisible(false);

    SubscribeToEvent(E_GAMEMODECHANGED, DRY_HANDLER(Dash, HandleGameModeChanged));
    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(Dash, HandleGameStatusChanged));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Dash, HandleUpdate));
}

void Dash::CreateFishStatusIcons()
{
    for (bool blup: { false, true })
    {
        BorderImage* icon{ CreateChild<BorderImage>("FishIcon") };
        icon->SetTexture(RES(Texture2D, "UI/FishStatus.png"));
        icon->SetImageRect({ 0, 256 * blup, 256, 256 * (1 + blup) });
        icon->SetAlignment(HA_LEFT, VA_TOP);
        icon->SetBlendMode(BLEND_ALPHA);

        if (!blup)
            fishStatus_.first_ = icon;
        else
            fishStatus_.second_ = icon;
    }
}

ProgressBar* Dash::CreateStarveBar(bool left, bool colorize)
{
    const HorizontalAlignment ha{ (left ? HA_LEFT : HA_RIGHT) };
    ProgressBar* starveBar = CreateChild<ProgressBar>("StarveBar");
    starveBar->SetShowPercentText(false);
    starveBar->SetAlignment(ha, VA_TOP);
    starveBar->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    starveBar->SetRange(1.f);
    starveBar->SetColor(Color::WHITE.Lerp(Color::BLACK, .5f));
    starveBar->GetKnob()->SetBlendMode(BLEND_ADDALPHA);
    if (left)
        starveBar->GetKnob()->SetHorizontalAlignment(HA_RIGHT);
    BorderImage* icon{ starveBar->CreateChild<BorderImage>("SatongIcon") };
    icon->SetTexture(RES(Texture2D, "UI/Satong.png"));
    icon->SetFullImageRect();
    icon->SetAlignment(ha, VA_CENTER);
    icon->SetBlendMode(BLEND_ALPHA);
    if (colorize)
        icon->SetColor((left ? Color::RED : Color::AZURE).Lerp(Color::WHITE, .5f));

    return starveBar;
}

void Dash::CreateScoreBoard()
{
    for (bool blips: { true, false })
    {
        Text* counter{ CreateChild<Text>() };
        counter->SetFont(FONT_HAPPY_MEMORIES);

        HorizontalAlignment ha{ (blips ? HA_LEFT : HA_RIGHT) };
        counter->SetTextAlignment(ha);
        counter->SetHorizontalAlignment(HA_CENTER);

        Color textCol;
        if (blips)
        {
            textCol.FromHSV(.0125f, .9f, .9f);
            scoreBoard_.first_ = counter;
        }
        else
        {
            textCol.FromHSV(.55f, .9f, .8f);
            scoreBoard_.second_ = counter;
        }
        counter->SetColor( textCol );
        counter->SetTextEffect(TE_STROKE);
        counter->SetEffectColor(textCol.Lerp(Color::BLACK, .8f));

        if (blips)
            starveBoard_.first_  = CreateStarveBar(true, true);
        else
            starveBoard_.second_ = CreateStarveBar(false, true);
    }

    SetScore(0, 0);
}

Color Dash::SatiationToColor(float satiation)
{
    Color col;
    col.FromHSV(Min(1/3.f, PowN(satiation * 1.125f, 2) / 3), 1.f, 1.f);
    return col;
}

void Dash::SetFishConscious(bool blup, bool conscious)
{
    BorderImage* fishIcon{ (!blup ? fishStatus_.first_ : fishStatus_.second_) };

    fishIcon->SetImageRect({ 256 * !conscious, 256 * blup, 256 + 256 * !conscious, 256 * (1 + blup) });
}

void Dash::ResetFishConsciousness()
{
    for (bool blup: { false, true })
        SetFishConscious(blup, true);
}

void Dash::ResetFade()
{
    RemoveAttributeAnimation("Opacity");
    SetOpacity(1.f);
}

void Dash::SetSatiation(float satiation, TeamPreference team)
{
    switch (team) {
    default: case TP_RANDOM: starveBar_->SetValue(satiation); break;
    case TP_BLIP: starveBoard_.first_  ->SetValue(satiation); break;
    case TP_BLUP: starveBoard_.second_ ->SetValue(satiation); break;
    }
}

void Dash::SetScore(int blipScore, int blupScore)
{
    if (scoreBoard_.first_)
        scoreBoard_.first_->SetText(String{ blipScore });
    if (scoreBoard_.second_)
        scoreBoard_.second_->SetText(String{ blupScore });
}

void Dash::UpdateStarveBarColor(ProgressBar* starveBar)
{
    const float satiation{ starveBar->GetValue() };
    const float darkness{ (GAME->GetStatus() == GS_PAUSED || satiation > 1/5.f
                ? 0.f
                : MC->Sine((2.f - 3.f * satiation), 0.f, .875f - 2.f * satiation )) };

    starveBar->GetKnob()->SetColor(SatiationToColor(satiation).Lerp(Color::BLACK, darkness));
}

void Dash::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const GameMode mode{ GAME->GetGameMode() };
    if (mode == GM_STORY)
    {
        UpdateStarveBarColor(starveBar_);
    }
    else if (mode == GM_SURVIVAL)
    {
        UpdateStarveBarColor(starveBoard_.first_);
        UpdateStarveBarColor(starveBoard_.second_);
    }
}

void Dash::HandleGameModeChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameMode mode{ static_cast<GameMode>(eventData[GameModeChanged::P_MODE].GetInt()) };
    bool arcade{ mode > GM_STORY};

    for (BorderImage* fishIcon: { fishStatus_.first_, fishStatus_.second_ })
        fishIcon->SetVisible(!arcade);

    starveBar_->SetVisible(!arcade);

    for (Text* counter: { scoreBoard_.first_, scoreBoard_.second_ })
    {
        if (counter)
            counter->SetVisible(arcade && mode != GM_SURVIVAL);
    }
    for (ProgressBar* starveBar: { starveBoard_.first_, starveBoard_.second_ })
    {
        if (starveBar)
            starveBar->SetVisible(arcade && mode == GM_SURVIVAL);
    }

    UpdateSizes();
}

void Dash::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameMode mode{ GAME->GetGameMode() };
    if (mode != GM_STORY)
        return;

    const GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };
    for (UIElement* elem: PODVector<UIElement*>{ fishStatus_.first_, fishStatus_.second_, starveBar_ })
        elem->SetVisible(status == GS_PLAY);
}

void Dash::UpdateSizes()
{
    if (!GAME)
        return;

    const float yScale{ 1.f / Max(1, MC->GetYSectors()) };

    SetPosition(0, GUI::VHInt(1.7f * yScale));
    SetSize({ GUI::sizes_.screen_.x_, GUI::VHInt(17.f * yScale) });

    GameMode mode{ GAME->GetGameMode() };
    if (mode == GM_STORY)
    {
        for (bool blup: { false, true })
        {
            const int fishIconSize{ GUI::VMinInt(8.f * yScale) };
            BorderImage* fishIcon{ (!blup ? fishStatus_.first_ : fishStatus_.second_) };
            fishIcon->SetSize(fishIconSize, fishIconSize);
            fishIcon->SetPosition(GUI::VWInt(1.f) + blup * 1.1f * fishIconSize, GUI::VHInt(.5f * yScale));
        }

        UpdateStarveBarSize(starveBar_, yScale);
    }
    else
    {
        for (Text* counter: { scoreBoard_.first_, scoreBoard_.second_ })
        {
            const bool blipScore{ scoreBoard_.first_ == counter};
            if (counter)
            {
                counter->SetFontSize(GUI::VMin(11.f * yScale));
                const int offcenter{ GUI::VWInt(23.f * yScale) };
                counter->SetPosition({ (blipScore ? -offcenter : offcenter), 0 });
                counter->SetEffectStrokeThickness(Ceil(GUI::VMinInt(.5f * yScale)));
            }
        }

        for (ProgressBar* starveBar: { starveBoard_.first_, starveBoard_.second_ })
        {
            if (starveBar)
                UpdateStarveBarSize(starveBar, yScale);
        }
    }
}

void Dash::UpdateStarveBarSize(ProgressBar* bar, float yScale)
{
    const bool left{ (bar->GetHorizontalAlignment() == HA_LEFT ? true : false) };
    bar->SetPosition(GUI::VWInt(left ? 2.3f : -2.3f), GUI::VHInt(1.f * yScale));
    bar->SetSize(GUI::VWInt(23.f), GUI::VMinInt(7.f * yScale));

    const int satongIconSize{ GUI::VMinInt(11.f * yScale) };
    BorderImage* satongIcon{ bar->GetChildStaticCast<BorderImage>(String{ "SatongIcon" }) };
    satongIcon->SetSize(satongIconSize, satongIconSize);
    satongIcon->SetPosition((left ? -1 : 1) * (-bar->GetWidth() + satongIconSize / 4), 0);
}
