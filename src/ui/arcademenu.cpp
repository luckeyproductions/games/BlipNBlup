/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"
#include "sidepicker.h"
#include "panelpicker.h"

#include "arcademenu.h"

ArcadeMenu::ArcadeMenu(Context* context): GameMenu(context),
    modePicker_{ nullptr },
    levelPicker_{ nullptr }
{
}

void ArcadeMenu::CreateButtons()
{
    modePicker_  = CreateChild<SidePicker>();
    modePicker_->SetOptions({ "Capture the bug", "Soccer", "Collector", "Survival" });
    PODVector<Button*> modePickerArrows{ modePicker_->GetArrows() };

    levelPicker_ = CreateChild<PanelPicker>();
    PODVector<Button*> levelPickerArrows{ levelPicker_->GetArrows() };

    for (int b{ 0 }; b < AMB_ALL; ++b)
    {
        String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Right", button);
            button->SetVar("Left", buttons_[b - 1]);
        }

        if (b == AMB_ALL - 1)
        {
            button->SetVar("Right", buttons_[0]);
            buttons_[0]->SetVar("Left", button);
        }
    }

    modePickerArrows.Front()->SetVar("Prev", buttons_[AMB_BACK]);
    modePickerArrows.Back() ->SetVar("Prev", buttons_[AMB_START]);
    buttons_[AMB_BACK] ->SetVar("Next", modePickerArrows.Front());
    buttons_[AMB_START]->SetVar("Next", modePickerArrows.Back());

    lastElem_  = buttons_[AMB_START];
    firstElem_ = buttons_[AMB_START];

    SubscribeToEvent(buttons_[AMB_START], E_CLICKEND, DRY_HANDLER(ArcadeMenu, HandleStartButtonClicked));
    SubscribeToEvent(buttons_[AMB_BACK],  E_CLICKEND, DRY_HANDLER(ArcadeMenu, HandleBackButtonClicked));
    SubscribeToEvent(modePicker_, E_ITEMSELECTED, DRY_HANDLER(ArcadeMenu, HandleGameModePicked));
    for (Button* a: modePickerArrows)
        SubscribeToEvent(a, E_CLICK, DRY_HANDLER(ArcadeMenu, HandleButtonClicked));
    SubscribeToEvent(levelPicker_, E_ACTIVEPANELCHANGED, DRY_HANDLER(ArcadeMenu, HandleActivePanelChanged));
    SubscribeToEvent(E_FOCUSCHANGED, DRY_HANDLER(ArcadeMenu, HandleFocusChanged));

    UpdateSizes();
}

void ArcadeMenu::UpdatePanelPicker()
{
    if (!levelPicker_)
        return;

    Arcade* arcade{ GetSubsystem<Arcade>() };
    if (!arcade)
        return;

    const String mapsDir{ "Maps/" };
    const GameMode mode{ static_cast<GameMode>(modePicker_->GetIndex() + GM_CTF) };
    StringVector allMaps{};
    StringVector filteredMaps{};
    for (const String& resourceDir: CACHE->GetResourceDirs())
    {
        FILES->ScanDir(allMaps, resourceDir + mapsDir, "*.emp", SCAN_FILES, true);
    }

    for (const String& mapName: allMaps)
    {
        XMLFile* mapFile{ RES(XMLFile, mapsDir + mapName) };
        switch (mode)
        {
        default: break;
        case GM_CTF:       if (arcade->IsCaptureMap(mapFile))   filteredMaps.Push(mapName); break;
        case GM_SOCCER:    if (arcade->IsSoccerMap(mapFile))    filteredMaps.Push(mapName); break;
        case GM_COLLECTOR: if (arcade->IsCollectorMap(mapFile)) filteredMaps.Push(mapName); break;
        case GM_SURVIVAL:  if (arcade->IsSurvivalMap(mapFile))  filteredMaps.Push(mapName); break;
        }
    }

//    Log::Write(LOG_INFO, "Found " + String{ filteredMaps.Size() } + " fitting maps");

    levelPicker_->SetNumPanels(filteredMaps.Size());
    levelPicker_->UpdateSizes();
    for (unsigned p{ 0u }; p < levelPicker_->GetPanels().Size(); ++p)
    {
        Panel* panel{ levelPicker_->GetPanel(p) };
        const String trimmedMapName{ GetFileName(filteredMaps.At(p)) };
        panel->SetTitle(trimmedMapName);
        panel->SetPreview(nullptr);
        panel->SetVar("Item", filteredMaps.At(p));
        panel->SetVar("Next", buttons_[AMB_START]);
        panel->SetVar("Prev", modePicker_->GetArrows().Back());
    }

    Panel* activePanel{ levelPicker_->GetActivePanel() };
    if (activePanel)
    {
        SetNextPrevPanel(activePanel);
    }
    else
    {
        const PODVector<Button*> modeArrows{modePicker_->GetArrows()};
        modeArrows.Front()->SetVar("Next", buttons_[AMB_BACK] );
        modeArrows.Back() ->SetVar("Next", buttons_[AMB_START]);
        buttons_[AMB_START]->SetVar("Prev", modeArrows.Back());
        buttons_[AMB_BACK] ->SetVar("Prev", modeArrows.Front());
    }
}

void ArcadeMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    if (modePicker_)
    {
        modePicker_->UpdateSizes();
        modePicker_->SetPosition(0, GUI::VMin(42));
    }
    if (levelPicker_)
    {
        levelPicker_->UpdateSizes();
        levelPicker_->SetPosition(0, GUI::VMin(50));
    }

    for (int b{ 0 }; b < AMB_ALL; ++b)
    {
        Button* button{ buttons_[b] };
        if (!button)
            continue;

        const float buttonHeight{ GUI::VMin(10) };
        button->SetSize(Min(GUI::VW(36), GUI::VMin(65)), buttonHeight);
        bool left{ b == AMB_BACK };
        button->SetPosition(RoundToInt((GUI::VW(42)) * (.5f - left)),
                            GUI::VH(90) - .5f * buttonHeight);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, -fontSize / 10);
        buttonText->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
    }
}

void ArcadeMenu::HandleStartButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData) || !modePicker_ || !levelPicker_ || levelPicker_->GetNumPanels() == 0u)
        return;

    Panel* activePanel{ levelPicker_->GetActivePanel() };
    if (!activePanel)
        return;

    const GameMode mode{ static_cast<GameMode>(modePicker_->GetIndex() + GM_CTF) };
    const String map{ activePanel->GetVar("Item").ToString() };
    if (map.IsEmpty())
        return;

    GetSubsystem<Game>()->StartArcade(mode, map);
    GetSubsystem<GUI>()->Show(nullptr);
}

void ArcadeMenu::Back()
{
    GameMenu::Back();
    GetSubsystem<GUI>()->ShowMain();
}

void ArcadeMenu::HandleVisibleChanged(StringHash eventType, VariantMap& eventData)
{
    GameMenu::HandleVisibleChanged(eventType, eventData);

    if (IsVisible())
        UpdatePanelPicker();
}

void ArcadeMenu::SetNextPrevPanel(UIElement* panel)
{
    if (!panel)
        return;

    for (Button* a: modePicker_->GetArrows())
        a->SetVar("Next", panel);
    for (Button* b: buttons_.Values())
        b->SetVar("Prev", panel);
}

void ArcadeMenu::HandleActivePanelChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    Panel* panel{ static_cast<Panel*>(eventData[ActivePanelChanged::P_ELEMENT].GetPtr()) };
    SetNextPrevPanel(panel);
}

void ArcadeMenu::HandleFocusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    Panel* panel{ static_cast<Panel*>(eventData[FocusChanged::P_ELEMENT].GetPtr()) };
    const PODVector<Panel*> panels{ levelPicker_->GetPanels() };
    const unsigned numCells{ levelPicker_->GetNumCells() };

    if (panels.Contains(panel))
    {
        const unsigned panelIndex{ panels.IndexOf(panel) };
        const unsigned panelPage{ panelIndex / numCells };
        if (panelPage != levelPicker_->GetPage())
        {
            levelPicker_->SetPage(panelPage, false);
            GetSubsystem<UI>()->SetFocusElement(panel);
        }
        SetNextPrevPanel(panel);
    }
}

void ArcadeMenu::HandleGameModePicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdatePanelPicker();
}
