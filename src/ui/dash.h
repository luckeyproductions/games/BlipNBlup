/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DASH_H
#define DASH_H

#include "gui.h"

#define DASH GetSubsystem<Dash>()

class Dash: public UIElement
{
    DRY_OBJECT(Dash, UIElement);

public:
    static void RegisterObject(Context* context);
    Dash(Context* context);

    void SetSatiation(float satiation, TeamPreference team = TP_RANDOM);
    void SetScore(int blipScore, int blupScore = 0);
    void UpdateSizes();
    void SetFishConscious(bool blup, bool conscious);
    void ResetFishConsciousness();
    void ResetFade();

private:
    void CreateFishStatusIcons();
    ProgressBar* CreateStarveBar(bool left = false, bool colorize = false);
    void CreateScoreBoard();
    void UpdateStarveBarSize(ProgressBar* bar, float yScale = 1.f);
    void UpdateStarveBarColor(ProgressBar* starveBar);

    Color SatiationToColor(float satiation);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleGameModeChanged(StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);

    Pair<BorderImage*, BorderImage*> fishStatus_;
    ProgressBar* starveBar_;
    Pair<Text*, Text*> scoreBoard_;
    Pair<ProgressBar*, ProgressBar*> starveBoard_;
};

#endif // DASH_H
