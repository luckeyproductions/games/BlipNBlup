/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "splashscreen.h"
#include "mainmenu.h"
#include "storymenu.h"
#include "arcademenu.h"
#include "sidepicker.h"
#include "panelpicker.h"
#include "ingamemenu.h"
#include "dash.h"
#include "overlay.h"
#include "../game.h"
#include "../jukebox.h"

#include "gui.h"

String GUI::SecondsToTimeCode(float time, bool hundredths)
{
    int minutes{};
    int seconds{};

    while (time >= 60.f)
    {
        time -= 60.f;
        ++minutes;
    }
    while (time >= 1.f)
    {
        time -= 1.f;
        ++seconds;
    }

    String secondsString{ String{ seconds } };
    if (seconds < 10)
        secondsString.Insert(0, '0');

    String timeCode{ String{ minutes } + ":" + secondsString};

    if (hundredths)
    {
        int hundredths{};
        while (time >= (.01f - M_EPSILON))
        {
            time -= .01f;
            ++hundredths;
        }

        String hundredthsString{ String{ hundredths } };
        if (hundredths < 10)
            hundredthsString.Insert(0, '0');

        timeCode.Append(":" + hundredthsString);
    }

    return timeCode;
}

void GUI::RegisterObject(Context* context)
{
    context->RegisterFactory<SplashScreen>();
    context->RegisterFactory<MainMenu>();
    context->RegisterFactory<StoryMenu>();
    context->RegisterFactory<ArcadeMenu>();
    context->RegisterFactory<SidePicker>();
    PanelPicker::RegisterObject(context);
    context->RegisterFactory<InGameMenu>();
    Dash::RegisterObject(context);
    Overlay::RegisterObject(context);
}

UISizes GUI::sizes_{};

GUI::GUI(Context* context): Object(context),
    blackness_{ nullptr },
    centerText_{ nullptr },
    overlay_{ nullptr },
    nonDash_{ nullptr },
    splash_{ nullptr },
    mainMenu_{ nullptr },
    storyMenu_{ nullptr },
    arcadeMenu_{ nullptr },
//    settingsMenu_{ nullptr },
    inGameMenu_{ nullptr },
    dash_{ nullptr }
{
    UIElement* root{ GetSubsystem<UI>()->GetRoot() };
    blackness_ = root->CreateChild<BorderImage>();
    blackness_->SetColor(Color::BLACK);
    CreateCenterText();
    nonDash_ = root->CreateChild<UIElement>();
    overlay_ = root->CreateChild<Overlay>();

    CreateNonDashElements();

    dash_ = root->CreateChild<Dash>();
    context_->RegisterSubsystem(dash_);

    SetMouseVisible(false);
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(GUI, HandleScreenMode));
    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(GUI, HandleGameStatusChanged));
}

void GUI::FadeOut(float duration)
{
    SetBlackness(0.f);
    ValueAnimation* blackOpacity{ new ValueAnimation{ context_ } };
    blackOpacity->SetKeyFrame(0.f, 0.f);
    blackOpacity->SetKeyFrame(duration, 1.f);
    blackness_->SetAttributeAnimation("Opacity", blackOpacity, WM_ONCE);

    ValueAnimation* dashOpacity{ new ValueAnimation{ context_ } };
    dashOpacity->SetKeyFrame(duration * .5f, 1.f);
    dashOpacity->SetKeyFrame(duration * 2.f, 0.f);
    dash_->SetAttributeAnimation("Opacity", dashOpacity, WM_ONCE);

    overlay_->PlayGameOverSequence();

    const float textAnimLength{ overlay_->GetAttributeAnimation("Opacity")->GetEndTime() * .95f };
    ValueAnimation* showText{ new ValueAnimation{ context_ } };
    showText->SetKeyFrame(0.f, false);
    showText->SetKeyFrame(textAnimLength, true);
    centerText_->SetAttributeAnimation("Is Visible", showText, WM_CLAMP);

    const GameMode mode{ GAME->GetGameMode() };
    switch (mode) {
    default:
    {
        centerText_->SetText("Game Over");
        centerText_->SetColor({ .95f });
        centerText_->SetEffectColor({ .23f });
    }
    break;
    case GM_SURVIVAL:
    {
        TeamPreference winner{ GetSubsystem<Arcade>()->GetWinner() };
        switch (winner)
        {
        default:
        {
            centerText_->SetText("Draw");
            centerText_->SetColor({ .95f });
            centerText_->SetEffectColor({ .23f });
        }
        break;
        case TP_BLIP:
        {
            centerText_->SetText("Blips won!");
            centerText_->SetColor(Color::RED);
            centerText_->SetEffectColor(Color::RED.Lerp(Color::BLACK, .23f));
        }
        break;
        case TP_BLUP:
        {
            centerText_->SetText("Blups won!");
            centerText_->SetColor(Color::AZURE);
            centerText_->SetEffectColor(Color::BLUE.Lerp(Color::BLACK, .23f));
        }
        break;
        }
    }
    break;
    }
}

void GUI::CreateNonDashElements()
{
    nonDash_->SetHorizontalAlignment(HA_CENTER);
    splash_     = nonDash_->CreateChild<SplashScreen>("Splash");
    mainMenu_   = nonDash_->CreateChild<MainMenu>();
    mainMenu_->CreateButtons();
    storyMenu_  = nonDash_->CreateChild<StoryMenu>();
    storyMenu_->CreateButtons();
    arcadeMenu_ = nonDash_->CreateChild<ArcadeMenu>();
    arcadeMenu_->CreateButtons();
    inGameMenu_ = nonDash_->CreateChild<InGameMenu>();
    inGameMenu_->CreateButtons();
    //    settingsMenu_   = root->CreateChild<SettingsMenu>();
}

void GUI::CreateCenterText()
{
    UIElement* root{ GetSubsystem<UI>()->GetRoot() };
    centerText_ = root->CreateChild<Text>();
    centerText_->SetAlignment(HA_CENTER, VA_CENTER);
    centerText_->SetFont(FONT_SUPER_BUBBLE);
    centerText_->SetColor({ .95f });
    centerText_->SetTextEffect(TE_STROKE);
    centerText_->SetEffectColor({ .23f });
    centerText_->SetOpacity(.875f);
    centerText_->SetText("Game Over");
    centerText_->SetVisible(false);
}

PODVector<GameMenu*> GUI::AllGameMenus() const
{
    return PODVector<GameMenu*>{ mainMenu_, storyMenu_, arcadeMenu_, inGameMenu_ };
}

void GUI::ShowMain()
{
    GetSubsystem<Game>()->SetStatus(GS_MAIN);
    Show(mainMenu_);
}

void GUI::ShowStory()
{
    Show(storyMenu_);
}

void GUI::ShowArcade()
{
    Show(arcadeMenu_);
}

void GUI::ShowInGame()
{
    SetMouseVisible(true);
    GetSubsystem<Game>()->Pause();
    Show(inGameMenu_);
}

//void GUI::ShowSettings()
//{
//    settingsMenu_->SetVisible(true);
//    GetSubsystem<UI>()->SetFocusElement(settingsMenu_->BackButton());
//}

void GUI::SetMouseVisible(bool visible)
{
    Input* input{ GetSubsystem<Input>() };
    input->SetMouseVisible(visible);

    if (visible)
        input->SetMouseMode(MM_ABSOLUTE);
    else
        input->SetMouseMode(MM_WRAP);
}

void GUI::UpdateSizes()
{
    UISizes oldSizes{ sizes_ };
    UISizes defaultSizes{};

    Graphics* graphics{ GetSubsystem<Graphics>() };
    sizes_.screen_ = graphics->GetSize();
    sizes_.logo_ = Min(Min(VMinInt(90.f), 1024), VHInt(55.f));

    for (int t{ 0 }; t < 7; ++t)
        sizes_.text_[t] = Round(Lerp(defaultSizes.text_[t] * 1.f,
                                VMin(defaultSizes.text_[t] * .15f), .75f));

    blackness_->SetSize(sizes_.screen_);
    nonDash_->SetSize(sizes_.screen_);

    UIElement* root{ GetSubsystem<UI>()->GetRoot() };

    for (int i{ 0 }; i <= 6; ++i)
        for (UIElement* elem: root->GetChildrenWithTag(i != 0 ? "h" + String{ i } : "p", true))
            static_cast<Text*>(elem)->SetFontSize(sizes_.text_[i]);

    splash_->UpdateSizes();
    splash_->Animate();

    centerText_->SetFontSize(sizes_.text_[1]);
    centerText_->SetEffectStrokeThickness(VMinInt(.1f));

    if (dash_)
        dash_->UpdateSizes();

    if (sizes_ != oldSizes)
        SendEvent(E_UISIZESCHANGED);
}

void GUI::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSizes();
}

void GUI::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus oldStatus{ static_cast<GameStatus>(eventData[GameStatusChanged::P_OLDSTATUS].GetInt()) };
    const GameStatus status{    static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    if (oldStatus == GS_MAIN)
    {
        SetMouseVisible(false);
    }
    else if (status == GS_MAIN)
    {
        ShowMain();
        SetMouseVisible(true);
    }

    if (status == GS_PLAY)
    {
        SetMouseVisible(false);
        dash_->SetVisible(true);
    }
    else if (status == GS_OVERWORLD)
    {
        Show(nullptr);
    }

    if (oldStatus == GS_GAMEOVER)
    {
        centerText_->RemoveAttributeAnimation("Is Visible");
        centerText_->SetVisible(false);
    }
}

void GUI::PlaySample(const String& sampleName, float frequency, float gain)
{
    Node* musicNode{ GetSubsystem<JukeBox>()->GetMusicNode() };
    SoundSource* clickSource{ musicNode->CreateComponent<SoundSource>() };
    clickSource->SetAutoRemoveMode(REMOVE_COMPONENT);
    clickSource->Play(RES(Sound, "Samples/" + sampleName), frequency, gain);
}

void GUI::SetMouseAtElement(UIElement* element)
{
    INPUT->SetMousePosition(element->GetScreenPosition() + element->GetSize() * IntVector2{ 8, 1 } / IntVector2{ 9, 2 });
    GetSubsystem<UI>()->SetFocusElement(element);
}

void GUI::Show(GameMenu* menu)
{
    for (GameMenu* m: AllGameMenus())
    {
        if (m)
            m->SetVisible(m == menu);
    }

    SetMouseVisible(menu != nullptr);
    dash_->SetVisible(menu == nullptr);

    // Delay hide non-dash so overworld ignores confirm on continue
    if (menu)
        nonDash_->SetVisible(true);
    else
        SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(GUI, HideNonDash));
}

void GUI::HideNonDash(StringHash eventType, VariantMap& eventData)
{
    nonDash_->SetVisible(false);
    UnsubscribeFromEvent(E_POSTUPDATE);
}
