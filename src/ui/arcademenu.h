/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ARCADEMENU_H
#define ARCADEMENU_H

#include "gamemenu.h"

enum ArcadeMenuButton{ AMB_BACK = 0, AMB_START, AMB_ALL };

class SidePicker;
class PanelPicker;

class ArcadeMenu: public GameMenu
{
    const StringVector buttonsText_
    {
        { "Back"    },
        { "Start"   },
    };

    DRY_OBJECT(ArcadeMenu, GameMenu);

public:
    ArcadeMenu(Context* context);
    void CreateButtons() override;

protected:
    void UpdatePanelPicker();
    void UpdateSizes() override;

    void Back() override;
    void HandleVisibleChanged(StringHash eventType, VariantMap& eventData) override;

private:
    void SetNextPrevPanel(UIElement* panel);
    void HandleStartButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleActivePanelChanged(StringHash eventType, VariantMap& eventData);
    void HandleFocusChanged(StringHash eventType, VariantMap& eventData);
    void HandleGameModePicked(StringHash eventType, VariantMap& eventData);

    SidePicker* modePicker_;
    PanelPicker* levelPicker_;
};

#endif // ARCADEENU_H
