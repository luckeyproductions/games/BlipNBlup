/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../gamedefs.h"

#include "splashscreen.h"

SplashScreen::SplashScreen(Context* context): UIElement(context),
    blip_{ nullptr },
    blup_{ nullptr },
    blipText_{ nullptr },
    n_{ nullptr },
    blupText_{ nullptr },
    subtitle_{ nullptr },
    wasp_{ nullptr },
    pressStartText_{ nullptr }
{
    CreateElements();
    AnimateInitial(-6.f);

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(SplashScreen, HandleKeyButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(SplashScreen, HandleKeyButtonDown));
    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(SplashScreen, HandleGameStatusChanged));
}

void SplashScreen::CreateElements()
{
    const SharedPtr<Texture2D> splashTexture{ RES(Texture2D, "UI/Splash.png") };
    wasp_ = CreateChild<Sprite>("Wasp");
    blip_ = CreateChild<BorderImage>("Blip");
    blup_ = CreateChild<BorderImage>("Blup");
    blipText_ = CreateChild<BorderImage>("BlipText");
    blupText_ = CreateChild<BorderImage>("BlupText");
    n_ = CreateChild<BorderImage>("N");
    subtitle_ = CreateChild<BorderImage>("Subtitle");

    blip_->SetImageRect({ 0, 530, 420, 1020 });
    blup_->SetImageRect({ 670, 600, 1010, 955 });
    blipText_->SetImageRect({ 0, 0, 396, 303 });
    n_->SetImageRect({ 395, 0, 576, 167 });
    blupText_->SetImageRect({ 576, 0, 1024, 292 });
    subtitle_->SetImageRect({ 5, 319, 661, 420 });
    wasp_->SetImageRect({ 678, 320, 1024, 595 });
    wasp_->SetPivot(.55f, -2.3f);

    for (BorderImage* b: { blip_, blup_, blipText_, n_, blupText_, subtitle_ })
    {
        b->SetTexture(splashTexture);
        b->SetBlendMode(BLEND_ALPHA);
        b->SetAlignment(HA_CENTER, VA_TOP);
    }

    for (Sprite* s: { wasp_ })
    {
        s->SetTexture(splashTexture);
        s->SetBlendMode(BLEND_ALPHA);
        s->SetAlignment(HA_CENTER, VA_TOP);
    }

    pressStartText_ = CreateChild<Text>();
    pressStartText_->SetAlignment(HA_CENTER, VA_BOTTOM);
    pressStartText_->SetTextAlignment(HA_CENTER);
    pressStartText_->SetTextEffect(TE_STROKE);
    pressStartText_->SetEffectRoundStroke(true);
    pressStartText_->SetEffectColor(Color::BLACK);
    pressStartText_->SetFont(FONT_SUPER_BUBBLE);
    pressStartText_->SetText("Press start");

    UpdateSizes();
}


void SplashScreen::UpdateSizes()
{
    const float scale{ GetScale() };

    for (BorderImage* b: { blip_, blup_, blipText_, n_, blupText_, subtitle_ })
        b->SetSize(VectorRoundToInt(scale * b->GetImageRect().Size()));

    for (Sprite* s: { wasp_ })
        s->SetSize(VectorRoundToInt(Min(GUI::VW(.075f), scale) * s->GetImageRect().Size()));

    if (pressStartText_)
    {
        pressStartText_->SetFontSize(scale * 23.f);
        pressStartText_->SetEffectStrokeThickness(CeilToInt(2 * scale));
    }
}

void SplashScreen::Animate()
{
    const float blipTime{ blip_->GetAttributeAnimationTime("Position") };
    const bool started{ !pressStartText_ };

    AnimateInitial(!started ? blipTime : 10.f);

    if (started)
        AnimateSecondary(blipTime);
}

void SplashScreen::AnimateInitial(float time)
{
    const int screenWidth{ GUI::sizes_.screen_.x_ };
    const int halfWidth{ screenWidth / 2 };
    const float scale{ GetScale() };
    const int step{ RoundToInt(256 * scale) };
    const int bigStep{ step * 3/2 };
    const int fitStep{ Min(bigStep, GUI::VWInt(27.f)) };

    ObjectAnimation* blipAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blipAnim, {
                          { 0.f, IntVector2{ -blip_->GetWidth() / 2, bigStep } },
                          { 1.f, IntVector2{ halfWidth - fitStep, bigStep } }
                      });
    blip_->SetObjectAnimation(blipAnim);

    ObjectAnimation* blipTextAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blipTextAnim, {
                          { .5f,  IntVector2{ halfWidth - step, -blipText_->GetHeight() } },
                          { 1.5f, IntVector2{ halfWidth - step, GUI::VMinInt(5.5f) } }
                      });
    blipText_->SetObjectAnimation(blipTextAnim);

    ObjectAnimation* nAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(nAnim, {
                          { 1.5f, IntVector2{ halfWidth - fitStep / 23, -n_->GetHeight() } },
                          { 2.5f, IntVector2{ halfWidth - fitStep / 23 + GUI::VMinInt(.5f), GUI::VMinInt(11.f) } }
                      });
    n_->SetObjectAnimation(nAnim);

    ObjectAnimation* blupAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blupAnim, {
                          { 2.5f, IntVector2{ screenWidth + blup_->GetWidth() / 2, bigStep } },
                          { 3.5f, IntVector2{ halfWidth + fitStep, bigStep * 11/10 } }
                      });
    blup_->SetObjectAnimation(blupAnim);

    ObjectAnimation* blupTextAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blupTextAnim, {
                          { 3.f, IntVector2{ halfWidth + step, -blupText_->GetHeight() } },
                          { 4.f, IntVector2{ halfWidth + step + GUI::VMinInt(1.f), GUI::VMinInt(2.3f) } }
                      });
    blupText_->SetObjectAnimation(blupTextAnim);

    ObjectAnimation* subtitleAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(subtitleAnim, {
                          { 5.f, IntVector2{ halfWidth + fitStep / 6, GUI::sizes_.screen_.y_ } },
                          { 6.f, IntVector2{ halfWidth + fitStep / 6, GUI::VMinInt(25.f) } }
                      });
    subtitle_->SetObjectAnimation(subtitleAnim);

    ObjectAnimation* waspAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(waspAnim, {
                          { 5.f, Vector2{ -2.3f * wasp_->GetWidth(), -1.f * wasp_->GetHeight() } },
                          { 8.5f, Vector2{ halfWidth - 2.f * fitStep - GUI::VW(3.f), GUI::VMin(20.f) - .23f * fitStep } }
                      });
    ValueAnimation* waspRotAnim{ new ValueAnimation{ context_ } };
    waspRotAnim->SetKeyFrame(5.f, 17.f);
    waspRotAnim->SetKeyFrame(8.5f, 0.f);
    waspAnim->AddAttributeAnimation("Rotation", waspRotAnim, WM_CLAMP, ANIM_SPEED);
    wasp_->SetObjectAnimation(waspAnim);

    if (pressStartText_)
    {
        ObjectAnimation* pressStartAnim{ new ObjectAnimation{ context_ } };
        SetPositionFrames(pressStartAnim, {
                              { 9.f , IntVector2{ halfWidth, GUI::VMinInt(42.f) } },
                              { 10.f, IntVector2{ halfWidth, GUI::VMinInt(45.f) } }
                          });
        ValueAnimation* fadeIn{ new ValueAnimation{ context_ } };
        fadeIn->SetKeyFrame(9.f,  0.f);
        fadeIn->SetKeyFrame(10.f, 1.f);
        pressStartAnim->AddAttributeAnimation("Opacity", fadeIn, WM_ONCE, ANIM_SPEED);
        ValueAnimation* effectColAnim{ new ValueAnimation{ context_ } };
        effectColAnim->SetKeyFrame(9.f,  Color::WHITE.Transparent(0.f));
        effectColAnim->SetKeyFrame(10.f, Color::BLACK);
        pressStartAnim->AddAttributeAnimation("Effect Color", effectColAnim, WM_ONCE, ANIM_SPEED);
        pressStartText_->SetObjectAnimation(pressStartAnim);
        fadeIn->SetEventFrame(10.f, E_PRESSSTARTAPPEARED, {});
        SubscribeToEvent(E_PRESSSTARTAPPEARED, DRY_HANDLER(SplashScreen, HandlePressStartAppeared));
    }

    for (UIElement* e: PODVector<UIElement*>{ blip_, blup_, blipText_, n_, blupText_, subtitle_, wasp_, pressStartText_ })
    {
        if (e)
            e->SetAnimationTime(time);
    }
}

void SplashScreen::AnimateSecondary(float time)
{
    const int halfWidth{ GUI::sizes_.screen_.x_ / 2 };
    const float scale{ GetScale() };
    const int step{ RoundToInt(256 * scale) };
    const int bigStep{ step * 3/2 };
    const int fitStep{ Min(bigStep, GUI::VWInt(27.f)) };

    const IntVector2 blipPos{ blip_->GetPosition() };
    ObjectAnimation* blipAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blipAnim, {
                          { 0.f, IntVector2{ halfWidth - fitStep, blipPos.y_ } },
                          { 1.f, IntVector2{ FishFinalX(true), blipPos.y_ } }
                      });
    blip_->SetObjectAnimation(blipAnim);

    const IntVector2 blupPos{ blup_->GetPosition() };
    ObjectAnimation* blupAnim{ new ObjectAnimation{ context_ } };
    SetPositionFrames(blupAnim, {
                          { 0.f, IntVector2{ halfWidth + fitStep, blupPos.y_ } },
                          { 1.f, IntVector2{ FishFinalX(false), blupPos.y_ } }
                      });
    blup_->SetObjectAnimation(blupAnim);

    for (UIElement* e: PODVector<UIElement*>{ blip_, blup_ })
        e->SetAnimationTime(time);

    if (pressStartText_)
    {
        pressStartText_->Remove();
        pressStartText_ = nullptr;
    }
}

void SplashScreen::SetPositionFrames(ObjectAnimation* objectAnim, const Vector<VAnimKeyFrame>& keyFrames, WrapMode wrapMode) const
{
    ValueAnimation* posAnim{ new ValueAnimation{ context_ } };

    for (const VAnimKeyFrame& frame: keyFrames)
        posAnim->SetKeyFrame(frame.time_, frame.value_);

    posAnim->SetInterpolationMethod(IM_SINUSOIDAL);
    objectAnim->AddAttributeAnimation("Position", posAnim, wrapMode, ANIM_SPEED);
}

void SplashScreen::HandlePressStartAppeared(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    ValueAnimation* blink{ new ValueAnimation{ context_ } };
    blink->SetKeyFrame(0.f, true);
    blink->SetKeyFrame(.5f, false);
    blink->SetKeyFrame(1.f, true);
    pressStartText_->SetAttributeAnimation("Is Visible", blink, WM_LOOP, .55f);

    UnsubscribeFromEvent(E_PRESSSTARTAPPEARED);
}

void SplashScreen::HandleKeyButtonDown(StringHash eventType, VariantMap& eventData)
{
    if (eventType == E_KEYDOWN)
    {
        const PODVector<Key> skipKeys{ KEY_ESCAPE, KEY_SPACE, KEY_RETURN, KEY_RETURN2, KEY_KP_ENTER };
        if (!skipKeys.Contains(static_cast<Key>(eventData[KeyDown::P_KEY].GetInt())))
            return;
    }
    else if (eventType == E_JOYSTICKBUTTONDOWN)
    {
        const PODVector<ControllerButton> skipButtons{ CONTROLLER_BUTTON_A, CONTROLLER_BUTTON_START };
        if (!skipButtons.Contains(static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt())))
            return;
    }

    if (!HasSubscribedToEvent(E_PRESSSTARTAPPEARED))
    {
        GUI* gui{ GetSubsystem<GUI>() };
        AnimateSecondary();
        UnsubscribeFromEvent(E_KEYDOWN);
        UnsubscribeFromEvent(E_JOYSTICKBUTTONDOWN);
        gui->ShowMain();
        gui->PlaySample("EndBubble.wav");
    }
    else
    {
        for (UIElement* e: PODVector<UIElement*>{ blip_, blup_, blipText_, n_, blupText_, subtitle_, wasp_, pressStartText_ })
            e->SetAnimationTime(10.f);
    }
}

void SplashScreen::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    SetVisible(status < GS_OVERWORLD);
}
