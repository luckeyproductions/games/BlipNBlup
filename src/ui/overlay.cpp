/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "overlay.h"

void Overlay::RegisterObject(Context* context)
{
    context->RegisterFactory<Overlay>();

    DRY_COPY_BASE_ATTRIBUTES(View3D);
}

Overlay::Overlay(Context* context): View3D(context)
{
    SetFormat(Graphics::GetRGBAFormat());
    SetBlendMode(BLEND_ALPHA);
    CreateSceneAndCamera();
    SetFocusMode(FM_NOTFOCUSABLE);
    SetVisible(false);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Overlay, HandleSizesChanged));
}

void Overlay::CreateSceneAndCamera()
{
    Scene* overlayScene{ new Scene{ context_ }};
    overlayScene->CreateComponent<Octree>();
    Zone* zone{ overlayScene->CreateComponent<Zone>() };
    zone->SetAmbientColor({ .23f, .23f, .23f });
    zone->SetFogColor(Color::TRANSPARENT_BLACK);
    zone->SetFogStart(5.f);
    zone->SetFogEnd(10.f);
    Node* cameraNode{ overlayScene->CreateChild("Camera") };
    cameraNode->SetPosition({ 0.f, .75f, -3.f });
    cameraNode->LookAt(Vector3::UP);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetFarClip(50.f);
    camera->SetNearClip(M_MIN_NEARCLIP);
    SetView(overlayScene, camera);

    Viewport* viewport{ GetViewport() };
    SharedPtr<RenderPath> renderPath{ new RenderPath{} };
    renderPath->Load(RES(XMLFile, "RenderPaths/ForwardTransparent.xml"));
    renderPath->GetCommand(0u)->SetShaderParameter("color", Color::TRANSPARENT_BLACK);
    viewport->SetRenderPath(renderPath);
}

void Overlay::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetSize(GUI::sizes_.screen_);
}

void Overlay::PlayGameOverSequence()
{
    Scene* scene{ GetScene() };
    GetCameraNode()->SetParent(nullptr);
    scene->RemoveAllChildren();
    GetCameraNode()->SetParent(scene);

    RemoveAttributeAnimation("Opacity");
    RemoveAttributeAnimation("Is Visible");
    SetVisible(true);

    Node* lightNode{ scene->CreateChild("Light") };
    lightNode->SetPosition({ 2.f, 5.f, -3.f });
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetCastShadows(true);
    Node* satongNode{ scene->CreateChild("Sa'Tong")};
    satongNode->Translate(Vector3::FORWARD * 5.f);
    satongNode->Yaw(180.f);
    AnimatedModel* satong{ satongNode->CreateComponent<AnimatedModel>() };
    satong->SetCastShadows(true);
    satong->SetModel(RES(Model, "Models/Satong.mdl"));
    satong->SetMaterial(RES(Material, "Materials/VCol2.xml"));
    AnimationController* satongController{ satongNode->CreateComponent<AnimationController>() };
    satongController->PlayExclusive("Animations/Satong/GameOver.ani", 0u, false);

    const float animLength{ satongController->GetLength("Animations/Satong/GameOver.ani") };
    ValueAnimation* approach{ new ValueAnimation{ context_ } };
    approach->SetKeyFrame(0.f, satongNode->GetPosition());
    approach->SetKeyFrame(animLength, Vector3::BACK * 1/3.f);
    satongNode->SetAttributeAnimation("Position", approach, WM_CLAMP);

    ValueAnimation* fade{ new ValueAnimation{ context_ } };
    fade->SetInterpolationMethod(IM_SINUSOIDAL);
    fade->SetKeyFrame(0.f, 0.f);
    fade->SetKeyFrame(animLength * .25f, 1.f);
    fade->SetKeyFrame(animLength * .95f, 1.f);
    fade->SetKeyFrame(animLength, 0.f);
    SetAttributeAnimation("Opacity", fade, WM_CLAMP);

    ValueAnimation* hide{ new ValueAnimation{ context_ } };
    hide->SetKeyFrame(0.f, true);
    hide->SetKeyFrame(animLength, false);
    SetAttributeAnimation("Is Visible", hide, WM_CLAMP);
}
