/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef STORYMENU_H
#define STORYMENU_H

#include "gamemenu.h"

class DifficultyMenu: public GameMenu
{
    DRY_OBJECT(DifficultyMenu, GameMenu);

    const StringVector buttonsText_
    {
        { "Easy"  },
        { "Normal" },
        { "Hard" },
        { "Back" }
    };

public:
    DifficultyMenu(Context* context);
    void CreateButtons() override;

protected:
    void UpdateSizes() override;
    void Back() override;
    
private:
    void HandleDifficultyButtonClicked(StringHash eventType, VariantMap& eventData);
};

enum StoryMenuButton{ SMB_NEW = 0, SMB_LOAD, SMB_BACK, SMB_ALL };

class StoryMenu: public GameMenu
{
    DRY_OBJECT(StoryMenu, GameMenu);

    const StringVector buttonsText_
    {
        { "New"  },
        { "Load" },
        { "Back" }
    };

public:
    StoryMenu(Context* context);
    void CreateButtons() override;

protected:
    void UpdateSizes() override;
    void Back() override;

private:
    void HandleNewButtonClicked( StringHash eventType, VariantMap& eventData);
    void HandleLoadButtonClicked(StringHash eventType, VariantMap& eventData);

    DifficultyMenu* difficultyMenu_;
};

#endif // STORYMENU_H
