/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../jukebox.h"
#include "gamemenu.h"
#include "../inputmaster.h"

GameMenu::GameMenu(Context* context): UIElement(context),
    buttons_{},
    firstElem_{ nullptr },
    lastElem_{ nullptr },
    activeElem_{ nullptr }
{
    SetAlignment(HA_CENTER, VA_TOP);
    SetVisible(false);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(GameMenu, HandleSizesChanged));
    SubscribeToEvent(E_MENUINPUT, DRY_HANDLER(GameMenu, HandleMenuInput));
    SubscribeToEvent(this, E_VISIBLECHANGED, DRY_HANDLER(GameMenu, HandleVisibleChanged));
}

Button* GameMenu::CreateTextButton(const String& text)
{
    SharedPtr<Button> button{ CreateChild<Button>(text + "Button") };

    button->SetAlignment(HA_CENTER, VA_TOP);
    button->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    button->SetOpacity(.42f);
    Text* buttonText{ button->CreateChild<Text>() };
    buttonText->SetAlignment(HA_CENTER, VA_CENTER);
    buttonText->SetFont(FONT_SUPER_BUBBLE);
    buttonText->SetColor(Color::AZURE.Lerp(Color::CYAN, .8f).Lerp(Color::WHITE, .2f));
    buttonText->SetTextEffect(TE_SHADOW);
    buttonText->SetEffectColor(Color::AZURE.Lerp(Color::BLUE, .25f).Transparent(.75f));
    buttonText->SetOpacity(.875f);
    buttonText->SetText(text);

    SubscribeToEvent(button, E_CLICK, DRY_HANDLER(GameMenu, HandleButtonClicked));

    return button;
}

void GameMenu::SetTextButtonEnabled(Button* button, bool enabled)
{
    button->GetChildStaticCast<Text>(0u)->SetOpacity((enabled ? .875f : .125f));
    button->SetEnabled(enabled);
}

void GameMenu::HandleMenuInput(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!IsVisibleEffective())
        return;

    UI* ui{ GetSubsystem<UI>() };
    UIElement* oldFocusElem{ ui->GetFocusElement() };
    const VariantVector& varVec{ eventData[MenuInput::P_ACTIONS].GetVariantVector() };
    PODVector<MasterInputAction> actions{};
    for (const Variant& a: varVec)
        actions.Push(static_cast<MasterInputAction>(a.GetUInt()));

    if (actions.Contains(MIA_CANCEL))
    {
        Back();
        return;
    }

    const int step{ actions.Contains(MIA_DOWN)
                  - actions.Contains(MIA_UP) };
    const int side{ actions.Contains(MIA_RIGHT)
                  - actions.Contains(MIA_LEFT) };
    const int modify{ actions.Contains(MIA_INCREMENT)
                    - actions.Contains(MIA_DECREMENT) };

    if (!oldFocusElem)
    {
        if (activeElem_)
        {
            ui->SetFocusElement(activeElem_, true);
        }
        else
        {
            if (side + step > 0 || actions.Contains(MIA_CONFIRM))
                ui->SetFocusElement(firstElem_, true);
            else if (side + step < 0)
                ui->SetFocusElement(lastElem_, true);
        }
    }
    else
    {
        //Change value
        if (modify)
        {
            for (UIElement* child: oldFocusElem->GetChildren())
            {
                if (child->GetTypeInfo()->IsTypeOf(Slider::GetTypeStatic()))
                {
                    Slider* slider{ static_cast<Slider*>(child) };
                    slider->SetValue(slider->GetValue() + modify * .1f);
                }
                else if (child->GetTypeInfo()->IsTypeOf(DropDownList::GetTypeStatic()))
                {
                    DropDownList* dropDown{ static_cast<DropDownList*>(child) };
                    const unsigned index{ Clamp(dropDown->GetSelection() - modify, 0u, dropDown->GetNumItems() - 1) };
                    dropDown->SetSelection(index);
                    VariantMap selectedData{ { ItemSelected::P_ELEMENT, dropDown },
                                             { ItemSelected::P_SELECTION, index } };
                    dropDown->SendEvent(E_ITEMSELECTED, selectedData);
                }
            }
        }

        //Focus next element
        if (step > 0)
        {
            UIElement* nextElem{ static_cast<UIElement*>(oldFocusElem->GetVar("Next").GetPtr()) };
            while (nextElem && (!nextElem->IsEnabled() || !nextElem->IsVisible()) && nextElem != oldFocusElem)
                nextElem = static_cast<UIElement*>(nextElem->GetVar("Next").GetPtr());

            if (nextElem)
                ui->SetFocusElement(nextElem);
        }
        //Focus previous element
        if (step < 0)
        {
            UIElement* prevElem{ static_cast<UIElement*>(oldFocusElem->GetVar("Prev").GetPtr()) };
            while (prevElem && (!prevElem->IsEnabled() || !prevElem->IsVisible()) && prevElem != oldFocusElem)
                prevElem = static_cast<UIElement*>(prevElem->GetVar("Prev").GetPtr());

            if (prevElem)
                ui->SetFocusElement(prevElem);
        }
        //Focus right element
        if (side > 0)
        {
            UIElement* rightElem{ static_cast<UIElement*>(oldFocusElem->GetVar("Right").GetPtr()) };
            while (rightElem && (!rightElem->IsEnabled() || !rightElem->IsVisible()) && rightElem != oldFocusElem)
                rightElem = static_cast<UIElement*>(rightElem->GetVar("Right").GetPtr());

            if (rightElem)
                ui->SetFocusElement(rightElem);
        }
        //Focus left element
        if (side < 0)
        {
            UIElement* leftElem{ static_cast<UIElement*>(oldFocusElem->GetVar("Left").GetPtr()) };
            while (leftElem && (!leftElem->IsEnabled() || !leftElem->IsVisible()) && leftElem != oldFocusElem)
                leftElem = static_cast<UIElement*>(leftElem->GetVar("Left").GetPtr());

            if (leftElem)
                ui->SetFocusElement(leftElem);
        }
    }

    UIElement* newFocusElement{ ui->GetFocusElement() };
    if (newFocusElement)
    {
        activeElem_ = newFocusElement;

        if (oldFocusElem != activeElem_)
            GetSubsystem<GUI>()->SetMouseAtElement(activeElem_);
    }
}

bool GameMenu::Discard(VariantMap& eventData)
{
    UIElement* element{ static_cast<UIElement*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    if (!element)
        return true;

    int mouseButton{ eventData[ClickEnd::P_BUTTON].GetInt() };

    if (!element->HasFocus() || mouseButton != MOUSEB_LEFT)
        return true;

    return false;
}

void GameMenu::HandleButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    UIElement* clickedElement{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };

    if (Discard(eventData))
        return;

    activeElem_ = clickedElement;

    if (!clickedElement->HasTag("Silent"))
        GetSubsystem<GUI>()->PlaySample("EndBubble.wav");
}

void GameMenu::HandleVisibleChanged(StringHash, VariantMap&)
{
    if (!IsVisible())
        return;

    if (activeElem_)
        GetSubsystem<GUI>()->SetMouseAtElement(activeElem_);
}

void GameMenu::HandleBackButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Back();
}
