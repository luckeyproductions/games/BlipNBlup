/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"

#include "mainmenu.h"

MainMenu::MainMenu(Context* context): GameMenu(context)
{
}

void MainMenu::CreateButtons()
{
    for (int b{ 0 }; b < MMB_ALL; ++b)
    {
        String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b == MMB_SETTINGS)
        {
            button->GetChildStaticCast<Text>(0u)->SetOpacity(.125f);
            button->SetEnabled(false);
        }

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == MMB_ALL - 1)
        {
            button->SetVar("Next", buttons_[0]);
            buttons_[0]->SetVar("Prev", button);
        }
    }

    buttons_[MMB_EXIT]->AddTag("Silent");
    firstElem_ = buttons_[MMB_STORY];
    lastElem_  = buttons_[MMB_EXIT];

    SubscribeToEvent(buttons_[MMB_STORY],    E_CLICKEND, DRY_HANDLER(MainMenu,  HandleStoryButtonClicked));
    SubscribeToEvent(buttons_[MMB_ARCADE],   E_CLICKEND, DRY_HANDLER(MainMenu, HandleArcadeButtonClicked));
    SubscribeToEvent(buttons_[MMB_SETTINGS], E_CLICKEND, DRY_HANDLER(MainMenu,  HandleSettingsButtonClicked));
    SubscribeToEvent(buttons_[MMB_EXIT],     E_CLICKEND, DRY_HANDLER(MainMenu,  HandleExitButtonClicked));

    UpdateSizes();
}

void MainMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    for (int b{ 0 }; b < MMB_ALL; ++b)
    {
        Button* button{ buttons_[b] };

        if (!button)
            continue;

        const float buttonHeight{ GUI::VMin(10) };
        button->SetSize(GUI::VMin(65), buttonHeight);
        button->SetPosition(0, Max(GUI::VH(35), GUI::VMin(40)) + (buttonHeight + GUI::VH(1)) * b);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, -fontSize / 10);
//        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
        buttonText->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
    }
}

void MainMenu::HandleStoryButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<GUI>()->ShowStory();
}

void MainMenu::HandleArcadeButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<GUI>()->ShowArcade();
}

void MainMenu::HandleSettingsButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetVisible(false);
//    GetSubsystem<GUI>()->ShowSettings();
}

void MainMenu::HandleExitButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<MasterControl>()->Exit();
}
