/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GUI_H
#define GUI_H

#include <Dry/UI/Button.h>
#include <Dry/UI/DropDownList.h>
#include <Dry/UI/Font.h>
#include <Dry/UI/Sprite.h>
#include <Dry/UI/Slider.h>
#include <Dry/UI/Text.h>
#include <Dry/UI/Window.h>
#include <Dry/UI/ProgressBar.h>
#include <Dry/UI/View3D.h>
#include <Dry/UI/UIEvents.h>

#include "../inputmaster.h"
#include "../mastercontrol.h"

#define FONT_HAPPY_MEMORIES RES(Font, "Fonts/Happy Memories.ttf")
#define FONT_SOME_TIME_LATER RES(Font, "Fonts/Some Time Later.otf")
#define FONT_SUPER_BUBBLE RES(Font, "Fonts/Super Bubble.ttf")

class GameMenu;
class SplashScreen;
class MainMenu;
class StoryMenu;
class ArcadeMenu;
//class SettingsMenu;
class InGameMenu;
class Dash;
class Overlay;

struct UISizes {
    IntVector2 screen_{};
    int margin_{ 4 };
    int text_[7] = { 12, 38, 34, 23, 17, 16, 14 };
    int logo_{ 512 };
    int centralColumnWidth_{ 512 };
    int toolButton_{ 64 };

    bool operator != (UISizes& rhs)
    {
        if (logo_ != rhs.logo_
         || centralColumnWidth_ != rhs.centralColumnWidth_
         || margin_ != rhs.margin_
         || screen_ != rhs.screen_)
            return true;

        for (int t{ 0 }; t <= 6; ++t)
            if (text_[t] != rhs.text_[t])
                return true;

        return false;
    }
};

class GUI: public Object
{
    DRY_OBJECT(GUI, Object);

public:
    static String SecondsToTimeCode(float time, bool hundredths = false);
    static UISizes sizes_;

    static float VW(     float c = 1.f) { return c * sizes_.screen_.x_ * .01f; }
    static float VH(     float c = 1.f) { return c * sizes_.screen_.y_ * .01f; }
    static float VMin(   float c = 1.f) { return c * Min(VW(), VH());          }
    static float VMax(   float c = 1.f) { return c * Max(VW(), VH());          }
    static int   VWInt(  float c = 1.f) { return RoundToInt(VW(c));            }
    static int   VHInt(  float c = 1.f) { return RoundToInt(VH(c));            }
    static int   VMinInt(float c = 1.f) { return RoundToInt(VMin(c));          }
    static int   VMaxInt(float c = 1.f) { return RoundToInt(VMax(c));          }
    static float ScreenRatio() { return VW() / VH(); }
    static bool Portrait() { return ScreenRatio() < .8f; }

    static void RegisterObject(Context* context);
    GUI(Context* context);

    void UpdateSizes();
    Dash* GetDash() const { return dash_; }

    void Show(GameMenu* menu);
    void ShowMain();
    void ShowStory();
    void ShowArcade();
//    void ShowSettings();
    void ShowInGame();
    bool IsNonDashVisible() const { return nonDash_->IsVisible(); }

    void PlaySample(const String& sampleName, float frequency = 0.f, float gain = .5f);

    void SetMouseAtElement(UIElement* element);

    void SetBlackness(float alpha)
    {
        blackness_->SetOpacity(alpha);
        blackness_->SetPosition({});
        blackness_->SetSize(sizes_.screen_);
    }
    float GetBlackness() { return blackness_->GetOpacity(); }
    void SetBlackness(const IntVector2& pos, const IntVector2& size)
    {
        blackness_->SetOpacity(1.f);
        blackness_->SetPosition(pos);
        blackness_->SetSize(size);
    }
    void FadeOut(float duration = .42f);

private:
    void CreateNonDashElements();
    void CreateCenterText();

    PODVector<GameMenu*> AllGameMenus() const;
    void SetMouseVisible(bool visible);

    void HandleScreenMode(       StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);

    BorderImage* blackness_;
    Text* centerText_;
    Overlay* overlay_;
    UIElement* nonDash_;
    SplashScreen* splash_;
    MainMenu* mainMenu_;
    StoryMenu* storyMenu_;
    ArcadeMenu* arcadeMenu_;
//    SettingsMenu* settingsMenu_;
    InGameMenu* inGameMenu_;
    Dash* dash_;
    void HideNonDash(StringHash eventType, VariantMap& eventData);
};

DRY_EVENT(E_UISIZESCHANGED, UISizeChanged)
{
}

#endif // GUI_H
