/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "panelpicker.h"

void PanelPicker::RegisterObject(Context* context)
{
    context->RegisterFactory<Panel>();
    context->RegisterFactory<PanelPicker>();
}

Panel::Panel(Context* context): Window(context),
    titleText_{ nullptr },
    previewImage_{ nullptr }
{
    SetHorizontalAlignment(HA_CENTER);
    SetOpacity(.875f);
    SetFocusMode(FM_FOCUSABLE);
    SetBlendMode(BLEND_ALPHA);
    SetTexture(RES(Texture2D, "UI/UI.png"));
    SetImageRect({ 64, 16, 80, 32 });
    SetBorder({ 4, 4, 4, 4 });

    CreateTitle();
    CreatePreview();
}

void Panel::CreateTitle()
{
    titleText_ = CreateChild<Text>();
    titleText_->SetName("PanelTitle");
    titleText_->SetFont(FONT_SOME_TIME_LATER);
    titleText_->SetAlignment(HA_LEFT, VA_TOP);
    titleText_->SetText("Untitled");
}

void Panel::CreatePreview()
{
    previewImage_ = CreateChild<BorderImage>();
}

void Panel::SetTitle(const String& text)
{
    titleText_->SetText(text);
}

void Panel::SetPreview(Texture* preview)
{
    previewImage_->SetTexture(preview);
}

PanelPicker::PanelPicker(Context* context): UIElement(context),
    panels_{},
    arrows_{},
    cells_{ 1u },
    page_{ 0u },
    lastPage_{ 0u },
    activePanel_{ 0u }
{
    SetHorizontalAlignment(HA_CENTER);

    CreateArrows();
}

void PanelPicker::CreateArrows()
{
    for (bool left: { true, false })
    {
        Button* arrowButton{ CreateChild<Button>() };
        arrowButton->SetAlignment(HA_CENTER, VA_CENTER);
        arrowButton->SetFocusMode(FM_RESETFOCUS);
        arrowButton->SetTexture(RES(Texture2D, "UI/Arrow.png"));
        arrowButton->SetImageRect({ 128 * !left, 0, 128 + 128 * !left, 256 });
        arrowButton->SetHoverOffset(0, 256);
        arrows_.Push(arrowButton);

        SubscribeToEvent(arrowButton, E_CLICKEND, DRY_HANDLER(PanelPicker, HandleArrowClicked));
    }

    UpdateSizes();
}

void PanelPicker::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_.x_, GUI::VH(34));
    IntVector2 panelSize{ GUI::VMinInt(40), GetHeight() };
    IntVector2 cellSize{ panelSize + IntVector2{ GUI::VMinInt(5), GUI::VMinInt(5) } };

    const int columns{ Min(Max(1, (GetWidth() - panelSize.x_ * .5f) / cellSize.x_), panels_.Size()) };
    if (columns == 0u)
        return;

    const int rows{ Min(Max(1, (GetHeight() - (cellSize.y_ + GUI::sizes_.logo_) / 2) / cellSize.y_), panels_.Size() / columns) };
    if (rows == 0u)
        return;

    cells_ = columns * rows;
    page_ = activePanel_ / cells_;
    lastPage_ = (panels_.Size() - 1u) / cells_;

    const IntVector2 arrowSize{ Min(Min(GUI::VHInt(11), GUI::VWInt(15)), 128), Min(Min(GUI::VHInt(22), GUI::VWInt(30)), 256) };
    cellSize = IntVector2{ (GUI::VWInt(80) - arrowSize.x_) / columns, GetHeight() / rows };
    panelSize = cellSize - IntVector2{ GUI::VWInt(2), GUI::VHInt(3) };

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        Window* panel{ panels_.At(p) };
        if (!panel)
            continue;

        const int c{ static_cast<int>(p % cells_) };
        const int column{ c % columns };
        const int row{ c / columns };

        panel->SetPosition((-columns / 2 + ((columns + 1) % 2) * .5f + column) * cellSize.x_,
                           row * cellSize.y_);
        panel->SetSize(panelSize);
        panel->SetVisible(p >= page_ * cells_ && p < cells_ + page_ * cells_);

        Text* panelTitleText{ panel->GetChildStaticCast<Text>(String{ "PanelTitle" }) };
        if (panelTitleText)
        {
            const float fontSize{ (panelSize.x_ + panelSize.y_) / 23.f };
            panelTitleText->SetFontSize(fontSize);
            panelTitleText->SetPosition(fontSize / 2, 0);
        }

        /// Should take rows/columns into account
        if (p == 0u)
            panel->SetVar("Left", panels_.At(panels_.Size() - 1u));
        else
            panel->SetVar("Left", panels_.At(p - 1u));

        if (p == panels_.Size() - 1u)
            panel->SetVar("Right", panels_.At(0u));
        else
            panel->SetVar("Right", panels_.At(p + 1u));
    }

    for (Button* arrowButton: arrows_)
    {
        arrowButton->SetSize(arrowSize);
        arrowButton->SetPosition((GUI::VWInt(48) - arrowSize.x_ / 2) * (1 - 2 * (arrowButton == arrows_.Front())), 0);
        arrowButton->SetVisible(panels_.Size() > cells_);
    }
}

void PanelPicker::SetNumPanels(unsigned count)
{
    for (Panel* p: panels_)
        p->Remove();
    panels_.Clear();

    for (unsigned p{ 0u }; p < count; ++p)
    {
        Panel* panel{ CreateChild<Panel>() };
        panel->SetVar("Index", p);
        panel->SetAlignment(HA_CENTER, VA_CENTER);
        //        panel->SetVar("Level", fullMapName);
        panels_.Push(panel);

        SubscribeToEvent(panel, E_CLICK, DRY_HANDLER(PanelPicker, HandlePanelClicked));
    }

    if (count != 0u)
        SetActivePanel(0u);
}

void PanelPicker::SetActivePanel(unsigned index)
{
    index = Clamp(index, 0u, panels_.Size() - 1u);
    const unsigned page{ index / cells_ };
    if (page_ != page)
        SetPage(page);

    for (unsigned p{ 0u }; p < panels_.Size(); ++p)
    {
        Window* panel{ panels_.At(p) };
        const bool active{ (p == index) };
        const int d{ active * 16 };
        const IntRect rect{ IntRect{ 48, 0, 64, 16 } + IntRect{ d, d, d, d } };
        panel->SetImageRect(rect);
        panel->SetOpacity((active ? .9f : .34f));
    }

    activePanel_ = index;

    VariantMap eventData{};
    eventData[ActivePanelChanged::P_ELEMENT] = GetActivePanel();
    eventData[ActivePanelChanged::P_INDEX]   = activePanel_;
    SendEvent(E_ACTIVEPANELCHANGED, eventData);
}

void PanelPicker::SetPage(unsigned page, bool setActive)
{
    page_ = page;

    const unsigned c{ cells_ * page };
    const Pair<unsigned, unsigned> range{ c, c + cells_ - 1u };

    for (unsigned p{ 0u }; p < panels_.Size(); ++p)
    {
        Window* panel{ panels_.At(p) };
        const bool visible{ p >= range.first_ && p <= range.second_ };
        panel->SetVisible(visible);
    }
}

void PanelPicker::HandlePanelClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    UIElement* element{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };
    if (!element)
        return;

    if (element->GetParent()->GetType() == "Panel")
        element = element->GetParent();

    SetActivePanel(element->GetVar("Index").GetUInt());
}

void PanelPicker::HandleArrowClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    unsigned page{ page_ };
    if (arrows_[0u] == eventData[Click::P_ELEMENT].GetPtr())
    {
        if (page_ > 0u)
            --page;
        else
            page = lastPage_;
    }
    else
    {
        if (page_ < lastPage_)
            ++page;
        else
            page = 0u;
    }

    if (page_ != page)
        SetPage(page);
}
