/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINMENU_H
#define MAINMENU_H

#include "gamemenu.h"

enum MainMenuButton{ MMB_STORY = 0, MMB_ARCADE, MMB_SETTINGS, MMB_EXIT, MMB_ALL };

class MainMenu: public GameMenu
{
    const StringVector buttonsText_
    {
        { "Story Mode" },
        { "Arcade"     },
        { "Settings"   },
        { "Exit"       }
    };

    DRY_OBJECT(MainMenu, GameMenu);

public:
    MainMenu(Context* context);

    void CreateButtons() override;
    void SetButtonsVisible(bool visible);

protected:
    void UpdateSizes() override;

private:
    void HandleStoryButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleArcadeButtonClicked(StringHash, VariantMap& eventData);
    void HandleSettingsButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleExitButtonClicked(StringHash eventType, VariantMap& eventData);

    HashMap<int, Button*> buttons_;
};

#endif // MAINMENU_H
