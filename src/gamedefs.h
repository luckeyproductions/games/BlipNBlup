/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAMEDEFS_H
#define GAMEDEFS_H

#include <Dry/Core/Object.h>

enum GameStatus{ GS_NONE = 0, GS_SPLASH, GS_MAIN, GS_OVERWORLD, GS_PLAY, GS_PAUSED, GS_GAMEOVER, GS_MODAL };
enum GameMode{ GM_NONE = 0, GM_STORY, GM_CTF, GM_SOCCER, GM_COLLECTOR, GM_SURVIVAL };
enum Difficulty{ D_EASY = 0, D_NORMAL, D_HARD, D_ALL };
enum TeamPreference{ TP_RANDOM = -1, TP_BLIP, TP_BLUP };

DRY_EVENT(E_GAMESTATUSCHANGED, GameStatusChanged)
{
    DRY_PARAM(P_STATUS, Status);       // int
    DRY_PARAM(P_OLDSTATUS, OldStatus); // int
}

DRY_EVENT(E_GAMEMODECHANGED, GameModeChanged)
{
    DRY_PARAM(P_MODE, Mode); // int
}

DRY_EVENT(E_RESTART, Restart)
{
}

#endif // GAMEDEFS_H
