/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAME_H
#define GAME_H

#include "gamedefs.h"
#include "mastercontrol.h"
#include "arcade.h"

#define GAME GetSubsystem<Game>()

class BlockMap;
class BnBCam;
class Fish;

typedef struct World
{
    Scene* playScene_{ nullptr };
    BlockMap* level_{ nullptr };
    Fish* blip_{ nullptr };
    Fish* blup_{ nullptr };

    NavigationMesh* walkNav_{ nullptr };
    NavigationMesh* flyNav_{ nullptr };
} World;

class Game: public Object
{
    DRY_OBJECT(Game, Object);

public:
    static void RegisterObject(Context* context);
    Game(Context* context);

    bool IsSinglePlayerStory() const;
    void SwitchFish();

    void SetStatus(GameStatus status);
    void SetGameMode(GameMode mode);
    GameStatus GetStatus() const { return status_; }
    GameMode GetGameMode() const { return mode_; }
    Difficulty GetDifficulty() const { return difficulty_; }

    void StartNewStory(Difficulty difficulty);
    void StartStoryLevel(const String& mapName);
    void RestartStoryLevel();
    void StartArcade(GameMode mode,  const String& mapName = "");
    void TogglePause();
    void Pause();
    void Unpause();

    void CreateMenuScene();
    void LoadMap(const String& mapName);

    World& GetWorld() { return world_; }
    Scene* GetScene() const { return world_.playScene_; }
    Fish* GetBlip()   const { return world_.blip_; }
    Fish* GetBlup()   const { return world_.blup_; }

    String CompleteMapName(const String& mapName);

private:
    Scene* CreatePlayScene();
    void PrepareStoryScene();
    void PrepareArcadeScene();
    void UpdatePlayerViewports();

    void HandleFishFainted(StringHash eventType, VariantMap& eventData);
    void HandleBugReleased(StringHash, VariantMap& eventData);
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    World world_;
    Arcade* arcade_;
    GameStatus status_;
    GameMode mode_;
    Difficulty difficulty_;
};

#endif // GAME_H
