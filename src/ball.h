/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BALL_H
#define BALL_H

#include "sceneobject.h"

class Ball: public SceneObject
{
    DRY_OBJECT(Ball, SceneObject);

public:
    static void RegisterObject(Context* context);
    Ball(Context* context);

    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;
    void Disable() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    void HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData);

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);

    RigidBody* rigidBody_;
    StaticModel* model_;
    bool scored_;
};

DRY_EVENT(E_GOALSCORED, GoalScored)
{
    DRY_PARAM(P_SIDE, Side); // int
}

#endif // BALL_H
