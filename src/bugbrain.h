/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BUGBRAIN_H
#define BUGBRAIN_H

#include "controllable.h"

#include "luckey.h"

class Catchable;

class BugBrain: public LogicComponent
{
    DRY_OBJECT(BugBrain, LogicComponent);

public:
    BugBrain(Context* context);

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    Controllable* GetControllable() const { return node_->GetDerivedComponent<Controllable>(); }
    bool IsFlyer() const;
    void ThinkStory(float timeStep);
    void ThinkCTF(float timeStep);
    NavigationMesh* GetNavMesh() const;
    void FindPath();
    void FollowPath(float timeStep);
    Vector3 AvoidWall();

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& evenData);
    void HandleCaught(StringHash eventType, VariantMap& evenData);

    Controllable* controllable_;
    Catchable* catchable_;
    Vector3 spawnPos_;
    PODVector<Vector3> path_;
};

#endif // BUGBRAIN_H
