/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "bnbcam.h"
#include "fish.h"
#include "couch.h"
#include "ui/gui.h"

#include "inputmaster.h"

using namespace LucKey;

InputMaster::InputMaster(Context* context): Object(context),
    keyBindingsMaster_{},
    buttonBindingsMaster_{},
    keyBindingsPlayer_{},
    buttonBindingsPlayer_{},
    pressedKeys_{},
    pressedJoystickButtons_{},
    leftStickPosition_{},
    rightStickPosition_{},
    lastMasterActions_{},
    sinceMasterAction_{},
    masterInterval_{ .23f },
    noRepeatMaster_( { MIA_CONFIRM, MIA_CANCEL, MIA_PAUSE, MIA_MENU, MIA_SWITCH } ),
    wasPaused_{ false }
{
    for (int a{ 0 }; a < MIA_ALL; ++a)
        sinceMasterAction_[a] = masterInterval_;

    BindDefault();
    SubscribeToEvents();
}

void InputMaster::BindDefault()
{
    keyBindingsMaster_[KEY_UP]     = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_UP]    = MIA_UP;
    keyBindingsMaster_[KEY_DOWN]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_DOWN]  = MIA_DOWN;
    keyBindingsMaster_[KEY_LEFT]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_LEFT]  = MIA_LEFT;
    keyBindingsMaster_[KEY_RIGHT]  = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_RIGHT] = MIA_RIGHT;
    keyBindingsMaster_[KEY_RETURN] = buttonBindingsMaster_[CONTROLLER_BUTTON_A]          = MIA_CONFIRM;
    keyBindingsMaster_[KEY_ESCAPE] = buttonBindingsMaster_[CONTROLLER_BUTTON_B]          = MIA_CANCEL;
    keyBindingsMaster_[KEY_P]      = buttonBindingsMaster_[CONTROLLER_BUTTON_START]      = MIA_PAUSE;
    keyBindingsMaster_[KEY_TAB]    = buttonBindingsMaster_[CONTROLLER_BUTTON_BACK]       = MIA_SWITCH;
    buttonBindingsMaster_[CONTROLLER_BUTTON_GUIDE] = MIA_MENU;

    keyBindingsPlayer_[1][KEY_W] = /*keyBindingsPlayer_[1][KEY_UP]     = */PIA_FORWARD;
    keyBindingsPlayer_[1][KEY_S] = /*keyBindingsPlayer_[1][KEY_DOWN]   = */PIA_BACK;
    keyBindingsPlayer_[1][KEY_A] = /*keyBindingsPlayer_[1][KEY_LEFT]   = */PIA_LEFT;
    keyBindingsPlayer_[1][KEY_D] = /*keyBindingsPlayer_[1][KEY_RIGHT]  = */PIA_RIGHT;
    keyBindingsPlayer_[1][KEY_C] = keyBindingsPlayer_[1][KEY_LSHIFT] = PIA_RUN;
    keyBindingsPlayer_[1][KEY_V] = keyBindingsPlayer_[1][KEY_SPACE]  = PIA_JUMP;
    keyBindingsPlayer_[1][KEY_B] = keyBindingsPlayer_[1][KEY_CTRL]   = PIA_BUBBLE;
    keyBindingsPlayer_[1][KEY_N] = keyBindingsPlayer_[1][KEY_ALT]    = PIA_INTERACT;

    keyBindingsPlayer_[2][KEY_KP_5] = PIA_BACK;
    /*keyBindingsPlayer_[2][KEY_KP_8] */ keyBindingsPlayer_[2][KEY_UP]    = PIA_FORWARD;
    /*keyBindingsPlayer_[2][KEY_KP_2] */ keyBindingsPlayer_[2][KEY_DOWN]  = PIA_BACK;
    /*keyBindingsPlayer_[2][KEY_KP_4] */ keyBindingsPlayer_[2][KEY_LEFT]  = PIA_LEFT;
    /*keyBindingsPlayer_[2][KEY_KP_6] */ keyBindingsPlayer_[2][KEY_RIGHT] = PIA_RIGHT;
    keyBindingsPlayer_[2][KEY_RETURN] = PIA_RUN;
    keyBindingsPlayer_[2][KEY_RSHIFT] = PIA_JUMP;
    keyBindingsPlayer_[2][KEY_RCTRL]  = PIA_BUBBLE;

    for (int p{ 1 }; p <= 6; ++p)
    {
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_DPAD_UP]    = PIA_FORWARD;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_DPAD_DOWN]  = PIA_BACK;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_DPAD_LEFT]  = PIA_LEFT;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_DPAD_RIGHT] = PIA_RIGHT;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_A]          = PIA_JUMP;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_B]          = PIA_BUBBLE;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_X]          = PIA_RUN;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_Y]          = PIA_INTERACT;
    }
}

void InputMaster::SubscribeToEvents()
{
    SubscribeToEvent(E_KEYDOWN,             DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP,               DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN,  DRY_HANDLER(InputMaster, HandleJoyButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP,    DRY_HANDLER(InputMaster, HandleJoyButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE,    DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_UPDATE,              DRY_HANDLER(InputMaster, HandleUpdate));
}

void InputMaster::FilterRepeat(PODVector<MasterInputAction>& masterActions)
{
    const PODVector<MasterInputAction> unfilteredMaster{ masterActions };
    for (MasterInputAction a: masterActions)
    {
        if (IsRepeating(a) && (noRepeatMaster_.Contains(a) || sinceMasterAction_[a] < masterInterval_))
            masterActions.Remove(a);
        else
            sinceMasterAction_[a] = 0.f;
    }

    lastMasterActions_ = unfilteredMaster;
}

void InputMaster::Unpress(int playerId)
{
    if (keyBindingsPlayer_.Contains(playerId))
    {
        for (int key: keyBindingsPlayer_[playerId].Keys())
        {
            if (keyBindingsPlayer_[playerId][key] < PIA_FORWARD)
                pressedKeys_.Remove(key);
        }
    }

    if (pressedJoystickButtons_.Contains(playerId))
        pressedJoystickButtons_[playerId].Clear();
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    for (int a{ 0 }; a < MIA_ALL; ++a)
    {
        const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };
        const bool fastRepeat{ false };//((a == MIA_INCREMENT || a == MIA_DECREMENT) && GetSubsystem<GUI>()->GetSettingsMenu()->IsVisible()) };
        const float factor{ 1.f + fastRepeat };
        sinceMasterAction_[a] += timeStep * factor;
    }

    PODVector<Player*> players{ GetSubsystem<Couch>()->GetPlayers() };

    InputActions activeActions{};
    for (Player* p: players)
        activeActions.player_[p->GetPlayerID()] = {};

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (keyBindingsMaster_.Contains(key))
        {
            MasterInputAction action{ keyBindingsMaster_[key] };

            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }

        GameStatus gameStatus{ GetSubsystem<Game>()->GetStatus() };
        if (key == KEY_ESCAPE && gameStatus > GS_MAIN)
            activeActions.master_.Push(MIA_MENU);

        //Check for player key presses
        for (Player* p: players)
        {
            const unsigned pid{ p->GetPlayerID() };

            if (keyBindingsPlayer_[pid].Contains(key))
            {
                PlayerInputAction action{ keyBindingsPlayer_[pid][key] };
                if (!activeActions.player_[pid].Contains(action))
                    activeActions.player_[pid].Push(action);
            }
        }
    }

    //Check for joystick button presses
    for (Player* p: players)
    {
        const unsigned pid{ p->GetPlayerID() };
        const unsigned jid{ p->GetJoystickID() };

        for (int button: pressedJoystickButtons_[jid])
        {
            if (buttonBindingsMaster_.Contains(button))
            {
                MasterInputAction action{ buttonBindingsMaster_[button] };

                if (!activeActions.master_.Contains(action))
                    activeActions.master_.Push(action);
            }

            if (buttonBindingsPlayer_[pid].Contains(button))
            {
                PlayerInputAction action{ buttonBindingsPlayer_[pid][button]};
                if (!activeActions.player_[pid].Contains(action))
                    activeActions.player_[pid].Push(action);
            }
        }

        // Menu stick
        const float leftX{ leftStickPosition_[jid].x_ };
        const float leftY{ leftStickPosition_[jid].y_ };
        if      (leftY >  .125f && !activeActions.master_.Contains(MIA_UP))    activeActions.master_.Push(MIA_UP);
        else if (leftY < -.125f && !activeActions.master_.Contains(MIA_DOWN))  activeActions.master_.Push(MIA_DOWN);
        if      (leftX < -.125f && !activeActions.master_.Contains(MIA_LEFT))  activeActions.master_.Push(MIA_LEFT);
        else if (leftX >  .125f && !activeActions.master_.Contains(MIA_RIGHT)) activeActions.master_.Push(MIA_RIGHT);
    }

    FilterRepeat(activeActions.master_);
    HandleActions(activeActions);
}

void InputMaster::HandleActions(const InputActions& actions)
{
    HandleMasterActions(actions.master_);

    if (GAME->GetStatus() == GS_PLAY)
        HandlePlayerActions(actions.player_);
}

void InputMaster::HandleMasterActions(const PODVector<MasterInputAction>& actions)
{
    if (actions.IsEmpty())
        return;

    Game* game{ GetSubsystem<Game>() };
    GameStatus gameStatus{ game->GetStatus() };

    if (actions.Contains(MIA_PAUSE) && !GetSubsystem<GUI>()->IsNonDashVisible())
    {
        game->TogglePause();
    }
    else
    {
        if (actions.Contains(MIA_CONFIRM))
        {
            UIElement* focusElem{ GetSubsystem<UI>()->GetFocusElement() };

            if (focusElem)
            {
                VariantMap eventData{};
                eventData.Insert({ ClickEnd::P_ELEMENT, Variant{ focusElem } });
                eventData.Insert({ ClickEnd::P_BUTTON, Variant{ MOUSEB_LEFT } });
                focusElem->SendEvent(E_CLICK, eventData);
                focusElem->SendEvent(E_CLICKEND, eventData);
            }
        }

        VariantMap eventData{};
        VariantVector masterActions{};

        for (MasterInputAction a: actions)
        {
            if (a != MIA_SWITCH && a != MIA_PAUSE)
                masterActions.Push(Variant{ static_cast<unsigned>(a) });
        }

        eventData.Insert({ MenuInput::P_ACTIONS, Variant{ masterActions } });
        SendEvent(E_MENUINPUT, eventData);

        if (actions.Contains(MIA_MENU) && gameStatus > GS_MAIN)
        {
            if (!GetSubsystem<GUI>()->IsNonDashVisible())
            {
                wasPaused_ = gameStatus == GS_PAUSED;
                GetSubsystem<GUI>()->ShowInGame();
            }
        }
    }

    if (actions.Contains(MIA_SWITCH) && gameStatus == GS_PLAY && game->IsSinglePlayerStory())
        GAME->SwitchFish();
}

void InputMaster::HandlePlayerActions(const HashMap<int, PODVector<PlayerInputAction>>& actions)
{
    const PODVector<Player*> players{ GetSubsystem<Couch>()->GetPlayers() };

    for (Player* p: players)
    {
        const unsigned pid{ p->GetPlayerID() };
        const unsigned jid{ p->GetJoystickID() };
        const auto playerInputActions = *actions[pid];

        Controllable* controlled{ p->GetControllable() };
        if (controlled)
        {
            Vector3 stickMove{ leftStickPosition_[jid].x_, 0.f, leftStickPosition_[jid].y_ };
            CorrectForCameraYaw(p, stickMove);
            Vector3 actionsMove{ GetMoveFromActions(playerInputActions) };
            CorrectForCameraYaw(p, actionsMove);

            controlled->SetMove(actionsMove + stickMove);

            std::bitset<4>restActions{};
            for (int a{ 0 }; a < NUM_CONTROLLABLE_ACTIONS; ++a)
                restActions[a] = playerInputActions.Contains(static_cast<PlayerInputAction>(a));

            controlled->SetActions(restActions);
        }
    }
}

Vector3 InputMaster::GetMoveFromActions(const PODVector<PlayerInputAction>& actions) const
{
    const Vector3 move{ Vector3::RIGHT *
                        (actions.Contains(PIA_RIGHT) -
                         actions.Contains(PIA_LEFT))

                        + Vector3::UP *
                        (actions.Contains(PIA_UP) -
                         actions.Contains(PIA_DOWN))

                        + Vector3::FORWARD *
                        (actions.Contains(PIA_FORWARD) -
                         actions.Contains(PIA_BACK))
                      };

    return move;
}

void InputMaster::HandleKeyDown(StringHash /*eventType*/, VariantMap &eventData)
{
    const int key{ eventData[KeyDown::P_KEY].GetInt() };

    if (!pressedKeys_.Contains(key))
        pressedKeys_.Push(key);

    switch (key)
    {
    case KEY_9:
    {
        Image screenshot(context_);
        Graphics* graphics{ GetSubsystem<Graphics>() };
        graphics->TakeScreenShot(screenshot);
        //Here we save in the Data folder with date and time appended
        String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
                         Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_')+".png" };
        //Log::Write(1, fileName);
        screenshot.SavePNG(fileName);
    } break;
    default: break;
    }
}

void InputMaster::HandleKeyUp(StringHash /*eventType*/, VariantMap &eventData)
{
    const int key{ eventData[KeyUp::P_KEY].GetInt() };

    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoyButtonDown(StringHash /*eventType*/, VariantMap &eventData)
{
    const int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    const ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };

    if (!pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Push(button);
}

void InputMaster::HandleJoyButtonUp(StringHash /*eventType*/, VariantMap &eventData)
{
    const int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    const ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };

    if (pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(StringHash /*eventType*/, VariantMap& eventData)
{
    const int joystickId{ eventData[JoystickAxisMove::P_JOYSTICKID].GetInt() };
    const int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    const float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    if (axis == 0)
        leftStickPosition_[joystickId].x_  = position;
    else if (axis == 1)
        leftStickPosition_[joystickId].y_  = -position;
    else if (axis == 2)
        rightStickPosition_[joystickId].x_ = position;
    else if (axis == 3)
        rightStickPosition_[joystickId].y_ = -position;
}

void InputMaster::CorrectForCameraYaw(Player* player, Vector3& vector) const
{
    vector = Quaternion{ player->GetCamera()->GetRotation().YawAngle(), Vector3::UP } * vector;
}
