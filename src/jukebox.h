/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JUKEBOX_H
#define JUKEBOX_H

#include "mastercontrol.h"

struct Song {
    String name_;
    float gain_;
};

class JukeBox: public Object
{
    const Vector<Song> songs_
    {
        { "Kevin MacLeod - W A Mozart Divertimento K131.ogg", .8f },
        { "Kevin MacLeod - Beach Party.ogg", .32f },
        { "", .5f },
        { "Spring Spring - Forest (Jazz remix).ogg", 1.5f },
        { "XL Ant - No-Trace Land.ogg", .5f },
        { "", .5f },
        { "", .5f },
        { "mirz - Space0.ogg", .75f },
        { "Kevin MacLeod - Faster Does It.ogg", .5f }
    };

    DRY_OBJECT(JukeBox, Object);

public:
    enum Music{ MUSIC_NONE = -1, MUSIC_MENU, MUSIC_BEACH, MUSIC_JUNGLE, MUSIC_FOREST,
                MUSIC_SNOW, MUSIC_MOUNTAINS, MUSIC_VOLCANO, MUSIC_SPACE,
                MUSIC_BONUS };

    JukeBox(Context* context);

    Node* GetMusicNode() const { return musicNode_; }

    void Play(Music music);
    void Play(const String& song, bool loop = true, float gain = 1.f);
    void RestartCurrent();

private:
    void HandleMusicFaded(          StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(   StringHash eventType, VariantMap& eventData);

    Music currentSong_;
    Node* musicNode_;
    Vector<SoundSource*> tracks_;
};

DRY_EVENT(E_MUSICFADED, MusicFaded)
{
}

#endif // JUKEBOX_H
