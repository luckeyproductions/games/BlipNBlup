/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ARCADE_H
#define ARCADE_H

#include "gamedefs.h"

#include "mastercontrol.h"

class SpawnPoint;
class Fish;
class Catchable;
class Satong;
class Base;
class Goal;
class Ball;

typedef struct Team
{
    Team(TeamPreference side):
        side_{ side }
    {}

    TeamPreference side_;
    PODVector<SpawnPoint*> spawnPoints_{};
    PODVector<Fish*> fish_{};
    Base* base_{ nullptr };
    Goal* goal_{ nullptr };
    Satong* satong_{};
    WeakPtr<Catchable> bug_{ nullptr };
    int score_{};
} Team;

class Arcade: public Object
{
    DRY_OBJECT(Arcade, Object);

public:
    static void RegisterObject(Context* context);

    Arcade(Context* context);

    void SetTeamSize(unsigned size);
    unsigned GetTeamSize() const { return teamSize_; }
    const Team& GetTeamBlip() const { return teamBlip_; }
    const Team& GetTeamBlup() const { return teamBlup_; }

    PODVector<Fish*> GetFish(TeamPreference side) const;
    bool AnyConscious(TeamPreference side) const;
    TeamPreference GetWinner() const;

    bool IsCaptureMap(XMLFile* mapFile) const;
    bool IsSoccerMap(XMLFile* mapFile) const;
    bool IsCollectorMap(XMLFile* mapFile) const;
    bool IsSurvivalMap(XMLFile* mapFile) const;
    unsigned MaxTeamSize(XMLFile* mapFile) const;

    void StartMatch();
    void RestartMatch();
    void AssignFish();
    void SpawnFish(Fish* fish);
    void CreateBugs();

private:
    void AssignObjects();
    Vector3 SpawnpointAverage(bool blip = true);
    void AssignSpawnpoints();
    void AssignBases();
    void AssignSatongs();
    void AssignGoals();

    unsigned CountCharacters(const XMLElement& root, const String& characterName) const;
    unsigned CountBlipSpawnpoints(const XMLElement& root)    const;
    unsigned CountBlupSpawnpoints(const XMLElement& root)    const;
    unsigned CountSatongs( const XMLElement& root) const;

    unsigned CountNonCharacters(const XMLElement& root, const String& blockName) const;
    unsigned CountBases(   const XMLElement& root) const;
    unsigned CountGoals(   const XMLElement& root) const;
    unsigned CountVitrines(const XMLElement& root) const;

    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);
    void HandleBugReleased(StringHash eventType, VariantMap& eventData);
    void HandleGoalScored(StringHash eventType, VariantMap& eventData);
    void HandleGameModeChanged(StringHash, VariantMap& eventData);

    unsigned teamSize_;
    Team teamBlip_;
    Team teamBlup_;
    Ball* ball_;
    void ResetBall();
    void ResetScore();
};
#endif // ARCADE_H
