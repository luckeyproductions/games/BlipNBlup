/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "spawnmaster.h"
#include "bubble.h"
#include "ball.h"
#include "rage.h"
#include "game.h"
#include "ui/dash.h"

#include "fish.h"

Fish::Fish(Context* context): Walker(context),
    isBlup_{ false },
    out_{ false },
    bubbleInterval_{ .5f },
    sinceBubble_{ bubbleInterval_ },
    kickInterval_{ .25f },
    sinceKick_{ kickInterval_ },
    blink_{},
    bubbleAnim_{ "Animations/Fish/Bubble.ani" },
    kickAnim_{ "Animations/Fish/Kick.L.ani", "Animations/Fish/Kick.R.ani" }
{
    runThrust_ = 9000.f;
    maxRunSpeed_ = 6.f;

    jumpForce_ = 34.f;
    jumpInterval_ = .1f;
    doubleJumper_ = true;

    idleAnim_   = "Animations/Fish/Idle.ani";
    walkAnim_   = "Animations/Fish/Walk.ani";
    midairAnim_ = "Animations/Fish/Midair.ani";
}

void Fish::RegisterObject(Context* context)
{
    context->RegisterFactory<Fish>();
}

void Fish::OnNodeSet(Node *node)
{
    if (!node)
        return;

    node_->SetScale(1.23f);

    Walker::OnNodeSet(node);

    model_->SetModel(RES(Model, "Models/Blip.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
    model_->GetNode()->SetPosition(Vector3::DOWN * .523f);

    rigidBody_->SetCollisionLayer(L_CHARACTERS);
    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);
    rigidBody_->SetMass(2.f);
    collider_->SetCapsule(.8f, 1.f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Fish, HandleNodeCollisionStart));
    //    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Fish, HandlePostRenderUpdate));
}

void Fish::Set(const Vector3& position, const Quaternion& rotation)
{
    Walker::Set(position, rotation);

    sinceBubble_ = bubbleInterval_;
    out_ = false;
}

void Fish::BecomeBlip()
{
    model_->SetModel(RES(Model, "Models/Blip.mdl"));
    model_->SetMorphWeight(1, 0.f);
    isBlup_ = false;
}

void Fish::BecomeBlup()
{
    model_->SetModel(RES(Model, "Models/Blup.mdl"));
    model_->SetMorphWeight(1, 1.f);
    isBlup_ = true;
}

void Fish::Update(float timeStep)
{
    Walker::Update(timeStep);

    sinceBubble_ += timeStep;
    sinceKick_ += timeStep;

    // Fade kick
    if (sinceKick_ > .34f)
    {
        for (const String& kickAnim: { kickAnim_.first_, kickAnim_.second_ })
            animCtrl_->Stop(kickAnim, .23f);
    }

    //Blink
    float blinkWeight{ 1.f };
    if (!out_)
    {
        blinkWeight = Lerp(model_->GetMorphWeight(0u), 0.f, Clamp(timeStep * 13.f, 0.f, 1.f));
        blink_ -= timeStep;

        if (blink_ < 0.f)
        {
            blink_ = Random(1.3f, 3.2f);
            model_->SetMorphWeight(0, 1.f);
        }
    }

    model_->SetMorphWeight(0, blinkWeight);
}

void Fish::FixedUpdate(float timeStep)
{
    if (out_)
        return;

    Walker::FixedUpdate(timeStep);
}

void Fish::HandleAction(int actionId)
{
    if (out_)
        return;

    switch (actionId)
    {
    case PIA_RUN:       /*Run*/         break;
    case PIA_JUMP:      Jump();         break;
    case PIA_BUBBLE:    BlowBubble();   break;
    case PIA_INTERACT:  Kick();         break;
    default: break;
    }
}

void Fish::BlowBubble()
{
    if (sinceBubble_ < bubbleInterval_)
        return;

    sinceBubble_ = 0.f;

    Bubble* bubble{ SPAWN->Create<Bubble>(true, node_->GetPosition() + node_->GetDirection() * .1f) };
    RigidBody* bubbleBody{ bubble->GetNode()->GetComponent<RigidBody>() };
    bubbleBody->SetLinearVelocity(rigidBody_->GetLinearVelocity());
    bubbleBody->ApplyImpulse(node_->GetDirection() * 2.3f);

    animCtrl_->Play(bubbleAnim_, 1, false, .05f);
    animCtrl_->SetTime(bubbleAnim_, 0.f);
    animCtrl_->SetStartBone(bubbleAnim_, "Mouth");
    model_->SetMorphWeight(0, 1.f);

    PlaySample("ShootBubble.wav");
}

void Fish::Kick()
{
    if (sinceKick_ < kickInterval_)
        return;

    sinceKick_ = 0.f;

    int sideKick{ 0 };
    if (RigidBody* nearest{ DetermineKickHit(sideKick) })
    {
        Node* nearNode{ nearest->GetNode() };
        Bubble* bubble{ nearNode->GetComponent<Bubble>() };
        Rage* rage{ nearNode->GetComponent<Rage>() };

        if (bubble && bubble->IsEmpty())
        {
            bubble->Disable();
        }
        else if (nearest->GetMass() != 0.f)
        {
            nearest->ApplyImpulse(node_->GetWorldDirection() * 23.f + Vector3::UP * 5.f);
            if (bubble)
                nearest->ApplyTorque(node_->GetWorldRight() * 235.f);
            else if (nearNode->HasComponent<Ball>())
            {
                const float offCenter{ (nearest->GetNode()->GetWorldPosition() - node_->GetWorldPosition()).DotProduct(node_->GetWorldRight()) };
                nearest->ApplyTorqueImpulse(node_->GetWorldDirection() * offCenter * 7.f);
            }
            else if (rage)
                rage->FillAnger();
        }

        PlaySample("Hit.wav", Random(.95f, 1.1f), 1.f, false);
    }

    PlayKickAnim(sideKick);
}

void Fish::PlayKickAnim(int side)
{
    bool kickRight{ (side == 0 ? RandomBool() : side == 1) };
    bool inWalk{ animCtrl_->IsPlaying(walkAnim_) };
    float weightGain{ 0.f };
    if (inWalk)
    {
        const float normalizedTime{ animCtrl_->GetTime(walkAnim_) / animCtrl_->GetLength(walkAnim_) };
        kickRight = Abs(.5f - normalizedTime) < .25f;
        weightGain = .25f * (1.f - PowN(2.f * Mod(normalizedTime + .875f, .5f), 2));
    }
    const String& kickAnim{ (kickRight ? kickAnim_.second_ : kickAnim_.first_) };
    animCtrl_->PlayExclusive(kickAnim, 1, false, .05f);
    animCtrl_->SetTime(kickAnim, 0.f);
    animCtrl_->SetSpeed(kickAnim, 1.7f);
    animCtrl_->SetBlendMode(kickAnim, ABM_ADDITIVE);
    animCtrl_->SetWeight(kickAnim, 1.f + weightGain);
}

RigidBody* Fish::DetermineKickHit(int& sideKick)
{
    PhysicsRaycastResult result{};
    RigidBody* nearest{ nullptr };
    float distance{ M_INFINITY };

    for (int r{ -1 }; r <= 1; ++r)
    {
        GetScene()->GetComponent<PhysicsWorld>()->RaycastSingle(result, GetKickRay(r * .5f), 1.f, L_BUBBLES);
        if (result.distance_ < distance && result.body_->GetMass() != 0.f)
        {
            nearest  = result.body_;
            distance = result.distance_;
            sideKick = r;
        }
    }

    return nearest;
}

Ray Fish::GetKickRay(float toRight)
{
    return { node_->GetWorldPosition() + node_->GetWorldRight() * toRight, node_->GetWorldDirection() };
}

void Fish::Faint()
{
    if (out_)
        return;

    out_ = true;
    ResetInput();
    rigidBody_->SetFriction(.8f);
    animCtrl_->PlayExclusive(idleAnim_, 0, true, .1f);
    animCtrl_->SetSpeed(idleAnim_, 0.f);

    DASH->SetFishConscious(isBlup_, false);

    VariantMap eventData{};
    eventData[FishFainted::P_FISH] = this;
    SendEvent(E_FISHFAINTED, eventData);
}

void Fish::Revive()
{
    out_ = false;
    DASH->SetFishConscious(isBlup_, true);
}

void Fish::HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData)
{
    Walker::HandleNodeCollisionStart(eventType, eventData);

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    Controllable* otherControllable{ otherNode->GetDerivedComponent<Controllable>() };
    if (otherControllable && otherControllable->IsHarmful())
        Faint();

    Bubble* bubble{ otherNode->GetComponent<Bubble>() };
    if (bubble && bubble->IsEmpty() && out_)
    {
        Revive();
        bubble->Pop();
    }
}

void Fish::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const Ray ray{ GetKickRay() };
    GetScene()->GetComponent<DebugRenderer>()->AddLine(ray.origin_, ray.origin_ + ray.direction_, Color::WHITE, false);
}
