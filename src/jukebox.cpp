/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "blockmap.h"

#include "jukebox.h"

JukeBox::JukeBox(Context* context): Object(context),
    currentSong_{ MUSIC_NONE },
    musicNode_{ nullptr },
    tracks_{}
{
    Scene* musicScene{ new Scene(context_) };
    musicNode_ = musicScene->CreateChild("Music");

    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(JukeBox, HandleGameStatusChanged));
    SubscribeToEvent(E_MUSICFADED, DRY_HANDLER(JukeBox, HandleMusicFaded));
}

void JukeBox::Play(Music music)
{
    if (currentSong_ == music)
        return;

    currentSong_ = music;
    RestartCurrent();
}

void JukeBox::Play(const String& song, bool loop, float gain)
{
    const float fadeTime{ .05f + .2f * song.IsEmpty() };

    for (SoundSource* source: tracks_)
    {
        if (fadeTime > 0.f)
        {
            SharedPtr<ValueAnimation> fade{ MakeShared<ValueAnimation>(context_) };
            fade->SetKeyFrame(0.f, { source->GetGain() });
            fade->SetKeyFrame(fadeTime, { .0f });
            fade->SetEventFrame(fadeTime, E_MUSICFADED);
            source->SetAttributeAnimation("Gain", fade, WM_ONCE);
        }
        else
        {
            tracks_.Remove(source);
            source->Remove();
        }
    }

    if (!song.IsEmpty())
    {
        Sound* music{ RES(Sound, String{ "Music/" } + song) };
        music->SetLooped(loop);
        SoundSource* musicSource{ musicNode_->CreateComponent<SoundSource>() };
        musicSource->SetSoundType(SOUND_MUSIC);
        musicSource->Play(music);
        musicSource->SetGain(gain);
        tracks_.Push(musicSource);
    }
}

void JukeBox::RestartCurrent()
{
    if (currentSong_ == MUSIC_NONE)
    {
        Play("");
        return;
    }

    Song song{ songs_.At(currentSong_) };
    Play(song.name_, true, song.gain_);
}

void JukeBox::HandleMusicFaded(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SoundSource* source{ static_cast<SoundSource*>(GetEventSender()) };
    tracks_.Remove(source);
    source->Remove();
}

void JukeBox::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    if (status < GS_PLAY)
        Play(MUSIC_MENU);
    else if (status == GS_GAMEOVER)
        Play(MUSIC_NONE);
    else if (status == GS_PLAY)
        Play(GetSubsystem<Game>()->GetWorld().level_->GetMusicTheme());
}
