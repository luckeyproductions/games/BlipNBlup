/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "effectmaster.h"
#include "game.h"
#include "ui/dash.h"
#include "couch.h"
#include "fish.h"
#include "container.h"
#include "catchable.h"
#include "fly.h"
#include "satong.h"
#include "spawnmaster.h"
#include "spawnpoint.h"
#include "base.h"
#include "goal.h"
#include "ball.h"

#include "arcade.h"

void Arcade::RegisterObject(Context* context)
{
    context->RegisterFactory<Arcade>();
    context->RegisterFactory<SpawnPoint>();
    context->RegisterFactory<Base>();
    context->RegisterFactory<Goal>();
    context->RegisterFactory<Ball>();
}

Arcade::Arcade(Context* context): Object(context),
    teamSize_{ 0u },
    teamBlip_{ TP_BLIP },
    teamBlup_{ TP_BLUP },
    ball_{ nullptr }
{
}

bool Arcade::IsCaptureMap(XMLFile* mapFile) const
{
    if (!mapFile)
        return false;

    const XMLElement root{ mapFile->GetRoot("blockmap") };
    if (root.IsNull())
        return false;

    return CountBases(root) == 2u && CountSatongs(root) == 2 && MaxTeamSize(mapFile) > 0u;
}

bool Arcade::IsSoccerMap(XMLFile* mapFile) const
{
    if (!mapFile)
        return false;

    const XMLElement root{ mapFile->GetRoot("blockmap") };
    if (root.IsNull())
        return false;

    return CountGoals(root) == 2u && MaxTeamSize(mapFile) > 0u;
}

bool Arcade::IsCollectorMap(XMLFile* mapFile) const
{
    if (!mapFile)
        return false;

    const XMLElement root{ mapFile->GetRoot("blockmap") };
    if (root.IsNull())
        return false;

    return CountVitrines(root) == 2u && MaxTeamSize(mapFile) > 0u;
}

bool Arcade::IsSurvivalMap(XMLFile* mapFile) const
{
    if (!mapFile)
        return false;

    const XMLElement root{ mapFile->GetRoot("blockmap") };
    if (root.IsNull())
        return false;

    return CountSatongs(root) == 2 && MaxTeamSize(mapFile) > 0u;
}

unsigned Arcade::MaxTeamSize(XMLFile* mapFile) const
{
    if (!mapFile)
        return 0u;

    const XMLElement root{ mapFile->GetRoot("blockmap") };
    if (root.IsNull())
        return 0u;

    return Min(CountBlipSpawnpoints(root), CountBlupSpawnpoints(root));
}

void Arcade::StartMatch()
{
    UnsubscribeFromAllEvents();

    AssignObjects();

    if (GAME->GetGameMode() != GM_SOCCER)
        CreateBugs();
    else if (!ball_)
        ball_ = SPAWN->Create<Ball>();

    SubscribeToEvent(ball_, E_GOALSCORED, DRY_HANDLER(Arcade, HandleGoalScored));
    SubscribeToEvent(GAME->GetWorld().playScene_, E_SCENEUPDATE, DRY_HANDLER(Arcade, HandleSceneUpdate));
    SubscribeToEvent(E_GAMEMODECHANGED, DRY_HANDLER(Arcade, HandleGameModeChanged));

    AssignFish();
    RestartMatch();
}

void Arcade::RestartMatch()
{
    SPAWN->DisableAllDerived<Container>();

    for (Fish* fish: GetTeamBlip().fish_ + GetTeamBlup().fish_)
        SpawnFish(fish);

    GameMode mode{ GAME->GetGameMode() };
    switch (mode)
    {
    default: break;
    case GM_CTF:
        for (Team* team: { &teamBlip_, &teamBlup_ })
        {
            Fly* fly{ team->bug_->GetComponent<Fly>() };
            fly->Set(team->base_->GetNode()->GetWorldPosition() + Vector3::UP);
        }
    break;
    case GM_SOCCER:
        ResetBall();
    break;
    case GM_SURVIVAL:
        for (Satong* satong: { teamBlip_.satong_, teamBlup_.satong_ })
            satong->SetSatiation(.5f);

        SPAWN->Restart();
        GAME->SetStatus(GS_PLAY);
    break;
    }

    ResetScore();

    SendEvent(E_RESTART);
}

void Arcade::ResetScore()
{
    DASH->SetScore(teamBlip_.score_ = 0, teamBlup_.score_ = 0);
}

void Arcade::ResetBall()
{
    Goal* blipGoal{ teamBlip_.goal_ };
    Goal* blupGoal{ teamBlup_.goal_ };
    if (blipGoal && blupGoal)
    {
        const Vector3 averageGoalPosition{ (blipGoal->GetNode()->GetWorldPosition() + blupGoal->GetNode()->GetWorldPosition()) * .5f };
        PhysicsRaycastResult result{};
        ball_->GetScene()->GetComponent<PhysicsWorld>()->RaycastSingle(result, { averageGoalPosition + Vector3::UP * 10.f, Vector3::DOWN }, 23.f, L_WORLD);
        if (result.body_)
        {
            ball_->Set(result.position_ + Vector3::UP);
            return;
        }
    }

    ball_->Set(Vector3::ZERO);
}

void Arcade::SetTeamSize(unsigned size)
{
    teamSize_ = size;

    for (bool teamBlup: { false, true })
    {
        Team& team{ (teamBlup ? teamBlup_ : teamBlip_) };

        while (team.fish_.Size() > teamSize_)
        {
            team.fish_.Back()->Disable();
            team.fish_.Pop();
        }

        for (unsigned f{ 0u }; f < teamSize_; ++f)
        {
            Fish* fish;
            if (f < team.fish_.Size())
            {
                fish = team.fish_.At(f);
            }
            else
            {
                fish = SPAWN->Create<Fish>(true, Vector3::ZERO);
                team.fish_.Push(fish);
            }

            if (!teamBlup)
                fish->BecomeBlip();
            else
                fish->BecomeBlup();
        }
    }
}

PODVector<Fish*> Arcade::GetFish(TeamPreference side) const
{
    switch (side)
    {
    default:
    case TP_RANDOM: return teamBlip_.fish_ + teamBlup_.fish_;
    case TP_BLIP:   return teamBlip_.fish_;
    case TP_BLUP:   return teamBlup_.fish_;
    }
}

bool Arcade::AnyConscious(TeamPreference side) const
{
    for (Fish* fish: GetFish(side))
        if (fish->IsConscious())
            return true;

    return false;
}

TeamPreference Arcade::GetWinner() const
{
    GameMode mode{ GAME->GetGameMode() };
    switch (mode) {
    default:
    {
        if (teamBlip_.score_ == teamBlup_.score_)
            return TP_RANDOM;
        else if (teamBlip_.score_ > teamBlup_.score_)
            return TP_BLIP;
        else
            return TP_BLUP;
    }
    case GM_SURVIVAL:
    {
        const bool blipsConscious{ AnyConscious(TP_BLIP) };
        const bool blupsConscious{ AnyConscious(TP_BLUP) };
        const float blipsSatiation{ teamBlip_.satong_->GetSatiation() };
        const float blupsSatiation{ teamBlup_.satong_->GetSatiation() };

        if (!blipsConscious && !blupsConscious)
            return TP_RANDOM;
        else if (!blupsConscious)
            return TP_BLIP;
        else if (!blipsConscious)
            return TP_BLUP;
        else if (blipsSatiation == blupsSatiation)
            return TP_RANDOM;
        else if (blipsSatiation > blupsSatiation)
            return TP_BLIP;
        else
            return TP_BLUP;
    }
    }
}

void Arcade::AssignObjects()
{
    AssignSpawnpoints();

    if (!teamBlip_.spawnPoints_.IsEmpty())
    {
        GameMode mode{ GAME->GetGameMode() };
        switch (mode)
        {
        default: break;
        case GM_CTF:      AssignBases();   [[fallthrough]];
        case GM_SURVIVAL: AssignSatongs(); break;
        case GM_SOCCER:   AssignGoals();   break;
        }
    }
}

Vector3 Arcade::SpawnpointAverage(bool blip)
{
    Vector3 spawnAverage{};
    if (!teamBlip_.spawnPoints_.IsEmpty())
    {
        for (SpawnPoint* point: (blip ? teamBlip_.spawnPoints_ : teamBlup_.spawnPoints_))
            spawnAverage += point->GetNode()->GetWorldPosition();
        spawnAverage /= teamBlip_.spawnPoints_.Size();
    }

    return spawnAverage;
}

void Arcade::AssignSpawnpoints()
{
    for (Team* t: { &teamBlip_, &teamBlup_ })
        t->spawnPoints_.Clear();

    Scene* scene{ GAME->GetWorld().playScene_ };
    PODVector<SpawnPoint*> allPoints{};
    PODVector<SpawnPoint*> matchingPoints{};
    scene->GetComponents<SpawnPoint>(allPoints, true);
    for (SpawnPoint* point: allPoints)
    {
        if (point->GetTeam() == TP_BLIP)
            teamBlip_.spawnPoints_.Push(point);
        else
            teamBlup_.spawnPoints_.Push(point);
    }
}

void Arcade::AssignBases()
{
    Scene* scene{ GAME->GetWorld().playScene_ };
    PODVector<Base*> allBases{};
    scene->GetComponents<Base>(allBases, true);
    Base* blipBase{ SPAWN->GetNearest<Base>(SpawnpointAverage(true), M_INFINITY) };
    if (blipBase)
    {
        blipBase->SetTeam(TP_BLIP);
        teamBlip_.base_ = blipBase;
        allBases.Remove(blipBase);

        teamBlup_.base_ = allBases.Front();
        teamBlup_.base_->SetTeam(TP_BLUP);
        allBases.Remove(teamBlup_.base_);
    }

    for (Base* b: allBases)
        b->SetTeam(TP_RANDOM);
}

void Arcade::AssignSatongs()
{
    Scene* scene{ GAME->GetWorld().playScene_ };
    PODVector<Satong*> allSatongs{};
    scene->GetComponents<Satong>(allSatongs, true);
    Satong* blipSatong{ SPAWN->GetNearest<Satong>(SpawnpointAverage(true), M_INFINITY) };
    if (blipSatong)
    {
        blipSatong->SetTeam(TP_BLIP);
        teamBlip_.satong_ = blipSatong;
        allSatongs.Remove(blipSatong);

        teamBlup_.satong_ = allSatongs.Front();
        teamBlup_.satong_->SetTeam(TP_BLUP);
        allSatongs.Remove(teamBlup_.satong_);
    }

    for (Satong* b: allSatongs)
        b->SetTeam(TP_RANDOM);
}

void Arcade::AssignGoals()
{
    Scene* scene{ GAME->GetWorld().playScene_ };
    PODVector<Goal*> allGoals{};
    scene->GetComponents<Goal>(allGoals, true);
    Goal* blipGoal{ SPAWN->GetNearest<Goal>(SpawnpointAverage(true), M_INFINITY) };
    if (blipGoal)
    {
        blipGoal->SetTeam(TP_BLIP);
        teamBlip_.goal_ = blipGoal;
        allGoals.Remove(blipGoal);

        teamBlup_.goal_ = allGoals.Front();
        teamBlup_.goal_->SetTeam(TP_BLUP);
        allGoals.Remove(teamBlup_.goal_);
    }

    for (Goal* b: allGoals)
        b->SetTeam(TP_RANDOM);
}

void Arcade::AssignFish()
{
    PODVector<Player*> players{ GetSubsystem<Couch>()->GetPlayers() };
    const PODVector<Fish*>& blips{ GetTeamBlip().fish_ };
    const PODVector<Fish*>& blups{ GetTeamBlup().fish_ };
    unsigned assignedBlips{ 0u };
    unsigned assignedBlups{ 0u };

    for (Player* p: players)
    {
        const bool freeBlips{ assignedBlips < blips.Size() };
        const bool freeBlups{ assignedBlups < blups.Size() };
        PODVector<TeamPreference> freeTeams{};
        if (freeBlips) freeTeams.Push(TP_BLIP);
        if (freeBlups) freeTeams.Push(TP_BLUP);

        TeamPreference team{ p->GetTeamPreference() };
        if (team == TP_RANDOM)
        {
            if (!freeTeams.IsEmpty())
                team = freeTeams.At(Random(static_cast<int>(freeTeams.Size())));
        }
        else if (!freeTeams.Contains(team))
        {
            team = static_cast<TeamPreference>(team ^ 1u);
        }

        if (team == TP_BLIP && freeBlips)
            p->SetControl(blips.At(assignedBlips++));
        else if (team == TP_BLUP && freeBlups)
            p->SetControl(blups.At(assignedBlups++));
        else
            p->SetControl(nullptr);
    }
}

void Arcade::SpawnFish(Fish* fish)
{
    PODVector<SpawnPoint*> matchingPoints{ (fish->IsBlup() ? teamBlup_.spawnPoints_ : teamBlip_.spawnPoints_) };
    PODVector<SpawnPoint*> freePoints{};
    for (SpawnPoint* point: matchingPoints)
    {
        bool taken{ false };
        for (Fish* fish: teamBlip_.fish_ + teamBlup_.fish_)
        {
            if (fish->GetNode()->GetWorldPosition().DistanceToPoint(point->GetNode()->GetWorldPosition()) < .5f)
                taken = true;
        }

        if (!taken)
            freePoints.Push(point);
    }

    Node* spawnNode{ nullptr };
    if (!freePoints.IsEmpty())
    {
        const unsigned pointIndex{ static_cast<unsigned>(Random(static_cast<int>(freePoints.Size()))) };
        spawnNode = freePoints.At(pointIndex)->GetNode();
    }
    else
    {
        const unsigned pointIndex{ static_cast<unsigned>(Random(static_cast<int>(matchingPoints.Size()))) };
        spawnNode = matchingPoints.At(pointIndex)->GetNode();
    }

    if (spawnNode)
        fish->Set(spawnNode->GetWorldPosition(), spawnNode->GetWorldRotation());
}

void Arcade::CreateBugs()
{
    GameMode mode{ GAME->GetGameMode() };

    switch (mode)
    {
    default: break;
    case GM_CTF:
        for (Team* team: { &teamBlip_, &teamBlup_ })
        {
            Fly* fly{ SPAWN->Create<Fly>() };
            fly->Set(team->base_->GetNode()->GetWorldPosition() + Vector3::UP);
            team->bug_ = WeakPtr<Catchable>{ fly->GetComponent<Catchable>() };
            team->bug_->SetTeam(team->side_);

            SubscribeToEvent(team->bug_, E_BUGRELEASED, DRY_HANDLER(Arcade, HandleBugReleased));
        }
    break;
    }
}

unsigned Arcade::CountCharacters(const XMLElement& root, const String& characterName) const
{
    unsigned characterCount{ 0u };
    int characterSetId{ -1 };
    int characterBlockId{ -1 };

    {
        // Find character set
        String characterSetName{};
        XMLElement blockSetRefElem{ root.GetChild("blockset") };
        while (blockSetRefElem)
        {
            const String blockSetName{ blockSetRefElem.GetAttribute("name") };
            if (blockSetName.Contains("Characters"))
            {
                characterSetId = blockSetRefElem.GetUInt("id");
                characterSetName = blockSetName;

                break;
            }

            blockSetRefElem = blockSetRefElem.GetNext("blockset");
        }

        // Find block in set
        XMLFile* characterSetFile{ RES(XMLFile, characterSetName) };
        XMLElement characterSetRoot{ characterSetFile->GetRoot("blockset") };
        XMLElement blockElem{ characterSetRoot.GetChild("block") };
        while (blockElem)
        {
            if (blockElem.GetAttribute("name") == characterName)
            {
                characterBlockId = blockElem.GetUInt("id");

                break;
            }

            blockElem = blockElem.GetNext("block");
        }
    }

    // Traverse layers
    if (characterSetId != -1 && characterBlockId != -1)
    {
        XMLElement layerElem{ root.GetChild("gridlayer") };
        while (layerElem)
        {
            XMLElement gridBlockElem{ layerElem.GetChild("gridblock") };
            while (gridBlockElem)
            {
                if (gridBlockElem.GetInt("set")   == characterSetId
                 && gridBlockElem.GetInt("block") == characterBlockId)
                    ++characterCount;

                gridBlockElem = gridBlockElem.GetNext("gridblock");
            }

            layerElem = layerElem.GetNext("gridlayer");
        }
    }

//    Log::Write(LOG_INFO, "Counted " + String{ characterCount } + " " + characterName + (characterCount == 1u ? "" : "s") );

    return characterCount;
}

unsigned Arcade::CountBlipSpawnpoints(const XMLElement& root) const
{
    return CountCharacters(root, "Blip");
}
unsigned Arcade::CountBlupSpawnpoints(const XMLElement& root) const
{
    return CountCharacters(root, "Blup");
}

unsigned Arcade::CountSatongs(const XMLElement& root) const
{
    return CountCharacters(root, "Satong");
}

unsigned Arcade::CountNonCharacters(const XMLElement& root, const String& blockName) const
{
    if (blockName.IsEmpty())
        return 0u;

    unsigned blockCount{ 0u };
    PODVector<Pair<int, int>> blockSignatures{};

    // Find block in non-character sets
    {
        String blockSetName{};
        XMLElement blockSetRefElem{ root.GetChild("blockset") };
        while (blockSetRefElem)
        {
            const String blockSetName{ blockSetRefElem.GetAttribute("name") };
            if (!blockSetName.Contains("Characters"))
            {
                XMLFile* blockSetFile{ RES(XMLFile, blockSetName) };
                if (blockSetFile)
                {
                    XMLElement blockSetRoot{ blockSetFile->GetRoot("blockset") };
                    XMLElement blockElem{ blockSetRoot.GetChild("block") };
                    while (blockElem)
                    {
                        if (blockElem.GetAttribute("name").Contains(blockName))
                        {
                            blockSignatures.Push({ blockSetRefElem.GetInt("id"), blockElem.GetInt("id") });
                        }

                        blockElem = blockElem.GetNext("block");
                    }
                }
            }

            blockSetRefElem = blockSetRefElem.GetNext("blockset");
        }
    }

    // Traverse layers
    if (!blockSignatures.IsEmpty())
    {
        XMLElement layerElem{ root.GetChild("gridlayer") };
        while (layerElem)
        {
            XMLElement gridBlockElem{ layerElem.GetChild("gridblock") };
            while (gridBlockElem)
            {
                for (const Pair<int, int>& sig: blockSignatures)
                if (gridBlockElem.GetInt("set")   == sig.first_
                 && gridBlockElem.GetInt("block") == sig.second_)
                    ++blockCount;

                gridBlockElem = gridBlockElem.GetNext("gridblock");
            }

            layerElem = layerElem.GetNext("gridlayer");
        }
    }

//    Log::Write(LOG_INFO, "Counted " + String{ blockCount } + " matching block" + (blockCount == 1u ? "" : "s") );

    return blockCount;
}

unsigned Arcade::CountBases(const XMLElement& root) const
{
    return CountNonCharacters(root, "Base");
}

unsigned Arcade::CountGoals(const XMLElement& root) const
{
    return CountNonCharacters(root, "Goal");
}

unsigned Arcade::CountVitrines(const XMLElement& root) const
{
    return CountNonCharacters(root, "Vitrine");
}

void Arcade::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    GameMode mode{ GAME->GetGameMode() };

    switch (mode)
    {
    default: break;
    case GM_CTF:
        for (Team* team: { &teamBlip_, &teamBlup_ })
        {
            if (!team->bug_->GetNode()->IsEnabled())
            {
                Node* bugNode{ team->bug_->GetNode() };
                bugNode->GetDerivedComponent<Controllable>()->Set(team->base_->GetNode()->GetWorldPosition() + Vector3::UP);
                bugNode->SetScale(0.f);
                FX->ScaleTo(bugNode, 1.f, .23f);
            }
        }
    break;
    }
}

void Arcade::HandleBugReleased(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Catchable* devoured{ static_cast<Catchable*>(eventData[BugReleased::P_DEVOURED].GetPtr()) })
    {
        Satong* devourer{ static_cast<Satong*>(eventData[BugReleased::P_DEVOURER].GetPtr()) };
        if (!devourer)
            return;

        if (devourer == teamBlip_.satong_)
        {
            ++teamBlip_.score_;
            Log::Write(LOG_INFO, "Team Blip scored!");
        }
        else if (devourer == teamBlup_.satong_)
        {
            ++teamBlup_.score_;
            Log::Write(LOG_INFO, "Team Blup scored!");
        }

        DASH->SetScore(teamBlip_.score_, teamBlup_.score_);
    }
}

void Arcade::HandleGoalScored(StringHash /*eventType*/, VariantMap& eventData)
{
    TeamPreference team{ static_cast<TeamPreference>(eventData[GoalScored::P_SIDE].GetInt()) };

    switch (team)
    {
    default: break;
    case 0: ++teamBlip_.score_; break;
    case 1: ++teamBlup_.score_; break;
    }

    DASH->SetScore(teamBlip_.score_, teamBlup_.score_);
    ResetBall();
}

void Arcade::HandleGameModeChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    UnsubscribeFromAllEvents();
}
