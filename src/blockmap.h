/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DUNGEON_H
#define DUNGEON_H

#include "block.h"
#include "jukebox.h"

#include "mastercontrol.h"

struct BlockInfo
{
    unsigned id_{};
    String name_{};
    String model_{};
    StringVector materials_{};
};

class BlockMap: public Component
{
    DRY_OBJECT(BlockMap, Component);

public:
    static void RegisterObject(Context* context);
    BlockMap(Context* context);

    void Clear();

    Vector3 GetExtents() const { return mapSize_ * blockSize_; }

    void LoadXMLFile(XMLFile* blockMap);
    bool LoadXML(const XMLElement& blockMap) override;
    String GetFilename() const
    {
        if (!xmlFile_)
            return "";
        else
            return xmlFile_->GetName();
    }

    bool HasBlockGroup(unsigned blockHash) const { return blockGroups_.Contains(blockHash); }
    StaticModelGroup* AddBlockGroup(unsigned blockHash, Model* blockModel, const PODVector<Material*>& materials);
    void AddBlockInstance(unsigned blockHash, Node* node)
    {
        if (HasBlockGroup(blockHash))
            blockGroups_[blockHash]->AddInstanceNode(node);
    }

    JukeBox::Music GetMusicTheme() const { return musicTheme_; }
    bool HasWater() const { return waterNode_ != nullptr; }
    Node* GetWaterNode() const { return waterNode_; }

protected:
    void OnNodeSet(Node* node) override;

private:
    void DeriveMusicTheme(const String& blockSetName);
    void AddWater();

    SharedPtr<XMLFile> xmlFile_;
    IntVector3 mapSize_;
    Vector3 blockSize_;
    JukeBox::Music musicTheme_;
    HashMap<unsigned, StaticModelGroup*> blockGroups_;
    Node* waterNode_;

//    RigidBody* rigidBody_;
    void UpdateZoneParameters();
};

#endif // DUNGEON_H
