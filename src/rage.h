/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RAGE_H
#define RAGE_H

#include "luckey.h"

class Rage: public LogicComponent
{
    DRY_OBJECT(Rage, LogicComponent);

public:
    static void RegisterObject(Context* context);

    Rage(Context* context);
    void Update(float timeStep) override;

    void SetCooldown(float time) { cooldown_ = time; }
    float GetAnger() const { return anger_; }
    void FillAnger() { anger_ = 1.f; sinceAngered_ = 0.f;}
    void ResetAnger() { anger_ = 0.f; sinceAngered_ = cooldown_; }

private:
    float anger_;
    float cooldown_;
    float sinceAngered_;
};

#endif // RAGE_H
