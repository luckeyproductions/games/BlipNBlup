/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "game.h"
#include "inputmaster.h"
#include "effectmaster.h"
#include "spawnmaster.h"
#include "couch.h"
#include "ui/gui.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    ySectors_{ 1 },
    drawDebug_{ false },
    storagePath_{}
{
}

void MasterControl::Setup()
{
    engineParameters_[EP_WINDOW_TITLE] = "Blip 'n Blup: Skyward Adventures";
    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "blipnblup.log";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WINDOW_RESIZABLE] = true;
//    engineParameters_[EP_WINDOW_WIDTH] = 1280;
//    engineParameters_[EP_WINDOW_HEIGHT] = 720;
//    engineParameters_[EP_FULL_SCREEN] = false;

    //Add resource path
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    String resourcePath{ "Resources" };
    if (!fs->DirExists(AddTrailingSlash(fs->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (fs->DirExists(installedResources))
            resourcePath = installedResources;
    }

    if (resourcePath == "Resources")
        storagePath_ = resourcePath;
    else
        storagePath_ = RemoveTrailingSlash(fs->GetAppPreferencesDir("luckey", "blipnblup"));

    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;
}

void MasterControl::Start()
{
    SetRandomSeed(TIME->GetSystemTime());
    ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());
    INPUT->SetMouseMode(MM_FREE);
    SetupDefaultRenderPath();
    SubscribeToEvents();

    GUI::RegisterObject(context_);
    Game::RegisterObject(context_);

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<EffectMaster>();
    context_->RegisterSubsystem<SpawnMaster>();
    context_->RegisterSubsystem<GUI>()->UpdateSizes();
    context_->RegisterSubsystem<Game>()->SetStatus(GS_SPLASH);
    context_->RegisterSubsystem<Couch>();
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::SubscribeToEvents()
{
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, HandlePostRenderUpdate));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(MasterControl, HandleScreenMode));
}

void MasterControl::SetupDefaultRenderPath()
{
    RenderPath* effectRenderPath{ RENDERER->GetDefaultRenderPath() };
    effectRenderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    effectRenderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", .42f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2{ .9f, .23f });
}

void MasterControl::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
//    if (INPUT->GetKeyPress(KEY_SPACE))
//        drawDebug_ = !drawDebug_;

    if (drawDebug_)
    {
        Game* game{ GetSubsystem<Game>() };
        Scene* playScene{ game->GetWorld().playScene_ };
        if (playScene)
            playScene->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
    }
}

float MasterControl::Sine(const float freq, const float min, const float max, const float shift)
{
    float phase{SinePhase(freq, shift)};
    float add{ .5f * (min + max) };
    return LucKey::Sine(phase) * .5f * (max - min) + add;
}

float MasterControl::Cosine(const float freq, const float min, const float max, const float shift)
{
    return Sine(freq, min, max, shift + .25f);
}

float MasterControl::SinePhase(float freq, float shift)
{
    return M_TAU * (freq * TIME->GetElapsedTime() + shift);
}


void MasterControl::UpdateViewportRects()
{
    const IntVector2 windowSize{ GRAPHICS->GetSize() };
    const int numViewports{ static_cast<int>(RENDERER->GetNumViewports()) };

    int xSectors{ 1 };
    int ySectors{ 1 };
    if (windowSize.x_ > windowSize.y_)
    {
        xSectors = CeilToInt(Sqrt(static_cast<float>(numViewports)));
        ySectors = numViewports / xSectors + (numViewports % xSectors != 0);
    }
    else
    {
        ySectors = CeilToInt(Sqrt(static_cast<float>(numViewports)));
        xSectors = numViewports / ySectors + (numViewports % ySectors != 0);
    }

    const IntVector2 sectors{ xSectors, ySectors };
    IntVector2 screenCut{ windowSize / sectors };
    const IntVector2 rest{ windowSize - screenCut * sectors };
    if (rest.x_ > 0)
        ++screenCut.x_;
    if (rest.y_ > 0)
        ++screenCut.y_;

    for (int v{ 0u }; v < numViewports; ++v)
    {
        const int righth  { v % xSectors };
        const int bottomth{ v / xSectors };
        const int right { screenCut.x_ * righth   };
        const int bottom{ screenCut.y_ * bottomth };

        Viewport* viewport{ RENDERER->GetViewport(v) };
        viewport->SetRect({ right, bottom, screenCut.x_ + right, screenCut.y_ + bottom });
    }

    GameStatus status{ GAME->GetStatus() };
    GUI* gui{ GetSubsystem<GUI>() };
    if (xSectors * ySectors > numViewports)
    {
        const int blackSectors{ xSectors - numViewports % xSectors };
        const IntVector2 blacknessPosition{ (xSectors - blackSectors) * screenCut.x_, (ySectors - 1) * screenCut.y_ };
        const IntVector2 blacknessSize{ blackSectors * screenCut.x_, screenCut.y_ };
        gui->SetBlackness(blacknessPosition, blacknessSize);
    }
    else if (status > GS_MAIN && status != GS_GAMEOVER)
    {
        gui->SetBlackness(0.f);
    }

    ySectors_ = ySectors;
}

void MasterControl::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateViewportRects();
}
