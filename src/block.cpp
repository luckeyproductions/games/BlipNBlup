/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockmap.h"

#include "block.h"

void Block::RegisterObject(Context* context)
{
    context->RegisterFactory<Block>();
}

Block::Block(Context* context): Component(context),
    collider_{ nullptr }
{
}

void Block::OnNodeSet(Node* node)
{
    if (!node)
        return;

//    node_->CreateComponent<StaticModel>();

    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
    rigidBody->SetCollisionLayer(L_WORLD);
    rigidBody->SetFriction(BLOCK_DEFAULTFRICTION);
    rigidBody->SetRestitution(BLOCK_DEFAULTRESTITUTION);
    collider_ = node_->CreateComponent<CollisionShape>();
}

void Block::Initialize(Model* model, const PODVector<Material*>& materials)
{
    BlockMap* blockMap{ node_->GetParent()->GetComponent<BlockMap>(true) };
    if (!blockMap)
    {
        node_->Remove();
        return;
    }

    const String modelName{ model->GetName() };
    SharedPtr<Model> collisionModel{ nullptr };
    if (model)
    {
        String colliderName{ modelName };
        colliderName.Insert(colliderName.Length() - 4u, "_COLLISION");
        collisionModel = RES_NO(Model, colliderName);

        if (!collisionModel)
            collisionModel = model;
    }

    if (collisionModel)
    {
        collider_->SetTriangleMesh(collisionModel, collisionModel->GetNumGeometryLodLevels(0u) - 1u/*, Vector3::ONE,
                                                              node_->GetPosition(),
                                                              node_->GetRotation()*/);
    }
    else if (!model)
    {
        node_->Remove();
        return;
    }

    // Determine hash
    const unsigned modelNameHash{ (model ? model->GetNameHash().ToHash() : 0u) };
    unsigned materialHash{ 0u };
    for (Material* m: materials)
        materialHash |= m->GetName().ToHash();
    const unsigned blockHash{ modelNameHash ^ materialHash };

    // Add instance
    if (!blockMap->HasBlockGroup(blockHash))
        blockMap->AddBlockGroup(blockHash, model, materials);

    blockMap->AddBlockInstance(blockHash, node_);

    if (modelName.Contains("Ceiling"))
        node_->CreateComponent<Obstacle>();
}
