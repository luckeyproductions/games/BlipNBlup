/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPUTMASTER_H
#define INPUTMASTER_H

#include "gamedefs.h"

#include "luckey.h"

namespace Dry {
class Drawable;
class Node;
class Scene;
class Sprite;
}

class Player;

enum MasterInputAction { MIA_UP = 0, MIA_RIGHT, MIA_DOWN, MIA_LEFT, MIA_CONFIRM, MIA_CANCEL, MIA_PAUSE, MIA_MENU, MIA_INCREMENT, MIA_DECREMENT, MIA_SWITCH, MIA_ALL };
enum PlayerInputAction { PIA_RUN = 0, PIA_JUMP, PIA_BUBBLE, PIA_INTERACT, PIA_FORWARD, PIA_BACK, PIA_LEFT, PIA_RIGHT, PIA_UP, PIA_DOWN };

struct InputActions
{
    PODVector<MasterInputAction> master_;
    HashMap<int, PODVector<PlayerInputAction>> player_;
};

class InputMaster: public Object
{
    DRY_OBJECT(InputMaster, Object);

public:
    InputMaster(Context* context);

    void BindDefault();
    void Unpress(int playerId);

private:
    void SubscribeToEvents();

    void HandleUpdate(          StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(         StringHash eventType, VariantMap& eventData);
    void HandleKeyUp(           StringHash eventType, VariantMap& eventData);
    void HandleJoyButtonDown(   StringHash eventType, VariantMap& eventData);
    void HandleJoyButtonUp(     StringHash eventType, VariantMap& eventData);
    void HandleJoystickAxisMove(StringHash eventType, VariantMap& eventData);

    void HandleActions(const InputActions& actions);
    void HandleMasterActions(const PODVector<MasterInputAction>& actions);
    void HandlePlayerActions(const HashMap<int, PODVector<PlayerInputAction> >& actions);

    void FilterRepeat(PODVector<MasterInputAction>& masterActions);
    bool IsRepeating(MasterInputAction action) const { return lastMasterActions_.Contains(action); }

    Vector3 GetMoveFromActions(const PODVector<PlayerInputAction>& actions) const;
    void CorrectForCameraYaw(Player* player, Vector3& vec3) const;

    HashMap<int, MasterInputAction> keyBindingsMaster_;
    HashMap<int, MasterInputAction> buttonBindingsMaster_;
    HashMap<int, HashMap<int, PlayerInputAction>> keyBindingsPlayer_;
    HashMap<int, HashMap<int, PlayerInputAction>> buttonBindingsPlayer_;

    Vector<int> pressedKeys_;
    HashMap<int, Vector<ControllerButton>> pressedJoystickButtons_;
    HashMap<int, Vector2> leftStickPosition_;
    HashMap<int, Vector2> rightStickPosition_;

    PODVector<MasterInputAction> lastMasterActions_;
    HashMap<int, float> sinceMasterAction_;
    const float masterInterval_;
    const Vector<MasterInputAction> noRepeatMaster_;
    bool wasPaused_;
};

DRY_EVENT(E_MENUINPUT, MenuInput)
{
    DRY_PARAM(P_ACTIONS, Actions); // Buffer
}

#endif // INPUTMASTER_H
