/*
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once

#include <Dry/Audio/Audio.h>
#include <Dry/Audio/Sound.h>
#include <Dry/Audio/SoundListener.h>
#include <Dry/Audio/SoundSource3D.h>
#include <Dry/Container/HashBase.h>
#include <Dry/Container/HashMap.h>
#include <Dry/Container/Vector.h>
#include <Dry/Core/CoreEvents.h>
#include <Dry/Core/ProcessUtils.h>
#include <Dry/Core/Spline.h>
#include <Dry/Engine/Application.h>
#include <Dry/Engine/Console.h>
#include <Dry/Engine/DebugHud.h>
#include <Dry/Engine/Engine.h>
#include <Dry/Engine/EngineDefs.h>
#include <Dry/Graphics/AnimatedModel.h>
#include <Dry/Graphics/AnimationController.h>
#include <Dry/Graphics/Animation.h>
#include <Dry/Graphics/AnimationState.h>
#include <Dry/Graphics/Camera.h>
#include <Dry/Graphics/DebugRenderer.h>
#include <Dry/Graphics/DecalSet.h>
#include <Dry/Graphics/Graphics.h>
#include <Dry/Graphics/Geometry.h>
#include <Dry/Graphics/GraphicsEvents.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Material.h>
#include <Dry/Graphics/Model.h>
#include <Dry/Graphics/Octree.h>
#include <Dry/Graphics/OctreeQuery.h>
#include <Dry/Graphics/ParticleEffect.h>
#include <Dry/Graphics/ParticleEmitter.h>
#include <Dry/Graphics/ReflectionProbe.h>
#include <Dry/Graphics/Renderer.h>
#include <Dry/Graphics/RenderPath.h>
#include <Dry/Graphics/Skybox.h>
#include <Dry/Graphics/StaticModel.h>
#include <Dry/Graphics/StaticModelGroup.h>
#include <Dry/Graphics/Texture2D.h>
#include <Dry/Graphics/Viewport.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/Input/InputEvents.h>
#include <Dry/Input/Input.h>
#include <Dry/IK/IKEffector.h>
#include <Dry/IK/IKSolver.h>
#include <Dry/IO/FileSystem.h>
#include <Dry/IO/Log.h>
#include <Dry/IO/MemoryBuffer.h>
#include <Dry/Math/MathDefs.h>
#include <Dry/Math/Plane.h>
#include <Dry/Math/Sphere.h>
#include <Dry/Math/Vector2.h>
#include <Dry/Math/Vector3.h>
#include <Dry/Navigation/NavigationMesh.h>
#include <Dry/Navigation/Navigable.h>
#include <Dry/Navigation/Obstacle.h>
#include <Dry/Physics/CollisionShape.h>
#include <Dry/Physics/Constraint.h>
#include <Dry/Physics/PhysicsEvents.h>
#include <Dry/Physics/PhysicsWorld.h>
#include <Dry/Physics/RigidBody.h>
#include <Dry/Resource/ResourceCache.h>
#include <Dry/Resource/XMLFile.h>
#include <Dry/Scene/LogicComponent.h>
#include <Dry/Scene/Component.h>
#include <Dry/Scene/Node.h>
#include <Dry/Scene/ObjectAnimation.h>
#include <Dry/Scene/SceneEvents.h>
#include <Dry/Scene/Scene.h>
#include <Dry/Scene/ValueAnimation.h>
#include <Dry/UI/UI.h>

#include <initializer_list>

#define FILES GetSubsystem<FileSystem>()
#define ENGINE GetSubsystem<Engine>()
#define TIME GetSubsystem<Time>()
#define CACHE GetSubsystem<ResourceCache>()
#define INPUT GetSubsystem<Input>()
#define GRAPHICS GetSubsystem<Graphics>()
#define RENDERER GetSubsystem<Renderer>()
#define AUDIO GetSubsystem<Audio>()

#define FX GetSubsystem<EffectMaster>()
#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)
#define RES_NO(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y, false)

#define LAYER(n) (1u << (n - 1u))
enum Layers{ L_WORLD        = LAYER(1u),
             L_CHARACTERS   = LAYER(2u),
             L_BUBBLES      = LAYER(3u),
             L_WATER        = LAYER(4u)
           };

using namespace Dry;

namespace LucKey
{

//unsigned Mask(Vector<unsigned> layers);
unsigned IntVector2ToHash(IntVector2 vec);

float Delta(float lhs, float rhs, bool angle = false);
float Distance(const Vector3 from, const Vector3 to);
Vector3 Scale(const Vector3 lhs, const Vector3 rhs);
IntVector2 Scale(const IntVector2 lhs, const IntVector2 rhs);
Vector2 Rotate(const Vector2 vec2, const float angle);
Color RandomColor();
Color RandomSkinColor();
Color RandomHairColor(bool onlyNatural = false);

float Sine(float x);
float Cosine(float x);

int Cycle(int x, int min, int max);
float Cycle(float x, float min, float max);
}

using namespace LucKey;
