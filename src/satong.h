/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SATONG_H
#define SATONG_H

#include "gamedefs.h"
#include "sceneobject.h"

class Catchable;

class Satong: public SceneObject
{
    DRY_OBJECT(Satong, SceneObject);

public:
    static void RegisterObject(Context* context);
    Satong(Context* context);

    void Set(const Vector3& position, const Quaternion& rotation) override;
    void SetTeam(TeamPreference team);
    TeamPreference GetTeam() const { return team_; }
    void SetSatiation(float satiation) { satiation_ = satiation; }
    float GetSatiation() const { return satiation_; }

    void Devour(Catchable* catchable);
    void OnSetEnabled() override;
    void Start() override;
    void DelayedStart() override;
    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void UpdateSatiation(float timeStep);
    void LeanIn(float timeStep);

    Node* DetectFood(float radius);
    Vector3 GetBitePos(Node* node);
    Quaternion GetBiteRot(Node* node);

    void HandleSceneDrawableUpdateFinished(StringHash eventType, VariantMap& eventData);

    AnimatedModel* model_;
    AnimationController* animCtrl_;
    float satiation_;
    TeamPreference team_;

    IKSolver* ikSolver_;
    IKEffector* ikEffector_;
    float chillTime_;

    PODVector<StaticModel*> accessories_;
};

#endif // SATONG_H
