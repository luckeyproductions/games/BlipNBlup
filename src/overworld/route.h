/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ROUTE_H
#define ROUTE_H

#include "../luckey.h"

DRY_EVENT(E_ROUTEAPPEARED, RouteAppeared)
{
    DRY_PARAM(P_ROUTE, Route);              // ptr
    DRY_PARAM(P_DESTINATION, Destination);  // int
}

class Route: public Component
{
    DRY_OBJECT(Route, Component);

public:
    Route(Context* context);

    void SetPoints(const PODVector<Vector3>& points);
    void SetDestination(unsigned destination) { destination_ = destination; }
    void Appear();
    void Reset();

    Vector3 GetPoint(float at) const;
    float GetLength() const { return length_; }

protected:
    void OnNodeSet(Node* node) override;

private:
    void CalculateLength();
    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);

    DecalSet* dashes_;
    Spline spline_;
    float length_;
    float lastDash_;
    float progress_;
    unsigned destination_;
};

#endif // ROUTE_H
