/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PARTY_H
#define PARTY_H

#include "../luckey.h"

#define TRAVEL_SPEED 1.7f

class Route;

class Party: public LogicComponent
{
    DRY_OBJECT(Party, LogicComponent);

public:
    Party(Context* context);

    void PostUpdate(float timeStep) override;

    void Ground(Node* node, float timeStep);
    void SetIdle(bool idle);
    bool IsIdle() const { return idle_; }
    void SetRoute(Route* route, bool reverse = false);
    Route* GetRoute() const { return route_; }
    void MoveTo(const Vector3& destination);
    float GetTravelTime() const;

    void Reset();

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandleAnimationRemoved(StringHash eventType, VariantMap& eventData);

    StringVector memberAnimations_;
    PODVector<AnimationController*> animCtrls_;
    Route* route_;
    float alongRoute_;
    bool reverse_;
    bool idle_;
};

DRY_EVENT(E_DESTINATIONREACHED, DestinationReached)
{
}

#endif // PARTY_H
