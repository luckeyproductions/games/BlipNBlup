/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ISLAND_H
#define ISLAND_H

#include "levelmarker.h"

//#include "../mastercontrol.h"

class Party;
class LevelMarker;

class Island: public Object
{
    DRY_OBJECT(Island, Object);

public:
    Island(Context* context);

    Camera* GetCamera() const { return camera_; }
    bool IsActive() const { return islandScene_->IsUpdateEnabled(); }

    void Show(bool interactive);
    void ResetProgress();

private:
    void SubscribeToEvents();

    void CreateScene();
    void CreateIsland();
    void CreateCamera();
    void CreateZoneSkyAndLight();
    void AddMarker(const Vector2& pos, const String& map);
    void CreateLevelText();

    void SetInteractive(bool interactive);
    void Unlock(unsigned index);
    void SelectLevel(unsigned index);

    void HandleInput(            StringHash eventType, VariantMap& eventData);
    void HandleSceneUpdate(      StringHash eventType, VariantMap& eventData);
    void HandleScreenMode(       StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);
    void HandleRouteAppeared(    StringHash eventType, VariantMap& eventData);
    void HandleSizesChanged(StringHash eventType, VariantMap& eventData);

    Scene* islandScene_;
    Node* islandNode_;
    StaticModel* islandModel_;
    Node* waterNode_;
    Node* cameraNode_;
    Camera* camera_;
    Camera* reflectionCamera_;
    float spin_;
    Vector<SharedPtr<LevelMarker>> levels_;
    DecalSet* markerDecals_;
    Party* party_;
    unsigned selectedLevel_;
    Text* levelText_;
    void UpdateSizes();
};

#endif // ISLAND_H
