/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "route.h"
#include "party.h"

Party::Party(Context* context): LogicComponent(context),
    memberAnimations_{},
    animCtrls_{},
    route_{ nullptr },
    alongRoute_{ 0.f },
    reverse_{ false },
    idle_{ true }
{}

void Party::OnNodeSet(Node* node)
{
    if (!node)
        return;

    for (unsigned m{ 0u }; m < 3u; ++m)
    {
        String memberName{};
        switch (m) {
        case 0u: memberName = "Blip";   break;
        case 1u: memberName = "Blup";   break;
        case 2u: memberName = "Satong"; break;
        }

        Node* memberNode{ node_->CreateChild(memberName) };
        const float scale{ .125f };
        memberNode->Translate(Quaternion{ 120.f * m - 45.f, Vector3::UP } * Vector3::FORWARD * scale);
        memberNode->SetScale(scale);
        AnimatedModel* model{ memberNode->CreateComponent<AnimatedModel>() };
        model->SetModel(RES(Model, String{ "Models/" } + memberName + ".mdl"));
        model->SetMaterial(RES(Material, "Materials/VColOutlineOverworld.xml"));
        model->SetViewMask(M_MAX_UNSIGNED - 1u);
        //            model->SetCastShadows(true);

        AnimationController* animCtrl{ memberNode->CreateComponent<AnimationController>() };
        const String walkAnim{ "Animations/" + (m == 2u ? memberName : "Fish") + "/Walk.ani" };
        animCtrl->PlayExclusive(walkAnim, 0u, true);
        if (m == 1u)
            animCtrl->SetTime(walkAnim, animCtrl->GetLength(walkAnim) * .25f);

        memberAnimations_.Push(walkAnim);
        animCtrls_.Push(animCtrl);
    }

    SubscribeToEvent(E_DESTINATIONREACHED, DRY_HANDLER(Party, HandleAnimationRemoved));
}

void Party::PostUpdate(float timeStep)
{
    if (!idle_)
    {
        for (Node* member: node_->GetChildren())
            Ground(member, timeStep);
    }
}

void Party::Ground(Node* node, float timeStep)
{
    Octree* octree{ GetScene()->GetComponent<Octree>() };
    const Ray groundRay{ node->GetWorldPosition().ProjectOntoPlane(Vector3::UP) + Vector3::UP * 3.f, Vector3::DOWN  };
    PODVector<RayQueryResult> result{};
    RayOctreeQuery query{ result, groundRay, RAY_TRIANGLE, 10.f, DRAWABLE_GEOMETRY, 1u };
    octree->RaycastSingle(query);

    if (!query.result_.IsEmpty())
    {
        const RayQueryResult firstResult{ query.result_.Front() };
        node->SetWorldPosition(firstResult.position_);
        Quaternion rot;
        rot.FromLookRotation(node->GetWorldRight().CrossProduct(firstResult.normal_));
        node->SetWorldRotation(node->GetWorldRotation().Slerp(rot, timeStep * 5.f));
    }
}

void Party::SetIdle(bool idle)
{
    idle_ = idle;

    for (unsigned a{ 0u }; a < animCtrls_.Size(); ++a)
        animCtrls_.At(a)->SetSpeed(memberAnimations_.At(a), 1/3.f + 4/3.f * TRAVEL_SPEED * !idle_);
}

void Party::SetRoute(Route* route, bool reverse)
{
    Route* previousRoute{ route_ };
    route_ = route;

    if (!route_)
        return;

    bool turn{ false };
    if (previousRoute && reverse_ != reverse)
    {
        for (Node* child: node_->GetChildren())
            child->Pitch(-child->GetRotation().PitchAngle() * 2.f);

        node_->Yaw(180.f);
        turn = true;
    }

    reverse_ = reverse;

    const float lastTime{ node_->GetAttributeAnimationTime("Position") };

    ObjectAnimation* travelAnim{ new ObjectAnimation{ context_ } };
    ValueAnimation* positionAnim{ new ValueAnimation{ context_ } };
    ValueAnimation* rotationAnim{ new ValueAnimation{ context_ } };
    travelAnim->AddAttributeAnimation("Position", positionAnim, WM_ONCE);
    travelAnim->AddAttributeAnimation("Rotation", rotationAnim, WM_ONCE);

    const float step{ .05f };
    const unsigned frames{ static_cast<unsigned>(CeilToInt(route_->GetLength() / step)) };
    for (unsigned f{ 0u }; f < frames; ++f)
    {
        const float progress{ f * step };
        const float l{ (reverse_ ? route_->GetLength() - progress : progress) };

        positionAnim->SetKeyFrame(progress / TRAVEL_SPEED, route_->GetPoint(l));
    }
    positionAnim->SetEventFrame(positionAnim->GetEndTime(), E_DESTINATIONREACHED);

    const Vector<VAnimKeyFrame>& posFrames{ positionAnim->GetKeyFrames() };
    Quaternion rot{};
    for (unsigned f{ 0u }; f < posFrames.Size() - 2u; ++f)
    {
        const VAnimKeyFrame earlierFrame{ posFrames.At(f) };
        const VAnimKeyFrame laterFrame{ posFrames.At(f + 1u) };
        const Vector3 from{ earlierFrame.value_.GetVector3() };
        const Vector3 to{   laterFrame.value_.GetVector3() };
        const Vector3 direction{ (to - from).Normalized() };
        const float dt{ laterFrame.time_ - earlierFrame.time_ };

        Quaternion targetRot{ node_->GetRotation() };
        if (f > 0u)
            targetRot.FromLookRotation(direction);
        rot = rot.Slerp(targetRot, (f == 0u ? 1.f : dt * 17.f));
        rotationAnim->SetKeyFrame(earlierFrame.time_, rot);
    }

    node_->RemoveAttributeAnimation("Position");
    node_->SetObjectAnimation(travelAnim);
    if (turn)
    {
        const float endTime{ positionAnim->GetEndTime() };
        node_->SetAttributeAnimationTime("Position", endTime - lastTime);
        node_->SetAttributeAnimationTime("Rotation", endTime - lastTime);
    }

    SetIdle(false);
}

void Party::MoveTo(const Vector3& destination)
{
    const Vector3 partyPosition{ node_->GetPosition() };
    const Vector3 toDestination{ destination - partyPosition };
    const float endTime{ Max(M_EPSILON, toDestination.Length() / TRAVEL_SPEED) };
    ValueAnimation* travelAnim{ new ValueAnimation{ context_ } };
    travelAnim->SetKeyFrame(0.f, partyPosition);
    travelAnim->SetKeyFrame(endTime, destination);
    travelAnim->SetEventFrame(endTime, E_DESTINATIONREACHED);
    node_->SetAttributeAnimation("Position", travelAnim, WM_ONCE);
    const Vector3 direction{ (destination - partyPosition).Normalized() };
    node_->LookAt(node_->GetPosition() + direction, Vector3::UP, TS_PARENT);

    SetIdle(false);
}

void Party::Reset()
{
    node_->SetPosition({ 8.f, 0.f, -1.5f });
    node_->RemoveAttributeAnimation("Position");
    node_->RemoveObjectAnimation();

    SetRoute(nullptr);
    SetIdle(true);
}

float Party::GetTravelTime() const
{
    const float progress{ (node_->GetAttributeAnimationTime("Position")) };
    ValueAnimation* posAnim{ node_->GetAttributeAnimation("Position") };
    return (!posAnim ? 0.f : posAnim->GetEndTime() - progress);
}

void Party::HandleAnimationRemoved(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetIdle(true);

    if (route_)
        SetRoute(nullptr);
    else
        node_->RemoveAttributeAnimation("Position");
}
