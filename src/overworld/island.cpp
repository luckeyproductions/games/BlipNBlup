/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "party.h"
#include "../ui/gui.h"
#include "../game.h"

#include "island.h"

Island::Island(Context* context): Object(context),
    islandScene_{ nullptr },
    islandNode_{ nullptr },
    islandModel_{ nullptr },
    waterNode_{ nullptr },
    cameraNode_{ nullptr },
    camera_{ nullptr },
    reflectionCamera_{ nullptr},
    spin_{ 0.f },
    levels_{},
    markerDecals_{ nullptr },
    party_{ nullptr },
    selectedLevel_{ M_MAX_UNSIGNED }
{
    context_->RegisterFactory<Party>();
    LevelMarker::RegisterObject(context_);

    CreateScene();
    CreateLevelText();

    UpdateSizes();
    SubscribeToEvents();
}

void Island::SubscribeToEvents()
{
    SubscribeToEvent(islandScene_, E_SCENEUPDATE, DRY_HANDLER(Island, HandleSceneUpdate));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(Island, HandleScreenMode));
    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(Island, HandleGameStatusChanged));
    SubscribeToEvent(E_MENUINPUT, DRY_HANDLER(Island, HandleInput));
    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Island, HandleSizesChanged));
}

void Island::CreateScene()
{
    islandScene_ = new Scene{ context_ };
    islandScene_->CreateComponent<Octree>();

    CreateIsland();
    CreateZoneSkyAndLight();
    CreateCamera();

    ResetProgress();
}

void Island::CreateIsland()
{
    islandNode_ = islandScene_->CreateChild("Island");
    islandModel_ = islandNode_->CreateComponent<StaticModel>();
    islandModel_->SetModel(RES(Model, "Models/Island.mdl"));
    islandModel_->SetMaterial(RES(Material, "Materials/Island.xml"));
    islandModel_->SetViewMask(1u);
    markerDecals_ = islandNode_->CreateComponent<DecalSet>();
    markerDecals_->SetMaterial(RES(Material, "Materials/Marker.xml"));

    AddMarker({ 4.f, -1.5f }, "Landing");
    AddMarker({ 3.f, .4f }, "TestMap");
    AddMarker({ 1.25f, 1.25f }, "TestMap");
    Node* partyNode{ islandNode_->CreateChild("Party") };
    party_ = partyNode->CreateComponent<Party>();

    waterNode_ = islandNode_->CreateChild("Water");
    waterNode_->SetScale(300.f);

    StaticModel* waterModel{ waterNode_->CreateComponent<StaticModel>() };
    waterModel->SetModel(RES(Model, "Models/Plane.mdl"));
    waterModel->SetMaterial(RES(Material, "Materials/Water.xml"));
    waterModel->SetViewMask(0x80000000);
}

void Island::AddMarker(const Vector2& pos, const String& map)
{
    SharedPtr<LevelMarker> marker{ MakeShared<LevelMarker>(context_, markerDecals_->GetNumDecals(), pos, map) };
    marker->SetScene(islandScene_);
    markerDecals_->AddDecal(islandModel_, { pos.x_, 3.f, pos.y_ }, { 90.f, Vector3::RIGHT }, .5f, 1.f, 10.f, Vector2::ZERO, Vector2::ONE);
    markerDecals_->SetColor(marker->GetID(), marker->GetColor());

    if (!levels_.IsEmpty())
    {
        LevelMarker* previous{ levels_.Back() };
        previous->SetNext(marker);
        marker->SetPrevious(previous);

        Node* routeNode{ islandNode_->CreateChild("Route") };
        Route* route{ routeNode->CreateComponent<Route>() };
        const Vector3 routeStart{ previous->GetPosition() };
        const Vector3 routeEnd{ marker->GetPosition() };
        const Vector3 average{ (previous->GetPosition() + marker->GetPosition()) * .5f };
        const Vector3 swayA{ average.Lerp(routeStart, .5f) * 2/3.f };
        const Vector3 swayB{ average.Lerp(routeEnd, .5f) * 4/3.f };
        route->SetPoints({ routeStart, swayA, swayB, routeEnd });
        previous->SetRoute(route);
    }

    levels_.Push(marker);
}

void Island::CreateZoneSkyAndLight()
{
    Zone* zone{ islandScene_->CreateComponent<Zone>() };
    zone->SetBoundingBox({ Vector3::ONE * -1000.f, Vector3::ONE * 1000.f });
    zone->SetFogStart(23.f);
    zone->SetFogEnd(55.f);
    zone->SetHeightFog(true);
    zone->SetFogHeight(-.8f);
    zone->SetFogHeightScale(3.4f);
    zone->SetFogColor(Color::WHITE.Lerp(Color::CYAN, .42f));
    zone->SetAmbientColor({ .23f });

    Node* skyNode{ islandNode_->CreateChild("Sky") };
    Skybox* skybox{ skyNode->CreateComponent<Skybox>() };
    skybox->SetModel(RES(Model, "Models/Box.mdl"));
    SharedPtr<Material> skyboxMat{ RES(Material, "Materials/Skybox.xml")->Clone() };
    skyboxMat->SetShaderParameter("MatDiffColor", Color::WHITE.Lerp(Color::CYAN, .34f));
    skybox->SetMaterial(skyboxMat);

    Node* lightNode{ islandScene_->CreateChild("Sun") };
    lightNode->SetPosition({ -2.3f, 10.f, -4.2f });
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetBrightness(.6f);
//    light->SetShadowIntensity(0.13f);
    light->SetColor({ .95f, .9f, .85f });
    light->SetCastShadows(true);
}

void Island::CreateCamera()
{
    cameraNode_ = islandScene_->CreateChild("Camera");
    camera_ = cameraNode_->CreateComponent<Camera>();
    camera_->SetFov(34.f);

    ValueAnimation* camAnim{ new ValueAnimation{ context_ } };
    camAnim->SetKeyFrame(0.f,  Vector3{ 0.f, 2.3f, -42.f });
    camAnim->SetKeyFrame(5.5f, Vector3{ 0.f, 3.5f, -11.f });
    camAnim->SetInterpolationMethod(IM_SINUSOIDAL);
    cameraNode_->SetAttributeAnimation("Position", camAnim, WM_CLAMP);
    cameraNode_->SetAttributeAnimationTime("Position", 2.3f);

    Viewport* viewport{ new Viewport{ context_, islandScene_, camera_ } };
    RENDERER->SetViewport(0, viewport);

    Plane waterPlane{ waterNode_->GetWorldRotation() * Vector3::UP, waterNode_->GetWorldPosition() };
    Plane waterClipPlane{ waterNode_->GetWorldRotation() * Vector3::UP,
                waterNode_->GetWorldPosition() - .01f * Vector3::UP };

    reflectionCamera_ = cameraNode_->CreateComponent<Camera>();
    reflectionCamera_->SetFarClip(750.f);
    reflectionCamera_->SetViewMask(0x7fffffff); // Hide objects with only bit 31 in the viewmask (the water plane)
    reflectionCamera_->SetAutoAspectRatio(false);
    reflectionCamera_->SetUseReflection(true);
    reflectionCamera_->SetReflectionPlane(waterPlane);
    reflectionCamera_->SetUseClipping(true); // Enable clipping of geometry behind water plane
    reflectionCamera_->SetClipPlane(waterClipPlane);
    reflectionCamera_->SetFov(camera_->GetFov());
    reflectionCamera_->SetAspectRatio(GRAPHICS->GetRatio());

    const unsigned textureSize{ Max(1u, NextPowerOfTwo(GRAPHICS->GetWidth()) / 2u) };
    SharedPtr<Texture2D> renderTexture{ new Texture2D{ context_ } };
    renderTexture->SetSize(textureSize, textureSize, Graphics::GetRGBFormat(), TEXTURE_RENDERTARGET);
    renderTexture->SetFilterMode(FILTER_ANISOTROPIC);
    RenderSurface* surface{ renderTexture->GetRenderSurface() };
    SharedPtr<Viewport> rttViewport{ new Viewport{ context_, islandScene_, reflectionCamera_ } };
    surface->SetViewport(0, rttViewport);
    Material* waterMat{ RES(Material, "Materials/Water.xml") };
    waterMat->SetTexture(TU_DIFFUSE, renderTexture);
}

void Island::CreateLevelText()
{
    levelText_ = GetSubsystem<UI>()->GetRoot()->CreateChild<Text>();
    levelText_->SetName("LevelText");
    levelText_->SetText("Untitled");
    levelText_->SetAlignment(HA_CENTER, VA_TOP);
    levelText_->SetFont(FONT_SUPER_BUBBLE);
    levelText_->SetColor(Color::AZURE.Lerp(Color::CYAN, .8f).Lerp(Color::WHITE, .4f));
    levelText_->SetTextEffect(TE_SHADOW);
    levelText_->SetEffectColor(Color::AZURE.Lerp(Color::BLUE, .25f).Transparent(.75f));
    levelText_->SetOpacity(.875f);
}

void Island::SetInteractive(bool interactive)
{
    levelText_->SetVisible(interactive);
    markerDecals_->SetEnabled(interactive);
    party_->GetNode()->SetEnabledRecursive(interactive);
    for (LevelMarker* marker: levels_)
    {
        Route* route{ marker->GetRoute() };
        if (route)
            route->GetNode()->SetEnabled(interactive);
    }

    if (interactive && selectedLevel_ == M_MAX_UNSIGNED)
        SelectLevel(0u);

//    for (LevelMarker* level: levels_)
//        level->Unlock();
}

void Island::Unlock(unsigned index)
{
    if (index < levels_.Size())
    {
        LevelMarker* marker{ levels_.At(index) };
        if (marker->HasRoute())
            SubscribeToEvent(marker->GetRoute(), E_ROUTEAPPEARED, DRY_HANDLER(Island, HandleRouteAppeared));

        marker->Unlock();
    }
}

void Island::SelectLevel(unsigned index)
{
    if (selectedLevel_ == index)
        return;

    const unsigned lastSelected{ selectedLevel_ };
    const bool reverse{ index < lastSelected };
    selectedLevel_ = index;

    LevelMarker* destination{ levels_.At(selectedLevel_) };

    if (lastSelected != M_MAX_UNSIGNED)
    {
        LevelMarker* previousMarker{ levels_.At(lastSelected) };
        Route* route{ (reverse ? destination : previousMarker)->GetRoute() };
        Route* currentRoute{ party_->GetRoute() };
        const bool movingOffRoute{ (!currentRoute && !party_->IsIdle()) };
        if ((currentRoute && route != currentRoute) || movingOffRoute)
        {
            selectedLevel_ = lastSelected;
            return;
        }

        party_->SetRoute(route, reverse);
        previousMarker->FadeToColor(COLOR_MARKER_INACTIVE, 0.f);
    }
    else
    {
        party_->MoveTo(destination->GetPosition());
    }

    destination->FadeToColor(COLOR_MARKER_ACTIVE, Max( 0.f, party_->GetTravelTime() - .1f));
    levelText_->SetText(destination->GetMapName());
}

void Island::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus gameStatus{ GetSubsystem<Game>()->GetStatus() };
    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };

    if (gameStatus < GS_OVERWORLD)
    {
        spin_ += timeStep * Max(7.f, 34.f - islandScene_->GetElapsedTime() * 7.f);
    }
    else
    {
        const float targetSpin{ levels_.At(selectedLevel_)->GetViewYaw() };
        spin_ = AbsMod(spin_, 360.f);
        const bool counter{ Abs(targetSpin - spin_) > 180.f };
        if (counter)
            spin_ += 360.f * Sign(targetSpin - spin_);

        spin_ = Lerp(spin_, targetSpin, timeStep);
    }

    islandNode_->SetRotation({ spin_, Vector3::UP });
    cameraNode_->LookAt(Vector3::UP * .875f);

    GUI* gui{ GetSubsystem<GUI>() };
    if (gui->GetBlackness() > 0.f)
    {
        const float fadeIn{ 1.f - Cbrt(Min(1.f, islandScene_->GetElapsedTime())) };
        gui->SetBlackness(fadeIn);
    }

    if (gameStatus != GS_OVERWORLD)
        return;

    for (LevelMarker* marker: levels_)
        markerDecals_->SetColor(marker->GetID(), marker->GetColor());
}

void Island::HandleInput(StringHash /*eventType*/, VariantMap& eventData)
{
    if (GAME->GetStatus() != GS_OVERWORLD || GetSubsystem<GUI>()->IsNonDashVisible())
        return;

    const VariantVector& varVec{ eventData[MenuInput::P_ACTIONS].GetVariantVector() };
    PODVector<MasterInputAction> actions{};
    for (const Variant& a: varVec)
        actions.Push(static_cast<MasterInputAction>(a.GetUInt()));

    int step{   actions.Contains(MIA_UP)
              - actions.Contains(MIA_DOWN)
              + actions.Contains(MIA_RIGHT)
              - actions.Contains(MIA_LEFT) };

    if (!(step == 0 || (selectedLevel_ == 0u && step < 0) || (levels_.At(selectedLevel_)->IsLocked() && step > 0)))
    {
        step = Sign(step);
        SelectLevel(Min(selectedLevel_ + step, levels_.Size() - 1u));
    }

    if (actions.Contains(MIA_CONFIRM) && party_->IsIdle())
    {
        LevelMarker* level{ levels_.At(selectedLevel_) };
        GAME->StartStoryLevel(level->GetMapName());
    }
}

void Island::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const float ratio{ (GRAPHICS->GetHeight() > 0 ? GRAPHICS->GetRatio() : 1.f) };
    const float fov{ 34.f / Sqrt(Min(1.f, ratio / 1.25f)) };

    camera_->SetFov(fov);
    reflectionCamera_->SetFov(fov);
    reflectionCamera_->SetAspectRatio(ratio);
}

void Island::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus oldStatus{ static_cast<GameStatus>(eventData[GameStatusChanged::P_OLDSTATUS].GetInt()) };
    const GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    if (oldStatus != GS_SPLASH && status != GS_SPLASH)
    {
        cameraNode_->SetAttributeAnimationTime("Position", cameraNode_->GetAttributeAnimation("Position")->GetEndTime());
        islandScene_->SetElapsedTime(Max(islandScene_->GetElapsedTime(), 3.f));
    }

    if (status == GS_MAIN)
        ResetProgress();
    else if (status == GS_OVERWORLD && oldStatus == GS_PLAY)
        Unlock(selectedLevel_);

    if (status < GS_PLAY)
    {
        Show(status == GS_OVERWORLD);
    }
    else
    {
        islandScene_->SetUpdateEnabled(false);
        levelText_->SetVisible(false);
    }

}

void Island::HandleRouteAppeared(StringHash /*eventType*/, VariantMap& eventData)
{
    const unsigned destination{ eventData[RouteAppeared::P_DESTINATION].GetUInt() };

    if (destination < levels_.Size())
    {
        LevelMarker* nextMarker{ levels_.At(destination) };
        if (nextMarker)
            nextMarker->FadeToColor(COLOR_MARKER_INACTIVE);
    }
}

void Island::Show(bool interactive)
{
    Viewport* viewPort{ GetSubsystem<Renderer>()->GetViewport(0) };
    viewPort->SetCamera(camera_);
    viewPort->SetScene(islandScene_);
    islandScene_->SetUpdateEnabled(true);

    SetInteractive(interactive);
}

void Island::ResetProgress()
{
    selectedLevel_ = M_MAX_UNSIGNED;
    party_->Reset();

    for (LevelMarker* level: levels_)
    {
        level->Reset();
        markerDecals_->SetColor(level->GetID(), level->GetColor());
    }
}

void Island::UpdateSizes()
{
    if (levelText_)
    {
        float fontSize{ Min(GUI::VW(3.4f), GUI::VH(5.5f)) };

        levelText_->SetFontSize(fontSize);
        levelText_->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
        levelText_->SetPosition(0, .23f * fontSize);
    }
}

void Island::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSizes();
}
