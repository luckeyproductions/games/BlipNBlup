/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "bugbrain.h"
#include "game.h"
#include "catchable.h"
#include "container.h"
#include "rage.h"
#include "effectmaster.h"

Catchable::Catchable(Context* context): LogicComponent(context),
    nourishment_{ 1/8.f },
    caught_{ false },
    releaseTime_{ 1.f },
    team_{ TP_RANDOM }
{
}

void Catchable::RegisterObject(Context* context)
{
    Rage::RegisterObject(context);
    context->RegisterFactory<Catchable>();
    context->RegisterFactory<BugBrain>();
}

bool Catchable::CatchIn(Container* container)
{
    if (!caught_)
    {
        caught_ = true;

        if (RigidBody* rb{ GetComponent<RigidBody>() })
            rb->SetKinematic(true);
        if (CollisionShape* cs{ GetComponent<CollisionShape>() })
            cs->SetEnabled(false);

        node_->SetParent(container->GetNode());
        FX->TransformTo(node_, Vector3::ZERO, node_->GetRotation(), .23f);

        node_->SendEvent(E_CATCH);
        return true;
    }

    return false;
}

void Catchable::Release(bool devoured)
{
    if (caught_)
    {
        caught_ = false;

        if (RigidBody* rb{ GetComponent<RigidBody>() })
            rb->SetKinematic(false);
        if (CollisionShape* cs{ GetComponent<CollisionShape>() })
            cs->SetEnabled(true);

        node_->SetParent(GetScene());
        FX->RotateTo(node_, Quaternion::IDENTITY, .42f);

        if (!devoured)
            node_->SendEvent(E_BUGRELEASED);
    }
}

void Catchable::SetTeam(TeamPreference team)
{
    team_ = team;

    Controllable* controllable{ node_->GetDerivedComponent<Controllable>() };
    AnimatedModel* bugModel{ (controllable ? controllable->GetModel() : nullptr) };
    if (!bugModel)
        return;

    Color col;
    switch (team)
    {
    default: col = RES(Material, "Materials/Carapace.xml")->GetShaderParameter("MatDiffColor").GetColor(); break;
    case TP_BLIP: col = Color::RED; break;
    case TP_BLUP: col = Color::AZURE; break;
    }

    bugModel->GetMaterial(0)->SetShaderParameter("MatDiffColor", col);
}

void Catchable::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->CreateComponent<BugBrain>();
}
