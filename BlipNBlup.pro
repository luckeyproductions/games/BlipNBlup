include(src/BlipNBlup.pri)

TARGET = blipnblup

LIBS += $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

DEFINES += DRY_COMPILE_QT

# Compile for C++11 with speed optimization to level 2
QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/blipnblup/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = blipnblup.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = blipnblup.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
