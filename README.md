![Blip](Docs/blip.png) | ![Sa'Tong](Docs/satong.png)| ![Blup](Docs/blup.png)
---|---|---


# Blip 'n Blup: Skyward Adventures

Two fish, a pact with Sa'Tong and a quest for food, enlightenment and trust. [[...]](Docs/ideas.md)

## Screenshots

[![Blip 'n Blup screenshot](Screenshots/20201231.jpg)](Screenshots/20201231.jpg)

## Inspiration

* **[Bubble Bobble](https://www.youtube-nocookie.com/embed/iVRKv22JjbI?autoplay=true)**
* **[The Lost Vikings](https://www.youtube-nocookie.com/embed/OwUYisoOUr4?autoplay=true)**
