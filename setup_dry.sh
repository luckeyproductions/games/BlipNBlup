#!/bin/sh
#
# Set up or update Dry.
#
# Usage:
#
#   ./setup_dry.sh
#
# This script does not actually build Dry.
# To build Dry, do:
#
#   cd Dry
#   ./quick.sh
#

cd `dirname $0`;
if [ ! -d Dry ]
then
    git clone https://gitlab.com/luckeyproductions/Dry
    cd Dry/
else
    cd Dry/
    git checkout master
    git pull
fi

./script/cmake_generic.sh . \
    -DDRY_ANGELSCRIPT=0 -DDRY_2D=0 \
    -DDRY_SAMPLES=0 -DDRY_TOOLS=0

cd `dirname $0`;

# Do not build Dry here!
# ./quick.sh
