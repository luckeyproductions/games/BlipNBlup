# Ideas

:warning: these ideas have ended up in the FAQ section :warning:

The sea isn't the place it used to be (pollution, mutants, weak fish) and when Blip 'n Blup narrowly avoid a sinking ship - which turns out to be loaded with shoes - they decide to walk out.  
All their lives they've been bombarded with trash from above and wish to find the highest place, where there is no above.

- Portals (Teleporting doors)
- Pits (Make things disappear)
- Double jump, second is aimed by its timing during the first.
- Angry creatures on bubble escape (float cooldown)


## Chapters

* Beach/Jungle | 1: Landing
   + Worms
   + Crabs
   + Frogs
* Leafy forest/Prickly forest | 2:
   + Wasps
   + Spiders
   + Grasshoppers
* Rocky hills/Snowy peaks | 3: Let there be rock
* Volcano/Lost temple | 4:
   + Gnomes
* Spaceship | 5:Liftoff
   - Spoon

### Bonus

Bonus levels consist of collecting trash. Cans can be kicked, glass shards need a bubble.

## Multiplayer Gamemodes

* **_Capture the bug_** (CTF)
* Footbubble
* Collector
* Survival (waves of creatures)

## Quotes

### Sa'Tong Sterneal
> "I'm such an S"

### Spoon
> "I am that I am not"

## Inspiration

- Bubble Bobble
- Super Mario War
- Golf
- Marble Madness

- Wall-E
